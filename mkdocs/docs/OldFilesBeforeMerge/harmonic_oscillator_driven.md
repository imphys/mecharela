# Driven Damped Harmonic Oscillator

Oscillators sometimes experience a driving force that is periodic in itself. The frequency of the driving force can be a broader spectrum or a narrow one. We will take here the case of a sinusoidal force with frequency $\nu$. Once we understand this, forces with a broader spectrum can be understood using Fourier analysis. We will not go in that direction here and stick to a simple driving force of the form $F_{ext} = F_0 \sin \nu t$.

This gives for the equation of motion (N2):

$$
m\ddot{x} = -kx -b\dot{x} + F_0 \sin \nu t
$$

with initial conditions: at $t= 0$ the particle will have some position $x_0$ and some velocity $v_0$.

**Archetype Driven Damped Harmonic Oscillator**

$$
\boxed{  \begin{array} &\text{          }&m\ddot{x} &= -kx -b \dot{x}  + F_0 \sin \nu t & \\
             \text{ i.c.:  } &t= 0 &\rightarrow x = x_0, &v = v_0 \end{array} }
$$

---

The solution of the driven damped harmonic oscillator equation of motion for the case $D = b^2 - 4mk < 0$ is:

$$
x(t) = A e^{- \frac{b}{2m} t} \sin \left ( \sqrt{\frac{k}{m} - \frac{b^2}{4m^2}} t+ \epsilon \right ) +
              x_{max} \sin \left ( \nu t + \alpha \right )
$$

With $A$ and $\epsilon$ determined by the initial conditions.

The two other parameters $x_{max}$ and $\alpha$ are fixed. We will give only the expression for $x_{max}$:

$$
x_{max} = \frac{F_0}{\sqrt{\left ( \omega_0^2 - \nu^2 \right ) ^2 + \frac{b^2}{m^2} \nu^2}}
$$


For a long time, the second part, i.e., the term from the driving force, survives as the exponential decay will have damped the first term. As can be seen, the amplitude of the motion as for the longer times $x_{max}$. If the driving frequency $\nu$ gets close to $\omega_0$, the amplitude increases. Especially for small damping, i.e., small $b$, the amplitude will increase to high values. This phenomenon is called resonance and can mathematically be written as:

$$
\text{if  } b \rightarrow 0 \text{ and } \nu \rightarrow \omega_0 \text{  then  } x_{max} \rightarrow \infty \text{   resonance}
$$

## Examples

1. Example of resonance: sound waves are exciting a glass. By changing the frequency of the sound waves to the resonance frequency, the glass starts oscillating with increasing amplitude until it finally breaks.

    <video id="myVideo" controls>
 	  	<source src="../movies/Resonance_GlassDestroyedbySound.mpg" type="video/mpg">
 		Your user agent does not support the HTML5 Video element.
	</video>

2. Driven harmonic oscillator with damping.

    Click on the image to start de physlet.

    [![](images/DrivenDampedOscillator.jpg){ width="400" }](https://qiweb.tudelft.nl/mecharela/Widgets/DrivenOscillator.html)

3. 1940: the Tacoma Narrows Bridge in the state Washington on the West side of the USA is brought into resonance by forcing from the wind. The end result: click the movie to see it yourself.

    *VIDEO*
