# Potential Energy & Stable/Unstable Equilibrium

A particle is in equilibrium when the sum of forces acting on it is zero. Then, it will keep the same velocity, and via a Galilei Transformation, we can easily find an inertial system in which the particle is sitting still, at an equilibrium position. The same position or more general state can also be found directly from the potential energy.

Potential energy and (conservative) forces are coupled via:

$$
\vec{F} = -\nabla V
$$

The equilibrium positions are found by finding the extremes of the potential energy:

$$
\text{ equilibrium position   } \leftrightarrow \nabla V = 0
$$

## Stability

Once we find the stable points, we can also quickly address its nature: is it a stable or unstable solution? That follows directly from inspecting the characteristics of the potential energy around the equilibrium points. 

For a stable equilibrium, we require that a small push or a slight displacement will result in a pushing back such that the equilibrium position is restored (apart from the inertia of the object that might cause an overshoot). 

On the other hand, an unstable equilibrium is one for which the slightest push or displacement will result in motion away from the equilibrium position.

In mathematical terms:

$$
\boxed{  \text{equilibrium:   } \nabla V = 0  
            \left \{ \begin{array} 
                &\text{} &\text{} \\ 
                &\text{stable:   } \nabla^2 V &\equiv \nabla \cdot \nabla V&\gt 0 \\ 
                &\text{} &\text{} \\
                &\text{unstable:   } \nabla^2 V &\equiv \nabla \cdot \nabla V&\lt 0 \\ 
                &\text{} &\text{}
            \end{array} \right . }
$$

---

Luckily, the definition of potential energy is such that these rules are easy to visualize and remember.

![StableUnstable](images/StableUnstable.jpg){ align="center" width="400"}

This is shown in the picture above. It is like a valley and a hill: a valley is stable; on top of the hill is unstable.

It is also easy to visualize what will happen if we distort the equilibrium:

* The valley, i.e., the stable system, will make the particle move back to the lowest point. Due to inertia, it will not stop but will continue to move. As the lowest position is one of zero force, the particle will 'climb toward the other end of the valley and start an oscillatory motion.'

* The top, i.e., the unstable point, will make the particle move away from the stable point. The force acting on the particle is now pushing it outwards, down the slope of the hill.

## Taylor Series

The Taylor expansion of Taylor series is a mathematical approximation of a function in the vicinity of a specific point. It is an infinite series of polynomial terms: the more you use, the better the approximation. Mathematically, it read as:

$$
f(x) \sim f(x_0) + \frac{1}{1!}f'(x_0) (x-x_0) + \frac{1}{2!}f"(x_0) (x-x_0)^2 + \cdot \cdot \cdot
$$

For our purpose here, it suffices to stop after the second derivative term.

---

A way of understanding what the Taylor series does is the following. Imagine you have to explain to someone how a function looks around some point $x_0$, but you are not allowed to draw. One way of passing on information about $f(x)$ is to start by giving the value of $f(x)$ at $x_0$:

$$
f(x) \approx f(x_0)
$$

Next, you give how the tangent at $x$ is; you pass on the first derivative at $x_0$. The other person can now see a bit better how the function changes when moving away from $x_0$:

$$
f(x) \approx f(x_0) + f'(x_0) (x-x_0)
$$

Then, you tell that the function is not a straight line but curved, and you give the second derivative. So now the other one can see how $f(x)$ deviates from a straight line:

$$
f(x) \sim f(x_0) + \frac{1}{1!}f'(x_0) (x-x_0) + \frac{1}{2!}f"(x_0) (x-x_0)^2
$$

Note that the prefactor is placed back. But the function is not necessarily a parabola; it will start deviating more and more as we move away from $x_0$. Hence we need to correct that by invoking the third derivative that tells us how fast this deviation is. And this process can continue on and on.

However, if we stay close enough to $x_0$ the terms with the lowest derivative will always prevail as a higher power tends to zero faster than a lower power for numbers smaller than 1.

---

The Taylor series is useful when studying the behavior of a particle around a stable equilibrium point. For such a point we have:

$$
V'( x_{eq}) = 0 \text{  and  } V^{''}_{eq} \equiv V"(x_{eq}) > 0
$$

Thus, if the particle stays close enough to the equilibrium point, we can write the energy equation as follows:

$$
\frac{1}{2}m\dot{x}^2 + V(x_{eq}) + 0 \cdot (x-x_0) + \frac{1}{2!} V_{eq} (x-x_0)^2 = E_0
$$

Take the time derivative and simplify to get:

$$
m\ddot{x} + v_{eq} (x-x_0) = 0
$$

But this is just the harmonic oscillator! So, we find that for small deviations from a stable equilibrium point, many systems will behave as a harmonic oscillator.

## Example

The widget below will show you the behavior close to and not-so-close to an equilibrium point of a given potential. Click on it to work with the widget.

[![](images/TaylorExpansion2.jpg){ width="400" }](https://qiweb.tudelft.nl/mecharela/Widgets/TaylorExpansion2.html)

Behavior around a stable equilibrium

## Exercises

1. A point particle (mass $m$) is moving under the influence of a force with associated potential energy $V(x) = -A \cos 2\pi \frac{x}{L}$ with $A > 0$.

    * Find the equilibrium points and determine whether these are stable or unstable.

    * The particle is initially at position $x=0$. At $t=0$ it is displaced by a small amount $\Delta x << L$. Find the motion of $m$ for $t >0$.

1. Given the potential energy $V(x) = e^{2-x} + e^x$:

    * Find the equilibrium points of *V* and determine their stability.

    * $V(x)$ has at least one stable equilibrium point. Determine the oscillation frequency for a particle of mass $m$ that started from rest just out of this equilibrium position.

1. Given the potential energy $V(x) = V_0 \left ( 5 + x +2x^2 + x^3 \right )$ with $V_0 > 0$:

    * Show that $V$ has two equilibrium points, determine their stability.

    * Determine the oscillation frequency for a particle of mass $m$ that started from rest just out the equilibrium position.

    * A particle is in an unstable position. Its position gets disturbed by a very small amount $\Delta x$. Determine how initially the particle will move away from the equilibrium position.
