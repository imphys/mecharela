# Kepler Revisited

![KeplerRevisited](images/KeplerRevisited.jpg){align="right" width="270"}

Now that we have seen how to deal with a two-particle problem, we can return to the motion of the earth around the sun. This is obviously not a single particle problem.

However, we can approximate it to a two-body problem: we ignore all other planets and have only the sun and earth. Hence, there are no external forces. Consequently, the center of mass of the earth-sun system moves at a constant velocity. And we can take the center of mass as our origin.

We have to solve the reduced mass problem to find the motion of both the earth and the sun:

$$
\mu \frac{d^2 \vec{r}_{12}}{dt^2} = -\frac{Gm_e m_s}{r_{12}^2} \hat{r}_{12}
$$

Note: this equation is almost identical to the original Kepler problem. All that happened is that $m_e$ on the left hand side got replaced by $\mu$.

Everything else remains the same: the force is still central and conservative, etc.

## Where is the Center of Mass Located

![EarthSunCoG](images/EarthSunCoG.jpg){align="right" width="180"}

We can easily find the center of mass of the sun-earth system. Chose the origin on the line through the sun and the earth (see fig.)

<br/>

$$
R = \frac{m_s x_s + m_e x_e }{m_s + m_e} = x_s + \frac{m_e}{m_s + m_e} (x_e - x_s)\approx x_s + 450km
$$

In other words: the sun and earth rotate in an ellipsoidal trajectory around the center of mass that is 450 km out of the center of the sun. Compare that to the radius of the sun itself: $R_s = 7 \cdot 10^5 km$. No wonder Kepler didn't notice.

## Exoplanets

However, in modern times, this slight motion of stars is a way of trying to find orbiting planets. Due to this small ellipsoidal trajectory, sometimes a star moves away from us, and sometimes it comes towards us. And this is a periodic motion, which lasts a 'year' of that 'solar system.'

So, astronomers started looking out for periodic changes in the apparent color of the light of stars. One can also look for periodic changes in the brightness of a star. If a planet is directly between the star and us, the intensity of the starlight decreases a bit. And they found one, and another one, and more and hundreds... Currently, more than 4300 exoplanets have been found.
