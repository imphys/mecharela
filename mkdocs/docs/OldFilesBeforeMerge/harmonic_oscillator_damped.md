# Damped Harmonic Oscillator

## Harmonic Oscillator in Terms of Energy

The harmonic oscillator can also be written in energy terms. For the 1-dimensional case, this is relatively easy. The force driving the oscillation is given by $F=−kx$. It is a conservative force, and the potential associated with it is readily calculated:

$$
F = -k x \rightarrow V(x) = -\int_{x_{ref}}^x -kx' dx' \Rightarrow V(x) = \frac{1}{2}k x^2
$$

Where we have taken $( x_{ref} = 0$.

Thus the energy equation for the harmonic oscillator is:

$$
\boxed{\frac{1}{2} m \dot{x}^2 + \frac{1}{2}k x^2 = E_0 }
$$

Note that for many potentials that have a stable equilibrium point ($V'(x_{eq}) = 0$ and $V"(x_{eq}) > 0$) we will have that for small deviations from the equilibrium position, a second-order Taylor expansion of the potential energy gives the harmonic oscillator:

$$
\begin{eqnarray} V(x) &\approx &V(X_{eq}) +\frac{1}{1!} V'(x_{eq})(x-x_{eq}) + \frac{1}{2!} V^{"}(x_{eq})(x-x_{{eq}})^2 + h.o.t. \rightarrow \\
          V(x) &\approx &V(X_{eq}) + \frac{1}{2} V^{"}(x_{eq})(x-x_{{eq}})^2 \Rightarrow \end{eqnarray}
$$

$$
\boxed{\frac{1}{2} m \dot{x}^2 + \frac{1}{2} V^{"}(x_{eq})(x-x_{{eq}})^2 = E_0 - V(x_{eq}) }
$$

This equation is similar to the one in the first box above. By shifting the origin of our coordinate system to the equilibrium position, $x_{eq}$ becomes 0 and disappears. By redefining the potential energy (i.e., shifting it such that $V(x_{eq})=0$, the extra term on the right disappears (or, alternatively, write the right-hand side as one, new letter $\tilde{E}_0$).

## Damped Harmonic Oscillator

In many cases, an additional friction-like force acts on the mass. Consequently, some of the spring's kinetic and/or potential energy is converted into heat, and the oscillator's motion decays over time: the oscillation is damped.

A simple case occurs when the damping force is proportional to the velocity of the mass. The equation of motion now takes the force, i.e., $F_f = -b \cdot{x}$:

$$
m\ddot{x} = -kx -b\dot{x}
$$

With initial conditions: at $t=0$ the particle will have some position $x_0$ and some velocity $v_0$.

**Archetype Damped Harmonic Oscillator**

$$
\boxed{  \begin{array} &\text{          }&m\ddot{x} &= -kx -b \dot{x} & \\
             \text{ i.c.:  } &t= 0 &\rightarrow x = x_0, &v = v_0 \end{array} }
$$

---

The solution of the damped harmonic oscillator equation of motion is more complicated than that of the harmonic oscillator:

$$
\left\{ \begin{array}{l}
          m \ddot{x} + b\dot{x} +kx  0 \\
          x(t) \sim e^{\lambda t} \end{array} \right\} \rightarrow
          \left\{ \begin{array} m\lambda^2 + b\lambda + k = 0 \\
          \lambda_\pm =\frac{-b \pm \sqrt{b^2 - 4mk}}{2m} \end{array} \right \}
$$

Discriminant: $D = b^2 - 4mk$.

---

Case 1: $D<0 \rightarrow \lambda$ complex

$$
x(t) = A e^{-\frac{b}{2m}t} \sin \sqrt{\frac{k}{m} - \frac{b^2}{4m^2}}t + b e^{-\frac{b}{2m}t} \cos \sqrt{\frac{k}{m} - \frac{b^2}{4m^2}}t
$$

![DampedOscillation](images/DampedOscillation.jpg){ align="right" width="230" }

with frequency

$$
\omega = \sqrt{\frac{k}{m} - \frac{b^2}{4m^2}} < \sqrt{\frac{k}{m}} = \omega_0
$$

---

Case 2: $D=0 \rightarrow \lambda = -\frac{b}{2m}$ both roots collapse

![CriticalDampedOscillation](images/CriticalDampedOscillation.jpg){ align="right" width="230" }

$$
x(t) = (A + Bt) e^{-\frac{b}{2m}t}
$$

Critically damped: fastest way to go back to equilibrium position

---

Case 3: $D>0 \rightarrow$ both roots are real

![RealRootOscillation](images/RealRootOscillation.jpg){ align="right" width="230" }

$$
x(t) = A e^{ \left ( -\frac{b}{2m} + \sqrt{\frac{b^2}{4m^2} - \frac{k}{m} } \right ) t} +
          B e^{ \left ( -\frac{b}{2m} - \sqrt{\frac{b^2}{4m^2} - \frac{k}{m} } \right ) t}
$$

Not really oscillating. The first term dictates the long-term behavior (the second one decays faster).

## Exercises

Here are some exercises that deals with forces. Make sure you practice IDEA.

1. A point particle (mass $m$ = 1kg) is moving under the influence of a force $F$ with associated potential energy $V(x) = x^3 + x^2 -x +1$ ($V$ in Joules). The problem is one-dimensional.

    The particle also experiences a friction force, that is porportional to the velocity of the particle. The proportionality constant equals 1 (in S.I. units).

    * Find the equilibrium points for $m$ and determine whether they are stable or unstable.

    * Find the oscillation frequency for the particle when it is slightly displaced from the stable point.

    * Determine whether the particle will move in a sinusoidal but damped way or that it decays exponentially back to its equilibrium position.

1. Harmonic oscillator with damping. Click on the image to start de physlet.

[![](images/DampedOscillator.jpg){ width="400" }](https://qiweb.tudelft.nl/mecharela/Widgets/DampedOscillator.html)

## Do it youself

![MassSpringDIY](images/MassSpringDIY.jpg){ align="right" width="250"}

Find a rubber band and a mass.

Set up an experiment to find both the mass $m$, the spring constant $k$, and the damping coefficient $b$.

Don't forget to make a physics analysis first, a plan of how to find both $m$ and $k$.
