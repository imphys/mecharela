# Polar Coordinates

!!! summary "Learning goals"

    After this lesson, you will be able to:

    - Convert from Cartesian to polar coordinates and vice versa

    - Use vector notation

    - Compute inner and outer vector products

In many cases, using Cartesian coordinates is not the most suitable choice. Sometimes polar coordinates or spherical coordinates are a much more natural choice. Here we will deal with plane polar coordinates. Plane means that we work in a plane (say the $xy$-plane), polar indicates that we do not use $(x,y)$ but $(r, \phi )$ to denote position.

![cartesian](images/CartesianPolar.jpg){ align="right" width="250"}

We represent a point $P$ by either $(x,y)$ or $(r, \phi )$. Note that this means that the position vector $\vec{r}$ that indicates point $P$ has Cartesian coordinates $(x,y)$ and as a vector is given by $\vec{r} = x\hat{x} + y\hat{y}$.

We have introduced two unit vectors at right angles (hence they form an orthonormal pair). The recipe for finding point $P$ in Cartesian coordinates is the following: start at the origin and walk $x$ times a unit vector in the $x$-direction. Stop there and follow by walking $y$ times a unit vector in the $y$-direction.

---

We can write the same vector $\vec{r}$ in polar coordinates. To begin with, we define the unit vector in the $r$-direction, that is a vector  $\hat{r}$ of length 1 and parallel to $\vec{r}$. We also define the unit vector $\hat{\phi}$, which is a unit vector perpendicular to $\hat{r}$. These two form an orthonormal pair. Now we can find point $P$ or denote the vector $\vec{r}$ as follows.

First, draw a unit vector $\hat{r}$ from the origin at an angle $\phi$ with the $x$-axis, then walk $r$ times this unit vector, and you will arrive at $P$. Thus the vector $\vec{r}$ can also be written as $\vec{r} = r \hat{r}$.

Note that it seems that this way of writing only needs one coordinate, i.e., $r$, but that is, of course, misleading. In order to know the direction of $\hat{r}$, we need the second coordinate $\phi$.

Thus, we have two equivalent ways or denoting $\vec{r}$:

$$
\vec{r} = x\hat{x} + y\hat{y} = r\hat{r}
$$

The coordinates are transformed into each other via:

$$
\left . \begin{array}{ll} x = r \cos \phi \\
             y = r \sin \phi\end{array} \right \} \leftrightarrow \left \{ \begin{array}{ll} r = \sqrt{x^2 + y^2} \\
             \phi = \arctan (y/x) \end{array} \right .
$$

It is also necessary to be able to transform both sets of unit vectors into each other. From the drawing on the right you can figure out that this goes as following:

$$
\left . \begin{array}{ll} \hat{r} = \cos \phi \hat{x} + \sin \phi \hat{y} \\
            \hat{\phi} = -\sin \phi \hat{x} + \cos \phi \hat{y} \end{array} \right \} \leftrightarrow \left \{ \begin{array}{ll} \hat{x} = \cos \phi \hat{r} - \sin \phi \hat{\phi} \\
             \hat{y} = \sin \phi \hat{r} + \cos \phi \hat{\phi} \end{array} \right .
$$

If you are familiar with matrices and rotation, you can notice that indeed the corresponding matrix is $\left ( \begin{matrix}\cos \phi & -\sin \phi \\ \sin \phi &\cos \phi \end{matrix} \right )$.

The expression therefore can be written as following:

$$
\begin{bmatrix}\hat{x} \\
\hat{y} 
\end{bmatrix} 
=
\begin{bmatrix}\cos \phi & - \sin \phi \\
\sin \phi & \cos \phi 
\end{bmatrix}  
\begin{bmatrix}\hat{r} \\
\hat{\phi} \end{bmatrix} 
$$

---

Since we are dealing with mechanics, we also need to have a representation of velocity and acceleration in polar coordinates.

That is quite straightforward from the definition:

$$
\vec{v} \equiv \frac{d\vec{r}}{dt} = \frac{d}{dt} \left ( r \hat{r} \right ) = \frac{dr}{dt} \hat{r} + r \frac{d\hat{r}}{dt}
$$

Before proceeding: we have here the time derivative of the unit vector $\hat{r}$. But isn't that zero? Unit vectors don't change over time, only coordinates do, isn't is? Wrong. The polar unit vectors are defined using the coordinate $\phi$, and this coordinate may certainly change over time.

---

We can find out what $\frac{d\hat{r}}{dt}$ is by going back to Cartesian unit vectors - they don't change with time: they are fixed in space, doing the time derivative and transforming back:

$$
\begin{array}{l} \frac{d\hat{r}}{dt} &= \frac{d}{dt} \left ( \cos \phi \hat{x} + \sin \phi \hat{y} \right ) \\
            &= -\sin \phi \frac{d\phi }{dt} \hat{x} + \cos \phi \frac{d\phi }{dt} \hat{y} \\
            &= \frac{d\phi }{dt} \left ( -\sin \phi \hat{x} + \cos \phi \hat{y} \right ) \\
            &= \frac{d\phi }{dt} \hat{\phi}
            \end{array}
$$

Similarly, for $\frac{d\hat{\phi}}{dt}$:

$$
\begin{array}{l} \frac{d\hat{\phi}}{dt} &= \frac{d}{dt} \left ( -\sin \phi \hat{x} + \cos \phi \hat{y} \right ) \\
            &= -\cos \phi \frac{d\phi }{dt} \hat{x} - \sin \phi \frac{d\phi }{dt} \hat{y} \\
            &= -\frac{d\phi }{dt} \left ( \cos \phi \hat{x} + \sin \phi \hat{y} \right ) \\
            &= -\frac{d\phi }{dt} \hat{r}
            \end{array}
$$

---

We summarize

$$
\vec{v} \equiv \frac{d\vec{r}}{dt} = \frac{dr}{dt} \hat{r} + r \frac{d\phi}{dt} \hat{\phi}
$$

and we thus have two equivalent ways of representing the velocity:

$$
\begin{array}{l} \vec{v} &= \frac{dx}{dt}\hat{x} + \frac{dy}{dt}\hat{y} = v_x \hat{x} + v_y \hat{y}\\
            &= \frac{dr}{dt} \hat{r} + r \frac{d\phi}{dt} \hat{\phi} = v_r \hat{r} + v_\phi \hat{\phi}
            \end{array}
$$

---

Does it make any sense that $\frac{d\hat{r}}{dt} = \frac{d\phi}{dt}\hat{\phi}$?

![cartesian2](images/drhatdt.jpg){ align="right" width="300"}

Yes, it does. If the position vector changes, then in general the angle $\phi$ will change as well. That will change the orientation (not the length) of $\hat{r}$.

In a time interval $dt$, the position changes from $(r,\phi )$ to $(r+dr, \phi + d\phi )$. Hence, the unit vector $\hat{r}$ changes its angle by $d\phi$. Its length, obviously, stays the same: it is a unit vector. Thus, it will only change in a direction perpendicular to itself, that is in the direction of $\hat{\phi}$.

$$
\hat{r}(t+dt) = \hat{r}(t) + 1\cdot d\phi \hat{\phi} \rightarrow
$$

$$
\frac{d\hat{r}}{dt} = \lim_{dt \rightarrow 0} \frac{\hat{r}(t+dt) - \hat{r}(t)}{dt} = \frac{d\phi}{dt} \hat{\phi}
$$

## Inner & Outer Product in Polar Coordinates

![outer_product](images/OuterProductUnitVectors.jpg){ align="right" width="300"}

In order to work with e.g. the angular momentum, it is useful to be able to work out outer products between two vectors in polar coordinates.

The unit vectors we use are orthonormal vectors, i.e., all have length 1 and are mutually at right angles. That is: Cartesian ones are, and polar ones are, but when they get mixed, they are not necessarily perpendicular. A few simple rules between the unit vectors suffice for inner and outer products.

$$
\begin{array}{ll} 
              &\hat{x} \cdot \hat{x} = 1 & \hat{x} \cdot \hat{y} = 0 & \text{etc.} \\
              &\hat{r} \cdot \hat{r} = 1 & \hat{r} \cdot \hat{\phi} = 0 & \text{etc.} \\
              &\hat{x} \times \hat{y} = \hat{z} & \hat{y} \times \hat{z} = \hat{x} & \hat{z} \times \hat{x} = \hat{y} \\
              &\hat{r} \times \hat{\phi} = \hat{z} & \hat{\phi} \times \hat{z} = \hat{r} & \hat{z} \times \hat{r} = \hat{\phi}
            \end{array}
$$

## Exercises

It is essential to master working with polar coordinates. So, practice and make sure that when you have to solve a classical mechanics problem, you don't struggle with the polar coordinates at the same time.

1. Show that for a particle (starting from rest) moving at a constant angular acceleration $\alpha$ along a circle, its acceleration is given by $\vec{a} =-r \alpha^2 t^2 \hat{r} + \alpha r \hat{\phi}$. Consequently, the radial part of the force responsible for this accelerated circular motion becomes time-dependent. Whereas the tangential part is constant.

1. Use the rules for outer products of the unit vectors to show that in a polar coordinate system, the angular momentum for a particle moving in the $xy$-plane can be written as $\vec{l} = m r^2 \frac{d\phi}{dt} \hat{z}$.
