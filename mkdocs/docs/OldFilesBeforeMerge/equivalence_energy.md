# Equivalence Energy and Newton's 2nd Law

!!! summary "Learning goals"

    After this lesson, you will be able to:

    - Relate energy conservation and Newton's 2^nd^ law

From Newton's 2nd law, we derived the concept of work and energy. From the concept of conservation of energy, we can go back to Newton's 2nd law. The two ways of describing the physics of a problem are equivalent: it is up to you to choose which one you prefer for a given problem. Sometimes you need both.

It is easy to derive from the energy conservation N2:

$$
\frac{1}{2}m\vec{v}^2 + V = const \text{ take the time derivative of this equation} \rightarrow
$$

$$
m\vec{v} \cdot \frac{d\vec{v}}{dt} + \frac{d}{dt}V(\vec{r}) = 0
$$

Use the chain rule for the time derivative of the potential energy:

$$
\frac{d}{dt}V(\vec{r}) = \frac{dV(\vec{r})}{d\vec{r}} \cdot \frac{d\vec{r}}{dt} = \frac{dV(\vec{r})}{d\vec{r}} \cdot \vec{v}
$$

$$
m\vec{v} \cdot \frac{d\vec{v}}{dt} + \frac{d}{dt}V(\vec{r}) = 0 \rightarrow 
$$

$$
\left ( m \frac{d\vec{v}}{dt} + \frac{d}{d\vec{r}}V(\vec{r}) \right ) \cdot \vec{v} = 0
$$

This must hold for any moment and for any situation as we have not specified anything. $\vec{v}$ is not always zero. Nor are the two terms always perpendicular. So, the term in brackets must be zero:

$$
m \frac{d\vec{v}}{dt} + \frac{d}{d\vec{r}}V(\vec{r}) = 0
$$

According to the definition of the potential energy, it is connected to force as $\frac{d}{d\vec{r}}V(\vec{r}) = -\vec{F}$ and we get N2 back:

$$
m \frac{d\vec{v}}{dt} + \frac{d}{d\vec{r}}V(\vec{r}) = 0 \rightarrow m \frac{d\vec{v}}{dt} = -\frac{d}{d\vec{r}}V(\vec{r}) \rightarrow \boxed{m \frac{d\vec{v}}{dt} = \vec{F}}
$$

Obviously, the above is done for conservative forces. What if some of the forces are not conservative? Then an additional term appears in the energy equation: no longer is energy conserved in the sense that the sum of kinetic and potential energy is the same during the process. But now there are forces that do not have an associated potential, but they still can do work. Thus the energy equation is modified by a term $W = \int \vec{F}_{non.cons} \cdot d\vec{r}$.

Taking the time derivative actually means comparing the situation between time $t$ and $t + dt$. As $t$ is infinitesimal small, we can safely assume that the force under the integral is a constant. Thus the amount of work done during $dt$ is: $dW = \vec{F}_{non.cons} \cdot d\vec{r}$.

In this time interval de change of kinetic and potential energy is $dE_{kin} + dV$ and we get that in $dt$ the energy is changed according to:

$$
\text{in } dt: dE_{kin} + dV = \vec{F}_{non.cons} \cdot d\vec{r}
$$

or:

$$
\frac{dE_{kin}}{dt} + \frac{d}{dt}V = \vec{F}_{non.cons} \cdot \frac{d\vec{r}}{dt}
$$

Follow the steps presented above and you will get:

$$
m \frac{d\vec{v}}{dt} = \vec{F}_{cons} + \vec{F}_{non.cons}
$$

## Example

We will apply the above to a basic example: a point particle that drops from height $H$. Gravity is taken as the only force and we will use a constant $\vec{g}$. We need to find the moment that the particle hits the ground and with which velocity. We will start from an energy consideration.

Try it yourself before peeking at the <a href="https://qiweb.tudelft.nl/mecharela/Solutions/EnergyN2.pdf" target="_blank">solution</a>.
