# Scattering

!!! note "Textbook & Collegerama"
    - Chapter 10 : section 10.1, 10.2, 10.3 
    - TN1612TU_09

!!! summary "Learning goals"
    - Know the definition of cross section
    - Understand scattering in a central force field
    - Understand the Rutherford experiment & relation to Bohr's atom model

Scattering is used to investigate the microscopic, but mostly the smaller atomic and sub-atomic structure of matter. We cannot just look at it, but try to learn the composition and constitution of matter and particles by hitting them with other particles and then observe the result of this hitting. You will known [CERN](https://home.cern/){ target="_blank"} where everything they do is scattering all day long.

The interaction when studying scattering is nearly always central, there is a [central force](central_forces.md) interacting with the particles we shoot at the target. You can abstract it like in the figure.

![scattering in 1D](images/scatt1.png){ align="right" width="400"}

In scattering experiments, we are looking for a relation between $b$, the *impact parameter* and the scattering angle $\theta$ to learn something about the nature of the interaction.

In 3D the scattering can be abstracted as follows, with the use of cylindric coordinates. As the impact parameter $b$ is unknown in experiments, we try to find a relation between the number of particles through an annular of size $db$ and the number of scattered particles into a solid angle element $d\theta$ .

![scattering in 3D](images/scatt3.png){ align="right" width="400"}

The experiment has an *statistical* character all of a sudden! We cannot follow individual particles or trajectories, but only what is happening on average. This is about probabilities and because of the probabilistic nature, we will adapt introduce a *scattering cross section*. 

## Scattering cross section

We define the *differential* scattering cross section

$$
\sigma(\Omega)d\Omega = \frac{dN}{I}
$$

as the number of particles scattered into the solid angle element $d\Omega$ per unit time over the incident intensity $I$.

The *total* cross section follows by integration over the solid angle $\sigma_T = \int \sigma (\Omega) \,d\Omega$. The unit of the total cross section is [m$^2$] an area. It directly give you a feeling "how large" the interaction area is, and therefor how likely the interaction is happening.
For historic reasons that is often given in cm$^2$. For example the cross section for [neutrinos](https://en.wikipedia.org/wiki/Neutrino){ target="_blank"} is about $10^{-40}$ cm$^2$, very small indeed.

For symmetry around the axis of incident we use cylindrical coordinates $d\Omega = 2\pi \sin\theta\,d\theta$

$$
\sigma_T = 2\pi \int \sigma(\theta )\sin\theta \,d\theta
$$

To find $\sigma(\theta)$ as a function of $b,\theta$ we proceed in general as follows. The number of particles through the annuls of size $db$ is $dN = I \pi(b+db)^2-I\pi b^2 = I 2\pi b\,db$ where we ignore $db^2$ terms.  As the number of scattered particle is also $dN = I \sigma d\Omega$ we have

$$
\begin{array}{rcl}
2\pi b \, db &=& \sigma d\Omega = \sigma 2\pi \sin\theta \,d\theta\\
\Rightarrow \sigma (\theta ) &=& \frac{b}{\sin\theta}
\left | \frac{db}{d\theta} \right |
\end{array}
$$

for the differential cross section.

Side note: one "[barn](https://en.wikipedia.org/wiki/Barn_(unit)){ target="_blank"}" was used during WW II in the [Manhattan Project](https://en.wikipedia.org/wiki/Manhattan_Project){ target="_blank"} as code for a scattering cross section of $10^{-24}$ cm$^2$. This size roughly corresponds to the cross section of an Uranium nucleus for neutron capture. For obvious reasons that was classified information then. It is still used nowadays as a measure of cross sections in nuclear physics.

## Scattering at a central force

What we call now *Rutherford scattering* is in essence scattering by interaction with a central, conservative force of the type $-k/r^2$, with $k$ the interaction constant and $r$ the distance of the particle and the center of the force. The potential is given by $k/r$ and the angular momentum $l$ is conserved as the force is [central](central_forces.md). Electric or gravitational potential have this form.

In terms of equation of motion, this must be equivalent to [Kepler](kepler.md#xact-solutions-to-the-equation-of-motion) (and it is)

$$
\frac{1}{2}m\dot{r}^2+\frac{l^2}{2mr^2}+\frac{k}{r}=E
$$

We need to find the solution of this equation of motion, which can be done by [integration](kepler.md#xact-solutions-to-the-equation-of-motion) or reusing the solution from Kepler

$$
u(\phi) \equiv \frac{1}{r(\phi)}=-\frac{km}{l^2}+A \cos (\phi-\phi_0)
$$

Please note, we have not the angle $\theta$ that we want in relation to $b$ but some $\phi$ describing the orbit $r(\phi)$ as the distance to the scattering center (compare the figure). Remember that time reversal symmetry in Newton mechanics implies symmetry of the orbit around some angle $\phi_0$ which is the angle of closest approach. For the relation of the angles we have $\pi = \theta + 2\phi_0$. You can also argue that because the angular momentum is conserved $l=-bmV$ also $b$ must be the same for both asymptotes. 

![Coordinates scattering](images/scatt_CS.png){ width="450"}

We want to find an equation relating $b$ to $\theta$, that is a bit involved, but outlined below.

From the orbit at large distance ($\phi =0$) we can find 

$$
A \cos\phi_0 = \frac{km}{l^2} \quad (*)
$$

and for the velocity $V$

$$
\frac{du}{d\phi} = \frac{du}{dt}\frac{dt}{d\phi} = -\frac{1}{r^2}\dot{r} \frac{mr^2}{l}=-\frac{\dot{r}m}{l}=\frac{mV}{mbV}=\frac{1}{b} 
$$

at large distance ($\phi =0$) we find

$$
\frac{du}{d\phi}(\phi=0) = -A \sin (-\phi_0) =\frac{1}{b} \quad (**)
$$

Dividing equations $(*)$ and $(**)$ we arrive at $\cot(\phi_0) = \frac{k}{mbV^2}$

Using the relation $\theta=\pi-2\phi_0$ and $\tan \frac{\theta}{2} = \cot \phi_0$ we finally arrive at

$$
\tan \frac{\theta}{2} = \frac{k}{mbV^2}
$$

Therefore we can learn something about $k$ by measuring $\theta$, the angular distribution. That is what Rutherford did.

## Rutherford & the nucleus

Probably the most famous scattering experiment is what is now called [Rutherford scattering](https://en.wikipedia.org/wiki/Rutherford_scattering){ target="_blank"}. It was first conducted around 1910 by [Geiger](https://en.wikipedia.org/wiki/Hans_Geiger){ target="_blank"}, [Marsden](https://en.wikipedia.org/wiki/Ernest_Marsden){ target="_blank"} and [Rutherford](https://en.wikipedia.org/wiki/Ernest_Rutherford){ target="_blank"} and led eventually to the development of the Bohr-Sommerfeld model for the atom. This is still largely valid today: a heavy nucleus consisting out of protons and neutrons and electrons that surround the nucleus in quantized shells. As a consequence of their experiment it was found that the space an atom takes up ($\sim 10^{-10}$ m), is much much larger than the size of where the positive charge is concentrated ($\sim 10^{-15}$ m). This lead to the term *nucleus* eventually. 

In the experiment they shoot a tight beam of $\alpha$-particles at thin gold foil. At the time it was not known that $\alpha$-particles are Helium nuclei, but the charge of $+2e$ was known. The spatial constitution of the (gold) atom was also unknown. It was known that gold is neutral, therefore the positive and negative charge must be compensated. As the electrons could be peeled off one by one, the charge was known (nearly correctly to be $+79e$). The questions was, how is the positive charge distributed over the atom? The stability of the atom with moving electrons and protons was a large miracle as the moving confined charges should loose energy by [Bremsstrahlung](https://en.wikipedia.org/wiki/Bremsstrahlung){ target="_blank"}.

The observations of Rutherford and his team revealed that i) most of the $\alpha$-particles went through the thin foil nearly without any deflection (very small scattering angle) and ii) some $\alpha$-particles where scattered at very high angles, some even recoiled.

The common hypothesis for the positive charge distribution was the [plum pudding model](https://en.wikipedia.org/wiki/Plum_pudding_model){ target="_blank"} introduced by [Thompson](https://en.wikipedia.org/wiki/J._J._Thomson){ target="_blank"} (who was the Ph.D. advisor of Rutherford) which expects the charge to be uniformly distributed over the whole atom size. This turned out to be wrong, because a uniform distribution of charge $+79e$ over a size of $r_0\sim 10^{-10}$ m cannot generate a large enough momentum change $\Delta p =F \Delta t$ to cause the large observed deflection angles. With the maximal force

$$
F_{Colomb} = \frac{1}{4\pi\epsilon_0} \frac{79e \cdot 2e}{r_0^2}  
$$

and the time a $\alpha$-particle is in the size of the atom $r_0\sim 10^{-10}$ m: $\Delta t=\frac{2r_0}{V_0}=1\cdot 10^{-17}$ s. The velocity of the used $\alpha$-particles resulting from a nuclear decay are about $V_0=1.6\cdot 10^7$ m/s.

Therefore 

$$
\frac{\Delta p}{p} = \frac{F_C \Delta t}{p} = 2\cdot 10^{-4} \ll 1
$$

You can only obtain larger changes in the deflection angle, if the quantity $r_0$ is much smaller. The force scales as $1/r_0^2$ while the time $\Delta t$ scales with $r_0$. There are no other quantities to change. In conclusion the positive charge, must be concentrated in a very tiny nucleus (compared to the size of the atom).

Deflection of the $\alpha$-particles by the electrons cannot happen as the electrons are much lighter ($10^4\times$) than the $\alpha$-particles .

---

As we have found a relation between $b$ and $\theta$ as $b=\frac{k}{mV^2\tan\frac{\theta}{2}}$ we can also compute the differential cross section, depending on the mass $m$ of the particles, the velocity $V$ and the interaction constant $k$ for Rutherford scattering

$$
\sigma(\theta) = \left ( \frac{k}{2mV^2} \right )^2 \frac{1}{\sin^4\frac{\theta}{2}}
$$ 
