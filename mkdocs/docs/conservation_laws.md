# Conservation Laws & Equivalence Energy

!!! note "Textbook & Collegerama"
    - Chapter 2: sections 2.0 - 2.2
    - TN1612TU_05

!!! summary "Learning goals"
    - Derive conservation laws from Newton's laws
    - Relate energy conservation and Newton's 2^nd^ law

From Newton's laws, we can derive several conservation laws. These are very important in physics. They tell us that no matter what happens, certain quantities will be present in the same amount: they are *conserved*. Conservation of energy follows the concept of work and potential energy. Conservation of momentum is a direct consequence of N2 and N3, as we will see below. And finally, under certain conditions, angular momentum is also conserved.

## Conservation of Energy

As we have seen when deriving the concept of potential energy, for a system with conservative forces the total amount of kinetic and potential energy of the system is constant. We can formulate that in a short way as:

$$
\sum{E_{kin}} + \sum{V} = const
$$

If non-conservative forces are present, the right hand side of the equation should be replaced by the work done by these forces.

$$
 \sum{E_{kin}} + \sum{V} = \sum W
$$

In many cases this will lead to heat, a central quantity in thermodynamics and another form of energy. The "loss" of energy goes always to heat.

## Conservation of Momentum

Consider two particles that mutually interact, that is they exert a force on each other. For each particle we can write down N2:

$$
\left . \begin{array}{ll} \frac{d\vec{p}_1}{dt} = \vec{F}_{21} \\
         \frac{d\vec{p}_2}{dt} = \vec{F}_{12} = - \vec{F}_{21}\end{array} \right \} \rightarrow \frac{d}{dt} \left ( \vec{p}_1 + \vec{p}_2 \right ) = 0 \Rightarrow \vec{p}_1 + \vec{p}_2 = const
$$

The total (linear) momentum is conserved if only internal forces are present; "action-reaction pairs" always cancel out.

## Conservation of Angular Momentum

Also angular momentum can be conserved. According to its governing law $\frac{d\vec{l}}{dt} = \vec{r} \times \vec{F}$ it might seem that we can for two interacting particles again invoke N3 "action = -reaction" and the terms with the forces will cancel out. But we need to be a bit more careful, as outer products are involved which are bilinear. So, let's look at the derivation of "conservation of angular momentum" for two interacting particles:

$$
\left . \begin{array}{ll} \frac{d\vec{l}_1}{dt} = \vec{r}_1 \times \vec{F}_{21} \\
         \frac{d\vec{l}_2}{dt} = \vec{r}_2 \times \vec{F}_{12} = -\vec{r}_2 \times  \vec{F}_{21}\end{array} \right \} \rightarrow \frac{d}{dt} \left ( \vec{l}_1 + \vec{l}_2 \right ) = \left ( \vec{r}_1 -\vec{r}_2 \right ) \times \vec{F}_{21}
$$

As we see, this is only zero if the vector $\vec{r}_1 -\vec{r}_2$ is parallel to the interaction forces. This particular case is called *central force*. We pick up this in more detail [in the next section](central_forces.md). Luckily, in many cases the interaction force works over the line connecting the two particles (e.g. gravity). In those cases, the angular momentum is conserved. Mathematically we can write this as:

$$
 \text{if } \vec{F}_{21}\, ||\, (\vec{r}_1 -\vec{r}_2)\ \Rightarrow \ \vec{l}_1 + \vec{l}_2  = const
$$

## Equivalence Energy and N2

From N2, we derived the concept of work and energy. From the concept of conservation of energy, we can go back to N2. These two ways of describing the physics of a problem are equivalent: it is up to you to choose which one you prefer for a given problem. 

We derive N2 from the energy conservation as follows:

$$
\frac{1}{2}m\vec{v}^2 + V(\vec{r}) = const  
$$

Now we take the time derivative

$$
m\vec{v} \cdot \frac{d\vec{v}}{dt} + \frac{d}{dt}V(\vec{r}) = 0
$$

Using the chain rule for the time derivative of the potential energy:

$$
\frac{d}{dt}V(\vec{r}) = \frac{dV(\vec{r})}{d\vec{r}} \cdot \frac{d\vec{r}}{dt} = \frac{dV(\vec{r})}{d\vec{r}} \cdot \vec{v}
$$

We obtain

$$
\Rightarrow \left ( m \frac{d\vec{v}}{dt} + \frac{d}{d\vec{r}}V(\vec{r}) \right ) \cdot \vec{v} = 0
$$

This must hold for any moment in time and for any situation as we have not specified anything, $\vec{v}$ is not always zero nor are the two terms always perpendicular. Therefore, the term in brackets must be zero:

$$
m \frac{d\vec{v}}{dt} + \frac{d}{d\vec{r}}V(\vec{r}) = 0
$$

According to the definition of the potential energy, it is connected to force as $\frac{d}{d\vec{r}}V(\vec{r}) \equiv \nabla V(\vec{r}) = -\vec{F}(\vec{r})$ and we get N2 back:

$$
m \frac{d\vec{v}}{dt} = -\frac{d}{d\vec{r}}V(\vec{r}) \Rightarrow m \vec{a} = \vec{F}
$$

Obviously, the above is done for conservative forces. What if some of the forces are not conservative? Then an additional term appears in the energy equation: no longer is energy conserved in the sense that the sum of kinetic and potential energy is the same during the process. But now there are forces that do not have an associated potential, but they still can do work. Thus the energy equation is modified by a term $W = \int \vec{F}_{non.cons} \cdot d\vec{r}$.

Taking the time derivative actually means comparing the situation between time $t$ and $t + dt$. As $dt$ is infinitesimal small, we can safely assume that the force under the integral is a constant. Thus the amount of work done during $dt$ is: $dW = \vec{F}_{non.cons} \cdot d\vec{r}$.

In this time interval the change of kinetic and potential energy is $dE_{kin} + dV$ and we get that in $dt$ the energy is changed according to:

$$
\text{in } dt: dE_{kin} + dV = \vec{F}_{non.cons} \cdot d\vec{r}
$$

or:

$$
\frac{dE_{kin}}{dt} + \frac{d}{dt}V = \vec{F}_{non.cons} \cdot \frac{d\vec{r}}{dt}
$$

Follow the steps presented above and you will get:

$$
m \vec{a} = \vec{F}_{cons} + \vec{F}_{non.cons}
$$

### Integrating the equation of motion in 1D

From the conservation of energy we can now derive the solution to the equation of motion for a general 1D case.

$$
\frac{1}{2}m\dot{x}^2+V(x)=E=const.
$$

This gives us a first order differential equation for $x(t)$

$$
\frac{dx}{dt} = \sqrt{\frac{2}{m}(E-V(x))}
$$

with solution (by separation of variables: we multiple $dt$ to the right side and divide by rest to left side, then integrate)

$$
t-t_0 = \sqrt{\frac{m}{2}}\int_{x_0}^{x(t)} \frac{dx'}{\sqrt{E-V(x')}}
$$

*If* we can integrate the above for a given potential, we directly find the equation of motion.

##Conservation laws and symmetry

Later you will see that the 3 basic conservation laws are linked to symmetry. 

* The conservation of energy is linked to the *homogeneity of time*. I.e. a mechanical system does not change as function of time.
* The conservation of linear momentum is linked to the *homogeneity of space*. I.e. a mechanical system can be parallel displaced/shifted without changing it.
* The conservation of angular momentum is linked to the *isotropy of space*. I.e. the properties of a closed system are not changed by rotating it as a whole.

Theses connections can be easily shown using the [Lagrange formalism](beyondNewton.md#lagrange-formalism) that you will learn in the second year course *Classical Mechanics*.

## Exercises

We will apply the above to a basic example: a point particle that drops from height $H$. Gravity is taken as the only force and we will use a constant $\vec{g}$. We need to find the moment that the particle hits the ground and with which velocity. Start from an energy consideration. You have done this before using N2.

Try it yourself before peeking at the [solution](Solutions/EnergyN2.pdf){ target="_blank" }.
