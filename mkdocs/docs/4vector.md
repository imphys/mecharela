# Spacetime and 4-vectors

!!! note "Textbook & Collegerama"
    - Chapter 13 : section 13.1 - 13.9
    - TN1612TU_27

!!! summary "Learning goals"
    - Know the inner product in Minkowski space
    - Know that the inner product of 4-vectors is LT invariant by construction
    - Understand that the form of the inner product follows from the requirement that distance must be LT invariant
    - Understand the light cone & causality
    - Work with Minkowski diagrams
    
## Space time

In 3D space we define a point/coordinate by its components $(x,y,z)$ where all components have the same unit. We can do this also in 4D space time by an event $(ct,x,y,z)$ as $ct$ has unit length (it should be called *time space* by this ordering, but what ever). The same unit for all components is needed if we want to do geometry with the coordinates.

If we want to measure distances $\Delta s$ between two points $(x_1,y_1,z_1)$ and $(x_2,y_2,z_2)$ we do this in 3D Euclidean space as $\Delta s^2 = (x_2-x_1)^2+(y_2-y_1)^2+(z_2-z_1)^2 = \Delta x^2 + \Delta y^2 + \Delta z^2$. These distances are Galileo invariant, observer $S$ and $S'$ moving with $\vec{V}$ measure the same distance $\Delta s^2 = \Delta s^{'2}$. If we want to measure distances in space time and require that the distance is now Lorentz invariant, we cannot measure distance the same way! To do geometry, measure angles etc. we need an inner product and the inner product induces a distance measure (a metric) by the norm. For 3D you know that for two vectors $\vec{r}_1$ and $\vec{r}_2$ as $\Delta s^2 = || \vec{r}_1-\vec{r}_2 || ^2 = (\vec{r}_1-\vec{r}_2)\cdot (\vec{r}_1-\vec{r}_2)= \Delta x^2 + \Delta y^2 + \Delta z^2$. Also the inner product in 4D space time cannot be the same clearly.

We want that two relativistic observes measure the same distance, that is, it must be Lorentz invariant. We start by noting that the speed of light is constant for both observers. A light wave traveling in $S$ and $S'$ must therefore obey

$$
c^2t^2-x^2-y^2-z^2 = 0 = c^2t^{'2}-x^{'2}-y^{'2}-z^{'2}
$$

NB: The Lorentz transformation [followed directly](lorentz.md#derivation-of-the-lorentz-transformation) from the requirement of invariance of the form of a spherical wave $c^2t^2-x^2-y^2-z^2 = 0$.

Given this observation it is needed (and natural) to define the distance in space time as

$$
d s^2 = c^2d t^2 - d x^2 - d y^2 - d z^2 
$$

!!! important "Warning"
	Notice directly that the distance $\Delta s^2$ can be negative! (we are OK with that)


### 4-vector

We define a 4-vector $\vec{A}=A^\mu=(A^0,A^1,A^2,A^3)$ to be a vector that transforms  between two observers $S$ and $S'$ moving with $V$ along $x$ by the LT

$$
\begin{array}{rcl}
A^{0'} &=& \gamma \left ( A^0-\frac{V}{c}A^1\right ) \\
A^{1'} &=& \gamma \left ( A^1-\frac{V}{c}A^0\right ) \\
A^{2'} &=& A^2\\
A^{3'} &=& A^3
\end{array}
$$

Other tuples of 4 values are not 4-vectors. The requirement that the 4-vector must transform via the LT is essential. We will use this later for the [4-velocity](4impuls.md#4-velocity) and [4-momentum](4impuls.md#4-momentum).

### Inner product & conventions

From the distance also the inner product can be defined between two *4-vectors*. We use a capital letter for a 4-vector $\vec{A} = A^\mu = (A^0,A^k)=(A^0,A^1,A^2,A^3)$. This notation is just to a make clear distinction with 3-vectors that only have spatial coordinates. With a Greek index $\mu, A^\mu$ we indicate all 4 components of the vector, while with a Latin index $k,A^k$ we only indicate the spatial components. We also start counting at 0 for the first component, which is time.

The inner product between two 4-vectors $\vec{A}, \vec{B}$ is now defined as

$$
\vec{A}\cdot \vec{B} \equiv A^0B^0 - A^1B^1-A^2B^2-A^3B^3
$$

This is not a "choice" for the inner product, but follows strictly from requirement that distance should not change under LT. A space with this inner product is called *Minkowski space* or the space has a *Minkowski metric* after [Hermann Minkowski](https://en.wikipedia.org/wiki/Hermann_Minkowski){ target="_blank"}.

Notice that time component $(+)$ is treated differently than the spatial components $(-)$ in the inner product. Sometimes the inner product is also called *pseudo Euclidean* as there are $-1$ and $+1$ present in the inner product (instead of only $+1$ for Euclidean space).

NB: The inner product in Euclidean space follows also directly from the requirement that the distance should remain the same under rotation. LT can be seen as rotations in Minkowski-space as [shown below](4vector.md#lt-as-a-rotation).


### Lorentz invariants

As clear by the above construction the inner product of two 4-vectors must be LT invariant, that is for observers $S:\vec{A},\vec{B}$ and $S':\vec{A}',\vec{B}'$ it holds

$$
\vec{A}\cdot \vec{B} = \vec{A}'\cdot \vec{B}' 
$$

This property can be a *very* powerful tool (OK, we constructed it that way). If we know the value of the inner product in one frame of reference, it will be the same in all other frames of reference! We will use that later often. It is also clear that the distance interval $ds^2$ is a Lorentz invariant.

??? "Inner product LT invariant: the hard way"

	If you do not believe that the inner product is LT invariant you can write it out of course (with $\beta \equiv \frac{V}{c}$).
	
	We compute $A'_0B'_0$
	
	$$
	\begin{array}{rcl}
	A'_0B'_0 &=& ct'_A ct'_B\\
	&=& \gamma^2 (ct_A-\beta x_A)(ct_B-\beta x_B) \\
	&=& \gamma^2 (ct_Act_B-ct_A\beta x_B -ct_B \beta x_A + \beta^2 x_Ax_B)
	\end{array}
	$$
	
	We compute $A'_1B'_1$
	
	$$
	\begin{array}{rcl}
	A'_1B'_1 &=& x'_Ax'_B\\
	&=& \gamma^2 (x_A-\beta ct_A)(x_B-\beta ct_B) \\
	&=& \gamma^2 (x_Ax_B - x_A \beta ct_B -x_B\beta ct_A + \beta^2 ct_A ct_B)
	\end{array}
	$$
	
	For $A'_0B'_0-A'_1B'_1$  four terms eliminate and we are left with
	
	$$
	\begin{array}{rcl}
	A'_0B'_0 - A'_1B'_1 &=& \gamma^2 (ct_A ct_B + \beta^2x_Ax_B) - \gamma^2 (x_A x_B + \beta^2 ct_A ct_B)\\
	&=& \frac{1}{1-\beta^2} \left [ ct_A ct_B(1-\beta^2) - x_Ax_B(1-\beta^2) \right ]\\
	&=& ct_A ct_B - x_A x_B
	\end{array}
	$$



## The light cone

Let us consider an event in space time $\vec{X}=X^\mu=(ct,x,y,z)=(x^0,x^1,x^2,x^3)$. For sake of simplicity we only consider one space like component here. In the sketch we have the space axis ($x$ or $x^1$) to the right and the time axis ($ct$ or $x^0$) up. We consider 3 events $A,B,C$ (points in space time) and their connection to the origin $O$

![Light cone](images/lightcone1.png){ align="right" width="260"}

- <span style="color:blue">OA</span>: The point $A$ can be reached from $O$ with velocity $v<c$, therefore it is called *causally connected* or *time like*.  For the distance $OA:\Delta s^2$, we see from projection of the coordinates $A$ onto the time and space axis $|x_A-0| < (ct-0) \Rightarrow \Delta s^2 >0$. Because the time component is larger than the space component, it is called *time like*. The distance is positive. 
- <span style="color:green">OB</span>: The point $B$ can be reached from $O$ only with velocity $v>c$, therefore it is called *non-causally connected* or *space like*.  For the distance $OB:\Delta s^2$, we see from projection of the coordinates $B$ onto the time and space axis $|x_B-0| > (ct-0) \Rightarrow \Delta s^2 <0$. Because the space component is larger than the time component, it is called *space like*. The distance is negative. 
- <span style="color:red">OC</span>: The point $C$ can be reached from $O$ only with velocity $v=c$, therefore it is called *light like* or *null*.  For the distance $OB:\Delta s^2$, we see from projection of the coordinates $C$ onto the time and space axis $|x_C-0| = (ct-0) \Rightarrow \Delta s^2 =0$. Because the space component is equal to the time component, it is called *light like*. The distance is zero. Therefore it is also called *null*.

Here you visually can observe that the sign of the distance using the Minkowski inner product classifies parts of space time.

![Light cone](images/lightcone.png){ width="350"}

This is even more evident if you look at the light cone in the sketch. The cone mantel is generated by resolving the line $x=ct$, a light line. Here only a 2D cone is shown $(ct,x,y)$, but of course this should be a 3D cone $(ct,x,y,z)$. The inside of the cone to negative times is the *past* that could have influenced me at *now*. My *now* can influence my *future* (inside the cone to positive times). All the rest, outside the cone is not causally connected to me.

## Minkowski-diagram

Now we can have a look at world lines of an observer $S'$ with respect to $S$ traveling with $V$ along the $x-$axis in a graphical manner. We plot the coordinate system of $S'$ (<span style="color:blue">blue</span>) in the coordinate system of S (black). 

![Minkowski](images/minkowski1.png){ align="right" width="350"}

- The time line of $S'$ in $S$ is given by the fact that $x'=0$. From the LT we have $x'=\gamma (x-\frac{V}{c}ct)=0 \Rightarrow x=\frac{V}{c}ct$. The angle $\alpha$ of the $ct'$-line with the $ct$ axis is given by $\tan \alpha = \frac{V}{c}$.
- The space line of $S'$ in $S$ is given by the fact that $ct'=0$. From the LT we have $ct'=\gamma (ct-\frac{V}{c}x)=0 \Rightarrow ct=\frac{V}{c}xt$. The angle $\alpha$ of the $x'$-line with the $x$ axis is given by $\tan \alpha = \frac{V}{c}$.

Both lines of $S'$ make the same angle $\alpha$ with the coordinates axis of $S$. They lie symmetric around the light line $x=ct$ (diagonal with $\alpha=45$ deg). The higher the speed $V$ the higher the angle and the closer the lines lie to the light line.

To further investigate how this plot can help us, let us consider lines of equal time in $S$. These are just the lines perpendicular to the $ct$-axis, and parallel to the $x$-axis, as you expect. (and of course, lines parallel to $ct$, perpendicular to $x$ are lines of constant space coordinate.

![Minkowski](images/minkowski2.png){align="right" width="250"}

For the frame of reference $S'$ that is only a bit different. 

- <span style="color:green">Lines of constant time</span> in $S'$ are parallel to $x'$
- <span style="color:red">Lines of constant space coordinate</span> in $S'$ are parallel to $ct'$

With this information in hand, we can investigate how events are transferred from $S$ to $S'$. We can graphically do a LT without the explicit computation.

See [here](https://commons.wikimedia.org/wiki/File:MinkBoost2.gif){ target="_blank"} for the CS of $S'$ in the frame of $S$ as a function of speed $V$.

### The ladder & barn revisited

We will now take a look back at the [ladder and barn paradox](timedilation.md#paradox-twins-and-barns). We had a barn of 10 m wide and a ladder of 26 m long (both measured in their rest frame). The ladder was moving towards the barn with high velocity. We start by drawing the barn $S$ (black) and ladder $S'$ (<span style="color:blue">blue</span>) coordinate systems in the Minkowski diagram. Now we add the barn world line into the diagram (light blue) with 2 lines of constant space coordinate (parallel to $ct$) in $S$.

![Minkowski barn & ladder 1](images/mink1.png){ width="300"}

Now we can add the <span style="color:red">ladder</span> to $S'$. It has rest length of 26 m and in the $(x',ct')$ CS it is a world line of constant space coordinate, therefore parallel to $ct'$. The ladder itself is a line of constant time in $ct'$ and therefore parallel to $x'$.

![Minkowski barn & ladder 2](images/mink2.png){ width="300"}

As the ladder moves (we move it parallel to $x'$ between the world lines) it will eventually enter the barn and hit the right door of the barn (dashed red line). This event is indicated by the space time point $A$. For $S'$ the other end of the ladder is then still outside the barn at space time point $C$. According to $S'$ the ladder does not fit into the barn.

![Minkowski barn & ladder 3](images/mink3.png){ width="300"}

When the ladder hits the right door for $S$ at space time point $A$, he makes a measurement of the ladder. To this end we draw a line of constant time (dashed light blue, parallel to $x$) until it intersects the world line of the ladder at space time point $B$. Observer $S$ measures that the ladder fits into the barn.

![Minkowski barn & ladder 4](images/mink4.png){ width="300"}

From this diagram it is obvious that the events $B$ and $C$ are not the same, therefore it is not strange that $S$ and $S'$ disagree about the outcome of the measurement. Both are right! But they would not be able to agree that both doors shut at the same time, to capture the ladder.

### The twin paradox

Let there be  two twins, Alice and Bob. Bob leaves earth in a space ship with relativistic speed $\vec{v}$, while Alice remains back home on earth. At some time Bob turns around, with $-\vec{v}$ and comes back to Alice. Based on time dilation Alice will argue that Bob is younger than her twin brother due to $\Delta T = \gamma \Delta T_0$. For the $\gamma$-factor it does not matter if Bob is moving away or approaching as it is quadratic in the velocity. For each year she ages, her brother only ages $1/\gamma$ years. Bob can argue that due to the principle of relativity, he is at rest and his twin sister is moving away and then coming back, therefore she will be younger than he - and we have a paradox.

This paradox has two issues:

1. The principle of relativity is not applicable as Bob must *turn around*. This requires acceleration of his frame and breaks the symmetry of the problem.
2. Bob will be younger than Alice, due to the relativity of simultaneity changing around the turning point. We can see this by looking at the Minkowski-diagram below. Just before Bob is turning around, his line of simultaneity is $x'$, but just after turning around his line of simultaneity is $x''$.  On the time line of Alice, Bob lines of simultaneity first is at point A, but then makes a jump around the turning point to B. Bob will be younger than Alice, by the length of this jump on her time line from A to B.

![Minkowski twin](images/mink5.png){ width="400"}

Extra: We symmetries the problem. Both Alice and Bob move in space ships away from each other at the same but opposite speed, then turn around and meet again. Who is older now?

??? "Answer"

	They are the same age. You can now reason with symmetry even though both are accelerated. You can also draw the Minkowski-diagram similar to the above and see that both make the same "jump" in the time, and thus are the same age.

### Worked example: the rabbit and the turtle

We consider the relativistic race between the well-known rabbit ($R$) with speed $v_R$ and his buddy turtle ($T$) with speed $v_T<v_R$. Both turtle and rabbit are point particles. To give turtle a chance, it does not need to run as far as rabbit $(L_T<L_R)$. The distances are chosen such that an observer at rest (the audience) records that $R$ and $T$ finish at the same time.

1. Draw a Minkowski-diagram of the situation described above. 
2. Indicate the following events in space time.
	- $R$ finishes in his frame (A)
	- $T$ finishes in his frame (B)
	- In the frame of $R$, when he finishes, the event where $T$ is then (C)
	- In the frame of $T$, when he finishes, the event where $R$ is then (D)
3. Who has won according to $R$ and who according to $T$. Do they agree?

Solutions:

We start by drawing the audience frame with $(ct,x)$ and an equal time line for the finish of $R$ and $T$. From that we draw the CS of $R$ as $(ct_R,x_R)$ and of $T$ as $(ct_T,x_T)$. As $v_T < v_R$, the CS $(ct_R,x_R)$ is closer to the light line. The length $L_R$ and $L_T$ follow as the intersections of $ct_R$ and $ct_T$ with the line of equal time for the audience. 

These intersections are also directly the events (A,B).

![Rabbit & Turtle 1](images/RT_1.png){ width="300"}

For the events C and D, we first draw from A a lines of constant time for $R$ (parallel to $x_R$) and then look at the intersection with the world line of $T$ and mark it with C. The same for the event D. We draw a line parallel to $x_T$ of constant time for $T$ through B to see where $R$ is when $T$ finishes and mark it with D.

![Rabbit & Turtle 2](images/RT_2.png){ width="300"}
 
Both $R$ and $T$ agree that $R$ has won, but the audience does of course not agree.

### Lines of invariant distance

We have seen that the length interval $ds^2$ is a Lorentz invariant. Therefore we can use it to also indicate corresponding time and space units in a Minkowski diagram for two moving observers. If we fix $ds^2$ then the equation $ds^2=c^2 dt^2-dx^2$ describes a hyperbola in $(ct,x)$ of the Minkowski diagram.

![Invariant Distance](images/invariantDs.png){ width="400" caption="asdf"}

Image from T. Idema, *Mechanics and Relativity*.

For $ds^2<0$ we find the corresponding space units (the interval is [space-like](4vector.md#the-light-cone)), and for $ds^2>0$ the corresponding time units (the interval is [time-like](4vector/#the-light-cone). All hyperbola have the light line $ds^2=0$ as asymptotes. 

## LT as a rotation

This part is optional, but insightful.

You can think of the LT as a rotation of the 4 coordinates of Minkowski space time. Obviously it is not a "normal" rotation with a [rotation matrix](https://en.wikipedia.org/wiki/Rotation_matrix){ target="_blank"} $R\in SO(n)$ as we encountered in change to [polar coordinates](central_forces.md#polar-coordinates). The book in chapter (11.7) does it quite a bit different, but we do not like the imaginary time axis there at all. 


The LT in matrix notation reads as follows with $\gamma = \frac{1}{\sqrt{1-\beta^2}}$ and $\beta=V/c$.

$$
\left ( \begin{array}{c}
ct' \\ x' \\ y' \\z'
\end{array} \right ) =
\left ( \begin{array}{cccc}
\gamma & -\gamma\beta & 0 & 0\\
-\gamma\beta & \gamma & 0 & 0\\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1\\
\end{array} \right )
\left ( \begin{array}{c}
ct \\ x \\ y \\z
\end{array} \right )
$$

The matrix transfers the space time coordinates between two observers moving with $V$. From this it is clear that transferring between more than two observers $S\to S' \to S^{''} \to \dots$ can be done easily by multiplying the respective Lorentz transformation matrices into one overall LT. This must be possible, of course, as the LT is a linear transformation in space time $(ct,x)$.

From the matrix notation it is also clear that for rotations around "different axis", speeds in $x,y,z$ direction, the order of change of frame matters as matrix multiplication does not commute.

In 3D normal space, distance is persevered under rotation with $R\in SO(n)$, in Minkowski space distance is preserved under Lorentz transformation which too is a rotation.

You can see the rotation clearer if we introduce the quantity *rapidity* $\alpha$, which is defined as $\tanh \alpha \equiv \frac{V}{c}$ (a relativistic generalization of the modulus of the velocity. It goes from 0 for $v=0$ to $\infty$ for $v=c$). We will not use the rapidity except here, however, it is used for relativistic velocity decompositions. With $\tanh \alpha = \frac{V}{c}$ we can write the Lorentz transformation as (using $\gamma = \frac{1}{\sqrt{1-\tanh^2 \alpha}}=\cosh \alpha$ and $\gamma\beta=\frac{\tanh\alpha}{\sqrt{1-\tanh^2 \alpha}}=\sinh\alpha$)

$$
\left ( \begin{array}{c}
ct' \\ x' \\ y' \\z'
\end{array} \right ) =
\left ( \begin{array}{cccc}
\cosh\alpha & -\sinh\alpha & 0 & 0\\
-\sinh\alpha & \cosh\alpha & 0 & 0\\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1\\
\end{array} \right )
\left ( \begin{array}{c}
ct \\ x \\ y \\z
\end{array} \right )
$$

Notice the similarity to the [rotation](central_forces.md#polar-coordinates) with sine and cosine.

With that LT is a rotation in hyperbolic space with "angle" $\alpha$ (where $\alpha$ is the rapidity), we identify the matrix as $L(\alpha$). That the [hyperbolic functions](https://en.wikipedia.org/wiki/Hyperbolic_functions#Useful_relations){ target="_blank"} appear should not be a surprise as they are they equivalent to the sine and cosine for the circle, $(ct^2+x^2=1)$, for the hyperbola $(ct^2-x^2=1)$. You notice the relation to the inner products for standard and Minkowski space.

Minkowski made the sketch below to show that the Lorentz transformation is a rotation over a hyperbola not a circle as we were used to. The asymptotes of the hyperbola are given by the light lines.

![Drawing by Minkowski](images/MinkDrawing.png){ width="500"}


The addition of velocities that we derived earlier is easy with this notation with rotations and rapidity $L(\alpha_1)L(\alpha_2)=L(\alpha_1+\alpha_2)$. In terms of speeds this reads

$$
\beta = \tanh (\alpha_1+\alpha_2)= \frac{\tanh \alpha_1 +\tanh \alpha_2}{1+\tanh \alpha_1 \tanh \alpha_2}=\frac{\beta_1 + \beta_2}{1+\beta_1\beta_2}
$$ 

The [addition of velocities](doppler.md) is brought back to [hyperbolic identities](https://en.wikipedia.org/wiki/Hyperbolic_functions#Useful_relations){ target="_blank"}. 


