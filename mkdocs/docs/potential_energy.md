# Potential Energy

!!! note "Textbook & Collegerama"
    - Chapter 2: sections 2.0 - 2.2
    - TN1612TU_03 first half & _04 & _05 first half

!!! summary "Learning goals"
    - Define conservative force and potential energy
    - Check if a force is conservative. Apply Stokes' theorem.
    - Identify stable/unstable equilibria
    - Understand the application of Taylor series to a potential around an equilibrium

## Conservative Force

![path](images/Path12.jpg){align="right" width = "100" }

Work done on a particle by $F$ during motion from point 1 to 2 over a prescribed trajectory is defined as:

$$
W_{12} = \int_1^2 \vec{F} \cdot d\vec{r}
$$

In general, the amount of work depends on the path followed. That is, the work done when going from $\vec{r}_1$ to $\vec{r}_2$ over the red path (see figure ) will be different when going from $\vec{r}_1$ to $\vec{r}_2$ over the blue path. Work depends on the specific trajectory followed.

However, there is a certain class of forces for which the path does not matter, only the start and end points do. The integral is path independent. These forces are called *conservative forces*. As a consequence, the work done by a conservative force over a closed path, i.e., start and end are the same, is always zero. No matter which closed path is taken.

Conservative force $\Leftrightarrow \oint \vec{F} \cdot d\vec{r} = 0$ *for all* closed paths.

## Potential Energy

A direct consequence of the path independence of the integral is, that a scalar valued function $V(\vec{r}):\mathbb{R}^3 \to \mathbb{R}$ exists such that 

$$
\int_1^2 \vec{F} \cdot d\vec{r} = -(V(\vec{r}_2) -  V(\vec{r}_1))
$$

with $\vec{F} = - \nabla V \equiv - \text{grad }V \equiv - (\partial_x V, \partial_y V, \partial_z V)$.

$$
\vec{F} = -\nabla V \Leftrightarrow V(\vec{r}) = -\int_{ref}^{\vec{r}} \vec{F} \cdot d\vec{r}'
$$

Where in the last integral, the lower limit is taken from some self-picked reference point. The upper limit is the position $\vec{r}$. Note: The minus sign in $\vec{F} = - \nabla V$ is of course arbitrary, but [later](potential_energy.md#Stability) you will see that this convention is "natural" and that now the sum of kinetic and potential energy stays constant.

This scalar valued function $V(\vec{r})$ is called the *potential energy*, or the *potential* for short. It has a direct connection to work and kinetic energy (which are also scalars of course):

$$
E_{kin, 2} - E_{kin, 1} = W_{12} = \int_1^2 \vec{F} \cdot d\vec{r} = -(V(\vec{r}_2) - V(\vec{r}_1))
$$

or:

$$
E_{kin, 1} + V(\vec{r}_1) = E_{kin, 2} + V(\vec{r}_2)
$$

In words, **for a conservative force, the sum of kinetic and potential energy stays constant.** Note that we can always add a constant to the potential with no consequence to the force (what matters for N2), that is we can choose the reference point as we wish.

The reason that there must exist a scalar potential if the integral is path independent can be proven in a number of ways, e.g. by the [Helmholtz decomposition](https://en.wikipedia.org/wiki/Helmholtz_decomposition){ target="_blank"} or [Fundamental theorem of calculus](https://en.wikipedia.org/wiki/Fundamental_theorem_of_calculus){ target="_blank"}.

## Stokes' Theorem

It was [George Stokes](https://en.wikipedia.org/wiki/Sir_George_Stokes,_1st_Baronet){ target="_blank"} who proved an important theorem, that we will use to identify properties of a conservative force:

$$
\oint \vec{F} \cdot d\vec{r} = \int_A \text{rot } \vec{F} \cdot \ d\vec{\sigma}
$$

In words: the integral of the force over a closed path equals the surface integral of the curl or rotation of that force; the surface $A$ is being 'cut out' by the closed path. Note: The right hand side of the above equation contains the inner product of the vector rot $\vec{F}$ and the surface normal vector $d\vec{\sigma}$.

The curl or rotation of a vector field is given in Cartesian coordinates as

$$
\text{rot } \vec{F}\equiv \text{curl } \vec{F}\equiv \nabla \times \vec{F}
$$

$$
=\left ( \begin{array}{c}\partial_x\\ \partial_y\\ \partial_z \end{array}\right ) \times \vec{F}= \left ( \begin{array}{c}\partial_y F_z - \partial_z F_y \\ 
- (\partial_x F_z - \partial_z F_x) \\ \partial_x F_y -\partial_y F_x \end{array}\right )
$$

For a conservative force the integral over the closed path is zero for *any* closed path. Thus we have:

$$
0=\oint \vec{F} \cdot d\vec{r} = \int_A \text{rot } \vec{F} \cdot \ d\vec{\sigma} \text{ for all paths, surfaces} \Leftrightarrow \text{rot } \vec{F}=0
$$

Conservative force $\Leftrightarrow \text{rot }\vec{F} = 0$ everywhere. This means we can check if a force (field) is conservative by computing the curl of the force.

---

You can also see that the curl of a conservative force must be zero, due to the existence of a scalar potential. $\vec{F}$ conservative $\Leftrightarrow \vec{F}=-\nabla V \Rightarrow \nabla \times (\nabla V)=0$. 

$$
\nabla \times \vec{F} = -(\partial_y\partial_z V - \partial_z\partial_y V, -(\partial_x\partial_z V - \partial_z\partial_x V), \partial_x\partial_y V - \partial_y\partial_x V)
$$

In the Electrodynamics class you will see this again as rot (grad $\varphi$)$=0$ as one of the [vector calculus identities](https://en.wikipedia.org/wiki/Vector_calculus_identities){ target="_blank"}.


## Worked Examples

1. Is gravity $\vec{F_g} = m\vec{g}$ a conservative force? If yes, what is the corresponding potential energy?
	
	To find the answer we can do two things:
	
	* show that $\nabla\times m\vec{g} = 0$
	
	* find a $V$ that satisfies $-m\vec{g} = -\nabla V$
	
	Is $\nabla \times m\vec{g} = 0$ ? How to compute it? For orthogonal coordinate systems (e.g. Cartesian or polar coordinates) there are 2 easy ways to remember how to compute the cross product:
	
	1. via the determinant
	
	    $$
	    \vec{\nabla} \times \vec{F} =
	    \left| \begin{array}{ccc}
	    \hat{x}&\hat{y}&\hat{z}\\
	    \frac{\partial}{\partial x}&\frac{\partial}{\partial y}&\frac{\partial}{\partial z}\\
	    F_x&F_y&F_z
	    \end{array}\right |
	    $$
	
	1. via computing the cross directly
	
	    Watch this video how to remember computing the cross product starting at 2:00 or remember it from the lectures.
	    [![path](images/crossproduct_trick.jpg){ align="center" width="300" }](https://www.youtube.com/watch?v=e9T5p_Jwv5c){ target="_blank" }
	
	If we chose our coordinates such that $\vec{g} = -g \hat{z}$ we get:
	
	$$
	\nabla \times \vec{F}_g = \left | \begin{array}{ccc}
	\hat{x}&\hat{y}&\hat{z}\\ 
	\partial_x & \partial_y &\partial_z\\
	0&0& -mg
	\end{array} \right | = 0
	$$
	
	Thus $\vec{F_g}$ is conservative.
	
	Secondly: does $-m\vec{g} = -\vec{\nabla} V$ have a solution for $V$? Let's try using the same coordinates as above.
	
	$$
	-\nabla V = - mg\hat{z}
	$$
	
	$$
	\begin{array}{rcl}
	\frac{\partial V}{\partial x} &=& 0 \Rightarrow V(x,y,z) = f(y,z)\\
	\frac{\partial V}{\partial y} &=& 0 \Rightarrow V(x,y,z) = g(x,z)\\
	\frac{\partial V}{\partial z} &=& mg \Rightarrow V(x,y,z) = mgz + h(x,y)
	\end{array}
	$$
	
	$f, g, h$ are unknown functions. But all we need to do is to find such a $V$ that satisfies $-m\vec{g} = -\vec{\nabla} V$. We have shown that gravity in this form is conservative and that we can take $V(x,y,z) = mgz$ for its corresponding potential energy.
	
	By the way: from the first part we know that the force is conservative and we know that we could try to find $V$ from:
	
	$$
	V(x,y,z) = -\int_{ref} m\vec{g} \cdot d\vec{r} = \int_{ref} mg \hat{z} \cdot d\vec{r} = \int_{ref} mg\, dz = mgz + const
	$$
	
2. The potential of gravity.
	
	See here for how to compute the [potential of gravity](https://qiweb.tudelft.nl/mecharela/central_forces/#integration) for the general form of the force $V(\vec{r}) =-\int -F \frac{m_1 m_2}{r}\hat{r}\cdot d\vec{s}$.
	
## Stable/Unstable Equilibrium

A particle (or system) is in equilibrium when the sum of forces acting on it is zero. Then, it will keep the same velocity, and we can easily find an inertial system in which the particle is at rest, at an equilibrium position. The same position (or more general state) can also be found directly from the potential energy.

Potential energy and (conservative) forces are coupled via: $\vec{F} = -\nabla V$.

The equilibrium positions ($\sum_i \vec{F}_i=0$) are found by finding the extremes of the potential energy:

$$
\text{ equilibrium position   } \Leftrightarrow \nabla V = 0
$$

### Stability

Once we find the stable points, we can also quickly address their nature: is it a stable or unstable solution? That follows directly from inspecting the characteristics of the potential energy around the equilibrium points.

For a stable equilibrium, we require that a small push or a slight displacement will result in a pushing back such that the equilibrium position is restored (apart from the inertia of the object that might cause an overshoot or oscillation).

However, an unstable equilibrium is one for which the slightest push or displacement will result in motion away from the equilibrium position.

The second derivative can be investigated to find the type of extremum. For 1D functions that is easy, for scalar valued functions of more variables that is a bit [more complicated](https://en.wikipedia.org/wiki/Second_partial_derivative_test). Here we only look at the 1D case $V(x):\mathbb{R}\to\mathbb{R}$

$$
\text{equilibrium: } \nabla V = 0  
\left \{ \begin{array}{ll}
&\text{stable: }& \frac{d^2}{dx^2} V\gt 0 \\
&\text{unstable: }& \frac{d^2}{dx^2} V\lt 0
\end{array} \right .
$$

Luckily, the definition of potential energy is such that these rules are easy to visualize in 1D and remember.

![StableUnstable](images/StableUnstable.jpg){ align="center" width="400" }

A valley is stable; a hill top is unstable. NB: Now the choice of the minus sign in the definition of the potential is clear $\vec{F}=-\nabla V$. Otherwise a hill would be stable, but that does not feel natural at all.

It is also easy to visualize what will happen if we distort that particle from the equilibrium state:

* The valley, i.e., the stable system, will make the particle move back to the lowest point. Due to inertia, it will not stop but will continue to move. As the lowest position is one of zero force, the particle will 'climb toward the other end of the valley and start an oscillatory motion.

* The top, i.e., the unstable point, will make the particle move away from the stable point. The force acting on the particle is now pushing it outwards, down the slope of the hill.

### Taylor Series Expansion of the Potential

The Taylor expansion or Taylor series is a mathematical approximation of a function in the vicinity of a specific point. It uses an infinite series of polynomial terms with coefficients given by the derivative value of the function at that specific point: the more terms you use, the better the approximation. If you use all terms, then it is exact. Mathematically, it reads for a 1D scalar function $f: \mathbb{R}\to\mathbb{R}$:

$$
f(x) = f(x_0) + \frac{1}{1!}f'(x_0) (x-x_0) + \frac{1}{2!}f^{''}(x_0) (x-x_0)^2 + \cdots
$$

For our purpose here, it suffices to stop after the second derivative term:

$$
f(x) = f(x_0) + f'(x_0) (x-x_0) + \frac{1}{2} f^{''}(x_0) (x-x_0)^2 +{\cal{O}}(x^3)
$$

---

A way of understanding why the Taylor series actually works is the following. Imagine you have to explain to someone how a function looks around some point $x_0$, but you are not allowed to draw it. One way of passing on information about $f(x)$ is to start by giving the value of $f(x)$ at some point $x_0$:

$$
f(x) \approx f(x_0)
$$

Next, you give how the tangent at $x_0$ is; you pass on the first derivative at $x_0$. The other person can now see a bit better how the function changes when moving away from $x_0$:

$$
f(x) \approx f(x_0) + f'(x_0) (x-x_0)
$$

Then, you tell that the function is not a straight line but curved, and you give the second derivative. So now the other one can see how $f(x)$ deviates from a straight line:

$$
f(x) \sim f(x_0) + \frac{1}{1!}f'(x_0) (x-x_0) + \frac{1}{2!}f^{"}(x_0) (x-x_0)^2
$$

Note that the prefactor is placed back. But the function is not necessarily a parabola; it will start deviating more and more as we move away from $x_0$. Hence we need to correct that by invoking the third derivative that tells us how fast this deviation is. And this process can continue on and on.

Important to note, if we stay close enough to $x_0$ the terms with the lowest order terms will always prevail as higher powers of $(x-x_0)$ tend to zero faster than a lower powers (remember $0.5^4\ll 0.5^2$).

Here is a [youtube movie](https://www.youtube.com/watch?v=3d6DsjIBzJ4&t=29s) that explains the 1D Taylor series nicely (for physicists).

---

For scalar valued functions as our potentials $V(\vec{r}):\mathbb{R}^3 \to \mathbb{R}$ the extension of the Taylor series is not too difficult. 
If we expand the function around a point $\vec{r}_0$

$$
V(\vec{r})=V(\vec{r}_0)+ \nabla V(\vec{r}_0) \cdot (\vec{r}-\vec{r}_0)+ \frac{1}{2}(\vec{r}-\vec{r}_0)\, (\partial^2 V)(\vec{r}_0)\, (\vec{r}-\vec{r}_0) + {\cal{O}}(r^3)
$$

The second derivative of the potential indicated by $\partial^2 V$ is the [Hessian matrix](https://en.wikipedia.org/wiki/Hessian_matrix){ target="_blank" }.

Conceptually the extrema of the function are again the hills and valleys (now with condition $\nabla V=0$, i.e. all derivatives must vanish at the extrema $\partial_x V_x = \partial_y V_y = \partial_z V_z=0$). The classification of the extrema has next to hills and valleys also saddle etc. For the classification you need to compute the determinate and trace of the Hessian matrix at that point. In this course we will not do this, but only stick to simple cases.

---

The Taylor series is *very* useful when studying the behavior of a particle around a stable equilibrium point. For such a point we have:

$$
\frac{d}{dx}V\equiv V'( x_{eq}) = 0 \text{  and  } V^{''}_{eq} \equiv V^{"}(x_{eq}) > 0
$$

Thus, if the particle stays close enough to the equilibrium point, we can write the potential as follows, ignoring higher orders:

$$
V(x) \sim V_{eq} + \frac{1}{2} V^{''}_{eq} (x-x_{eq})^2 
$$

This means to lowest non-constant order the potential around a stable equilibrium is given by a parabola in $x$ around $x_{eq}$. As we did not make any assumption about the potential this is true *for all* potentials.

The energy equation for a particle is

$$
\frac{1}{2}m\dot{x}^2 + V_{eq} + \frac{1}{2!} V^{''}_{eq} (x-x_{eq})^2 = E_0
$$

Taking the time derivative and simplifying to obtain the equation of motion:

$$
m\ddot{x} + V^{''}_{eq} (x-x_{eq}) = 0
$$

But this is the [harmonic oscillator](harmonic_oscillator.md)! **We find that for small deviations from a stable equilibrium point, _all_ systems will behave as a harmonic oscillator**. This basic result as major consequences for describing how any system reacts to small perturbations.

### Example

The widget below will show you the behavior close to and not-so-close to an equilibrium point of a given potential. Click on the image to start.

[![taylor expansion 2](images/TaylorExpansion2.jpg){ width="500" }](Widgets/TaylorExpansion2.html){ target="_blank" }

## Demos

Here you can see the conversion of potential into kinetic energy for particles in different potentials [Physics Roller Coaster](https://www.myphysicslab.com/roller/roller-single-en.html){ target="_blank" }

## Exercises

1. A simple model for the frictional force experienced by a body sliding over a horizontal, smooth surface is $F_f = -\mu F_g$ with $F_g$ the gravitational force on the object. The friction force is opposite to the direction of motion of the object.

    * Show that this frictional force is not conservative (and, consequently, a potential energy associated does not exist!).

    Hint: think of two different trajectories to go from point 1 to point 2 and show that the amount of work along these trajectories is not the same.

1. A force is given by: $\vec{F} = x\hat{x} + y\hat{y} + z\hat{z}$.

    * Show that this force is conservative.

    * Find the corresponding potential energy.

    Another force is given by: $\vec{F} = y\hat{x} + x\hat{y} + z\hat{z}$.

    * Show that this force is also conservative.

    * Find the corresponding potential energy.

1. Another force is given by: $\vec{F} = y\hat{x} - x\hat{y}$.

    * Show that this force is not conservative.

    * Compute the work done when moving an object over the unit circle in the $xy$-plane in an anti-clockwise direction.

        Hint: use Stokes' theorem.

    * Discuss the meaning of your answer: is it positive or negative? And what does that mean in terms of physics?

1. Another force is given by: $\vec{F}(x,y)=(-y,x^2)=-y \hat{x} +x^2 \hat{y}$. This is the force in the [Worked Example](work#worked-example) from the previous chapter.

    * Show that this force is not conservative.
