# Collisions

!!! note "Textbook & Collegerama"
    - Chapter 9: sections 9.1-9.3
    - TN1612TU_10

!!! summary "Learning goals"
    - Know characteristics of (in)elastic collisions
    - Solve for velocities after the collision
    - Treat collisions in CM frame

There are two principle type of collisions to distinguish: *elastic* and *inelastic* collisions. For an elastic collision the kinetic energy is conserved, whereas for an inelastic that is not the case. In the latter case, energy can be converted into deformation or heat.

![collision](images/collision.png){ align="center" width="200" }

We do not consider ourselves with what is exactly happening at the collision, only inspect the situation *before* and *after* the actual collision.  For both types of collision the momentum is conserved.

$$
\sum_i \vec{p}_i^{before} = \sum_i \vec{p}_i^{after}
$$

Why is the momentum conserved? You should know this directly by now

??? Answer
	
	There are no external forces only internal forces. From N2 we have $\dot{\vec{P}}=0$. Read again about [conservation laws](conservation_laws.md#conservation-of-momentum), the [two-body problem](two_body_problem), and the [many body case](many_particles.md#linear-momentum).
	

## Elastic Collisions

For an elastic collision the kinetic energy is conserved by definition next to the momentum. That is the sum of the kinetic energy before the collision is the same as the sum after the collision. This type of collision is also called hard-ball collision; no energy is dissipated into heat or deformation. Think about colliding billiard balls.

### Solving strategy

Here we explain how to compute the velocities after the collision, given the initial situation before the collision. We show this by an example. Consider a head-on collision of two point particles on a line as shown in the figure.

![collision1D](images/collision2.png){ align="right" width="200" }

One particle with mass $3m$ is initially at rest ($u=0$), the other with mass $2m$ has velocity $v$. What are the velocities $v',u'$ of the particles after the collision?

We can write down two equations using conservation of momentum and kinetic energy before and after the collision

$$
\begin{array}{rcl}
2m2v+0 &=& 2mv' + 3mu' \quad (*)\\
\frac{1}{2}2m(2v)^2 + 0 &=& \frac{1}{2}2mv'^2 + \frac{1}{2} 3mu'^2
\end{array}
$$

We have 2 equation and 2 unknowns $(v',u')$, therefore we can always solve this problem. The question is, what is the best strategy to do so? One equation involves the square of the velocity therefore a good strategy will help. We first bring the velocities $v,v'$ and $u,u'$ to the same side.

$$
\begin{array}{rcl}
2m(2v-v')&=& 3mu'\\
\frac{1}{2}2m(4v^2-v'^2)  &=& \frac{1}{2}3mu'^2
\end{array}
$$

Now we divide the two equations, this makes the solution very easy as the squares of the velocities always divide out.

$$
\Rightarrow 2v+v'=u' \quad (**)
$$

We can use this to find both unknowns by adding equations $(*)$ and $(**)$

$$
\begin{array}{lcl}
4v=2v'+3u' && 4v=2v'+3u'\\
2v=-v'+u'|\cdot 2 && 2v=-v'+u'|\cdot -3 \\
\end{array}
$$

$$
\begin{array}{lcr}
8v=5u' \Rightarrow u'=\frac{8}{5}v && -2v=5v' \Rightarrow v'=-\frac{2}{5}v
\end{array}
$$

This solution strategy always works. NB: you need to practice this. Although it is conceptually easy, we often see that students have problems when actually solving for the 2 unknowns.

### Collisions in the plane

Consider now a 2D collision such that the two particles collide in the origin.

![Collision in Plane](images/collision2D.png){ align="right" width="250" }

If we write down the equation of conservation of momentum (in $x,y$ components) and of kinetic energy

$$
\begin{array}{lcr}
2m2v+0 &=& 2mv'_x + 3mu'_x \\
0+ 3mv &=& 2mv'_y + 3mu'_y \\
\frac{1}{2}2m(2v)^2 + \frac{1}{2}3mv^2 &=& \frac{1}{2}2m v'^2 + \frac{1}{2}3mu'^2  
\end{array}
$$

Now we have 4 unknowns ($v'_x, v'_y, u'_x, u'_y$) but only 3 equations. The outcome seems not to be determined by the initial condition? Of course, that cannot be the case. Think shortly why? The outcome is fully determined by the initial conditions, but we did not set up the equations in the best way because we did not first transform the problem into a 1D problem such that the collision is head-on.

Using a Galilean Transformation to put one particle at rest. Here we set the blue particle to rest by subtracting $-2vt$. The red particle now has velocity $(-2v,v)$. The problem is still 2D.

$$
\begin{array}{rcl}
x' &=& x-2vt\\
y' &=& y
\end{array}
$$

![Galilean Transformation](images/collisionTrans.png){ align="center" width="200" }

But we can rotate the coordinate system, to obtain a 1D head-on collision that we can solve as above. Later the computed velocities can be converted back to the initial coordinate frame. It is a bit cumbersome, but again conceptually easy.

## Collisions in the CM frame

Now let's consider the [CM frame](two_body_problem.md#center-of-mass), with the definition of the CM coordinate as $\vec{R}=\frac{\sum m_i \vec{x}_i}{\sum m_i}$. As the mass is conserved during collision we have 1) $\sum m_i=const$ and as the momentum is conserved $\sum m_i \dot{\vec{x}}_i=const$, we see that the velocity of the CM does not change before and after collision

$$
\dot{\vec{R}}_{before}=\dot{\vec{R}}_{after}
$$

![Relative Positions CM](images/CMsketch.png){ align="right" width="200" }

For the relative coordinates $\vec{r}_i$ it holds that $\sum m_i \vec{r}_i =0$. Considering 2 particles: The relative distance $\vec{r}=\vec{r}_1-\vec{r}_2 = \vec{x}_1 -\vec{x}_2$ is Galilei invariant.

Using this property together with the [reduced mass](two_body_problem.md#reduced-mass) $\mu$ we obtain expressions for $\vec{r}_1=\frac{\mu}{m_1}\vec{r}$ and $\vec{r}_2=-\frac{\mu}{m_2}\vec{r}$, therefore

$$
\begin{array}{lcr}
m_1\vec{r}_1 &=& \mu \dot{\vec{r}}_1\\
m_2\vec{r}_2 &=& -\mu \dot{\vec{r}}_2
\end{array}
$$

This means the momenta of both particles are *always* equal in magnitude and opposed in direction in the CM frame. Only the orientation of the pair $\dot{\vec{r}}_{1,2}$ can change from before to after the collision.

![Relative Positions CM](images/CMmomentum.png){ align="center" width="250" }

## Inelastic Collisions

For inelastic collisions only the momentum is conserved, but not the kinetic energy. The total energy is of course always conserved. The kinetic energy after the collision is always less than before the collision. This difference is converted to deformation or heat.

The amount of "inelasticity" or loss of energy can be quantified by the *coefficient of restitution* $e$

$$
e\equiv \frac{v_{rel,after}}{v_{rel,before}}
$$

$$
e^2 \equiv \frac{E_{kin,after}}{E_{kin,before}} \text{ in CM frame}
$$

For $e=0$ the collision is fully inelastic, for $e=1$ it is fully elastic.
The definition of the coefficient must be independent of the frame of reference therefore *relative velocities* must be used; $E_{kin,rel}=\frac{1}{2}\mu \dot{r}^2$.

## Demos

1. [![Newton's Cradle](images/newtonCrad.jpg){ width="250" }](https://www.myphysicslab.com/engine2D/newtons-cradle-en.html){ target="_blank" }

2. [Colliding *Superballs*](https://www.youtube.com/watch?v=2UHS883_P60){ target="_blank" }

3. Elastic Collision (click on image)

[![](images/ElasticCollision.jpg){ width="500" }](Widgets/ElasticCollision.html){ target="_blank" }

4. Inelastic Collision (click on image)

[![](images/InElasticCollision.jpg){ width="500" }](Widgets/InElasticCollision.html){ target="_blank" }


## Exercises
