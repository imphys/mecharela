# Work & Kinetic Energy

!!! note "Textbook & Collegerama"
    - Chapter 1: sections 1.4 - 1.9
    - TN1612TU_03 second half

!!! summary "Learning goals"
    - Define work and kinetic energy
    - Derive the expression for kinetic energy $\frac{1}{2}mv^2$

## Work

![work](images/Work.jpg){ align="right" }

Work and energy are two important concepts. Work is defined as 'force times path', but we need a formal definition taking into account the vectorial character of the force and the path: if a point particle moves from $\vec{r}$ to $\vec{r} + d\vec{r}$  and during this interval a force $\vec{F}$ acts on the particle, then this force has performed an amount of work equal to:

$$
dW \equiv \vec{F} \cdot d\vec{r}
$$

That also means if the force is orthogonal to the path, no work is performed. If you carry a heavy backpack from Delft to Rotterdam, no work is done on the backpack! The gravitational force is perpendicular to the path. Note that there is an inner product ($\cdot$) between the two vectors $\vec{r}$ and $\vec{F}$, resulting in a scalar $W$. In other words, work is a scalar, not a vector. It has no direction. That is a large advantage over force in terms of working with it and thinking about it. Work as the unit of Joule [Nm=kgm$^2$/s$^2$=J].

In a 3D Cartesian coordinate system $(\hat{x},\hat{y}, \hat{z})$ the inner product can be written with 3 orthogonal components:

$$
dW = \vec{F} \cdot d\vec{r} = F_x dx + F_y dy + F_z dz
$$

Work done on a particle by $F$ during motion from point 1 to 2 over a prescribed trajectory:

$$
W_{12} = \int_1^2 \vec{F} \cdot d\vec{r}
$$

Note: in general the work depends on the starting point 1, the end point 2, and on the trajectory. Different trajectories from 1 to 2 may lead to different amounts of work.

## Kinetic Energy

Kinetic energy is defined and derived using the definition of work and Newton's 2nd law. We try to derive the work from point $1\to 2$ in terms of velocity (change). This will lead to the definition of *kinetic energy*.

$$
\displaylines{W_{12} \equiv \int_1^2 \vec{F} \cdot d\vec{r} = \int_1^2 \vec{F} \cdot \frac{d\vec{r}}{dt} dt = \int_1^2 \vec{F} \cdot \vec{v}dt = \\ = \int_1^2 m \frac{d\vec{v}}{dt} \cdot \vec{v}dt = m \int_1^2 \vec{v} \cdot d\vec{v} = m[\frac{1}{2} \vec{v}^2]_1^2 = \\ = \frac{1}{2} m v_2^2 - \frac{1}{2} m v_1^2}
$$

It states that if work is done on a particle ($W\neq 0$), then its kinetic energy must change. And vice versa: if the kinetic energy of an object changes, then work must have been done on that particle. NB: $v^2 = \vec{v}\cdot\vec{v} = v_x v_x + v_y v_y + v_z v_z$.

The sign of the work becomes also clear via the kinetic energy $\Delta W = \Delta E_{kin}$. If the final velocity is larger, then the work is positive, i.e. work done on the mass results in a higher kinetic energy. If the final velocity is smaller than the initial, then kinetic energy is released into work that can be used for something else.

Imaging a car hitting a brick wall during a crash test. Before the car has a velocity of 64 km/h, after hitting the wall the velocity is zero. Therefore $\Delta E_{kin}<0$, it is released into work, this work is "used" to deform the car. 

## Worked Examples

1. You carry a heavy backpack $m=20$ kg from Delft to Rotterdam (20 km). What is the work that you have done again the gravitational force? The answer is, of course, zero! That is because the path (from Delft to Rotterdam) is perpendicular to the gravitational force. Therefore the inner product $\vec{F}\cdot d\vec{r}=0$ over the whole way. Let us look at it more formally, this will help us when things get more complicated later. The force is $\vec{F}(x,y,z)=(0,0,-mg)=-mg\hat{z}$ and we choose our coordinate system such that the path be along the $x$-axis, the $y$-coordinate is zero and we the backpack is at height $z=1$ m. 
	
	$$
	W = \int_{Delft}^{Rott} F_x dx + F_y dy + F_z dz = \int F_x dx|_{y=0,z=1} = \int 0\, dx = 0
	$$
	
1. Now we consider a force field $\vec{F}(x,y)=(-y,x^2)=-y \hat{x} +x^2 \hat{y}$. We compute the work done over a path from the origin $(0,0)$ to $(1,0)$ and then to $(1,1)$ first along the $x$-axis and then parallel to the $y$-axis. We can split up the integral in these two parts as the direction in both parts is constant, therefore the inner product can be separated out.

	$$
	\begin{array}{rcl}
	W &=& \int_{(0,0)}^{(1,0)} \vec{F}\cdot d\vec{r} + \int_{(1,0)}^{(1,1)} \vec{F}\cdot d\vec{r} \\
	&=& \int_{(0,0)}^{(1,0)} F_x dx|_{y=0} + \int_{(1,0)}^{(1,1)} F_y dy|_{x=1}\\
	&=&  \int_{(x=0)}^{(x=1)} -y\,dx|_{y=0} +   \int_{(y=0)}^{(y=1)} -x^2\,dy|_{x=1}\\
	&=& -yx|_{x=0}^{x=1}|_{y=0} + -x^2y|_{y=0}^{y=1}|_{x=1} = -1
	\end{array}
	$$ 
	
	Try to integrate the force field yourself along a different path $(0,0)\to(0,1)\to(1,1)$ to the same end point.

	??? Answer
	
		$$
		\begin{array}{rcl}
		W &=& \int_{y=0}^{y=1} F_y \,dy|_{x=0} + \int_{x=0}^{x=1} 	F_x\,dx|_{y=1} \\
		&=& \int_{y=0}^{y=1} x^2\,dy|_{x=0} + \int_{x=0}^{x=1} 	-y\,dx|_{y=1} \\
		&=& -1 +0 =-1
		\end{array}
		$$
	 	
	 	The work done is the same over this path too. This could be coincidence or the case for *any* path. You will learn how to check this in the next chapter on [Potentials](potential_energy). Here is the [Exercise](potential_energy.md#exercises).
 
1. As long as the path can be split along coordinate axis the separation above is a good recipe. If that is not the case, then we need to turn back to the way how things have been introduced in the Analysis class. We need to make a 1D parameterization of the path.

	??? "Reminder of path/line integral from Analysis"
		
		Line integral of a vector valued function $\vec{F}(x,y,z): \mathbb{R}^3\to\mathbb{R}^3$ over a curve ${\cal{C}}$ is given as
		
		$$
		\int_{{\cal{C}}} \vec{F}(x,y,z)\cdot d\vec{r} = \int_a^b \vec{F}(\vec{r}(\tau)) \cdot \frac{d \vec{r}(\tau)}{d\tau} \, d\tau
		$$
	
		We integrate in the definition of the work from point 1 to 2 over an implicitly given path. To compute this actually, you need to parameterize the path by $\vec{r}(\tau )=(x(\tau), y(\tau), z(\tau))$. The integration variable $\tau$ tells you where you are on the path, $\tau \in [a,b]\in\mathbb{R}$. The derivative of $\vec{r}$ with respect to $\tau$ gives the tangent vector to the curve, the "speed" of walking along the curve. In the analysis class you used $\vec{v}(\tau)\equiv \frac{d \vec{r}(\tau)}{d\tau}$ for the speed. The value of the line integral is independent of the chosen parameterization. However, it changes sign when reversing the integration boundaries.
	
	Now we integrate from $(0,0)\to(1,1)$ but along the diagonal. A parameterization of this path is $\vec{r}(\tau) = (0,0)+(1,1)\tau, \tau\in [0,1]$. The derivative is $\frac{d \vec{r}(\tau)}{d\tau}=(1,1)$. Therefore we can write the work of $\vec{F}(x,y)=-y \hat{x} +x^2 \hat{y}$ along the diagonal as
	
	$$
	\int_0^1 \vec{F}(\tau,\tau) \cdot (1,1) \, d\tau
	= \int_0^1 (-\tau,\tau^2) \cdot (1,1) \, d\tau
	= \int_0^1 -\tau+\tau^2 \,d\tau =
	 -\frac{1}{6}
	$$

1. Integration of the same force $\vec{F}(x,y)=-y \hat{x} +x^2 \hat{y}$ from $(0,0)\to(1,1)$ but along a normal parabola. A parameterization of the path is $\vec{r}(\tau)=(0,0)+(\tau,\tau^2), \tau\in[0,1]$ and the derivative is $\frac{d\vec{r}}{d\tau} = (1,2\tau)$. The work then is

	$$
	\int_0^1 \vec{F}(\tau,\tau^2)\cdot (1,2\tau) \,d\tau =
	\int_0^1 (-\tau^2,\tau^2)\cdot (1,2\tau) \,d\tau = 
	\int_0^1 -\tau^2+2\tau^3\,d\tau = \frac{1}{6}
	$$

## Simulations

* Below is a physlet showing the relation between work done by a constant force and kinetic energy of a particle. Can you understand the curves plotted? Click on the image to start.

[![work constant force](images/WorkConstantForce.jpg){ width="500" }](Widgets/WorkConstantForce.html){ target="_blank" }

* Of course, forces are not always constant. This physlet show the relation between work done by a force that increases linearly with position. Can you understand the curves plotted? Click on the image to start.

[![work linear force](images/WorkLinearForce.jpg){ width="500" }](Widgets/WorkLinearForce.html){ target="_blank" }

## Exercises

Here are some exercises that deals with forces and work. Make sure you practice IDEA.

1. A point particle (mass $m$) drops from a height $H$ downwards. It starts with zero initial velocity. The only force acting is gravity (take gravity's acceleration as a constant).

    * Set up the equation of motion (i.e. N2) for $m$.

    * Calculate the velocity upon impact with the ground.

    * Calculate the kinetic energy of $m$ when it hits the ground.

    * Calculate the amount of work done by gravity by solving the integral $W_{12} = \int_1^2 \vec{F} \cdot d\vec{r}$.

    * Show that the amount of work calculated is indeed equal to the change in kinetic energy.

    Solve this exercise using IDEA.

    The video below shows a solution.

    <video id="myVideo" width="600px" controls>
 	  	<source src="../movies/WorkExample1.mp4" type="video/mp4">
 		Your user agent does not support the HTML5 Video element.
	</video>

1. A hockey puck ($m$ = 160 gram) is hit and slides over the ice-floor. It starts at an initial velocity of 10 m/s. The hockey puck experiences a frictional force from the ice that can be modeled as $-\mu F_g$ with $F_g$ the gravitational force on the puck. The friction force eventually stops the motion of the puck. Then the friction is zero (otherwise it would accelerate the puck from rest to some velocity :smiley: ). Constant $\mu = 0.01$.

    * Set up the equation of motion (i.e. N2) for $m$.

    * Solve the equation of motion and find the trajectory of the puck.

    * Calculate the amount of work done by gravity by solving the integral $W_{12} = \int_1^2 \vec{F} \cdot d\vec{r}$.

    * Show that the amount of work calculated is indeed equal to the change in kinetic energy.

    Solve this exercise using IDEA.
