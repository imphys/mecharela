# Central Forces & Polar Coordinates

!!! note "Textbook & Collegerama"
    - Chapter 3: section 3.1
    - TN1612TU_05

!!! summary "Learning goals"
    - Define central forces and their relation to angular momentum
    - Convert from Cartesian to polar coordinates and vice versa; also for velocity and acceleration
    - Use vector notation, compute inner and outer products in polar coordinates

## Central Force

A special class of forces is the central force. By definition, a force $\vec{F}(\vec{r})$ is *central* if:

* $\vec{F}\, ||\, \hat{r}$, with $\hat{r}$ the unit vector beloning to $\vec{r}$ (i.e., $\hat{r} \equiv \frac{\vec{r}}{||\vec{r}||}$)

* $|| \vec{F} ||$ only depends on $r = || \vec{r} ||$

Or in short:

$$
 \vec{F}(\vec{r}) = f(r) \hat{r} 
$$

with some scalar function $f:\mathbb{R}\to \mathbb{R}$. As mentioned [before](conservation_laws/#conservation-of-angular-momentum), for central forces the angular momentum is conserved. The change of angular momentum over time is governed by

$$
\frac{d\vec{l}}{dt} = \vec{r} \times \vec{F}
$$

In general, the force $\vec{F}$ is a function of position, i.e. of $\vec{r}$.  If  $\vec{F}(\vec{r})\, || \,\vec{r}$,  then  $\vec{r} \times \vec{F} = 0$ and angular momentum is conserved.

### Example

Consider a point particle (mass $m$) moving at a constant velocity (in magnitude!) along a circle in the plance with radius $R$ around the origin. This is a 2-dimensional problem and we will use $(x,y)$ coordinates. In fact it is a 1-dimensional problem, as the motion is restricted to the circle and the coordinates are constrained as $x^2+y^2=R^2$, but let's start with $(x,y)$.

Several points are to be addressed:

1. Give the relation between the velocity components and the $(x,y)$ coordinates.
1. Determine using N2 and Cartesian coordinates the force acting on $m$.
1. Show that this is a central force.
1. Given the force determined in the previous question, show using the angular momentum that the velocity is constant (in magnitude).

Don't forget to use IDEA when trying this example. Below is the solution video.

<br/>

<video id="myVideo" width="500px" controls>
  	<source src="../movies/CircularMotionCentralForce.mp4" type="video/mp4">
	Your user agent does not support the HTML5 Video element.
</video>

### Exercises

1. Same situation as in the Example, but now the particle is accelerating along its circular orbit.  
Show that the net force acting on the particle cannot be a central force.

1. Another force is given by: $\vec{F} = c\vec{r}$ with constant $c$ of unit [N/m]. At $t=0$, a point particle (mass $m$ = 1 kg) is located at $(x,y,z) = (1,0,0)$ (all in meters). It has an initial velocity of $\vec{v} = (0,1,0)$ (all in m/s).

    * Show that this is a central force.

    * Compute the angular momentum of $m$ at $t=0$.

    * Since the force is central, $\vec{l}$ is a constant. Show that this implies that $m$ will move in the $xy$-plane. Could you have reached the same conclusion by only inspecting $\vec{F}$ and the initial conditions? If yes: what are your arguments? If not: go through this part again :wink:.

    * Solve N2 for this case and find the trajectory of the particle.

    * From the trajectory, compute the angular momentum and convince yourself that it is the same as in question above.

## Polar Coordinates

In many cases, using Cartesian coordinates is not the most suitable choice. Sometimes polar coordinates in 2D or spherical coordinates in 3D are a much more natural choice. Here we will deal with planar polar coordinates. Planar means that we work in a plane (say the $xy$-plane), polar indicates that we do not use $(x,y)$ but a distance $r$ and an angle $\phi$. 

![cartesian](images/CartesianPolar.jpg){ align="right" width="250" }

We represent a point $P$ by either $(x,y)$ or $(r, \phi )$. Note that this means that the position vector $\vec{r}$ that indicates point $P$ has Cartesian coordinates $(x,y)$ and as a vector is given by $\vec{r} = x\hat{x} + y\hat{y}$.

We have introduced two unit vectors $(\hat{x},\hat{y})$ at right angles (hence they form an orthonormal pair). The recipe for finding point $P$ in Cartesian coordinates is the following: start at the origin and walk $x$ times a unit vector in the $x$-direction. Stop there and follow by walking $y$ times a unit vector in the $y$-direction.

We can write the same vector $\vec{r}$ in polar coordinates. To begin with, we define the unit vector in the $r$-direction, that is a vector  $\hat{r}$ of length 1 and parallel to $\vec{r}$. We also define the unit vector $\hat{\phi}$, which is a unit vector perpendicular to $\hat{r}$. These two form an orthonormal pair ($\hat{r}\cdot \hat{r}=1, \hat{r}\cdot \hat{\phi}=0, \hat{\phi}\cdot \hat{\phi}=1$). Now we can find point $P$ or denote the vector $\vec{r}$ as follows.

First, draw a unit vector $\hat{r}$ from the origin at an angle $\phi$ with the $x$-axis, then walk $r$ times this unit vector, and you will arrive at $P$. Thus the vector $\vec{r}$ can also be written as $\vec{r} = r \hat{r}$.

Note that it seems that this way of writing only needs one coordinate, i.e., $r$, but that is, of course, misleading. In order to know the direction of $\hat{r}$, we need the second coordinate $\phi$.

Thus, we have two equivalent ways or denoting $\vec{r}$:

$$
\vec{r} = x\hat{x} + y\hat{y} = r\hat{r}
$$

The coordinates are transformed into each other via:

$$
\left . \begin{array}{ll} x = r \cos \phi \\
             y = r \sin \phi\end{array} \right \}
             \leftrightarrow \left \{ \begin{array}{ll} r = \sqrt{x^2 + y^2} \\
             \phi = \arctan (y/x) \end{array} \right .
$$

It is also necessary to be able to transform both sets of unit vectors into each other. From the drawing on the right you can figure out that this goes as following:

$$
\left . \begin{array}{lrr} \hat{r} =& \cos \phi\, \hat{x} &+ \sin \phi \,\hat{y} \\
            \hat{\phi} =& -\sin \phi\, \hat{x} &+ \cos \phi \,\hat{y} \end{array} \right \} \leftrightarrow \left \{ \begin{array}{lrr} \hat{x} =& \cos \phi \,\hat{r} &- \sin \phi \,\hat{\phi} \\
             \hat{y} =& \sin \phi \,\hat{r} &+ \cos \phi \,\hat{\phi} \end{array} \right .
$$

If you are familiar with matrices and rotation, you can notice that indeed the corresponding matrix transformation can be written as a rotation of the coordinate frame $(\hat{x},\hat{y})$ over an angle $\pm\phi$. Where from $\hat{r},\hat{\phi}\to \hat{x},\hat{y}$ we have $+\phi$ (counter clockwise) and 

$$
\left (\begin{array}{c}\hat{r}\\ \hat{\phi}\end{array} \right ) =
 \left ( \begin{matrix}\cos \phi & \sin \phi \\ -\sin \phi &\cos \phi \end{matrix} \right )
 \left (\begin{array}{c}\hat{x}\\ \hat{y}\end{array} \right )
$$

and $-\phi$ (clockwise) the over way around. 

$$
\left (\begin{array}{c}\hat{x}\\ \hat{y}\end{array} \right ) =
 \left ( \begin{matrix}\cos \phi & -\sin \phi \\ \sin \phi &\cos \phi \end{matrix} \right )
 \left (\begin{array}{c}\hat{r}\\ \hat{\phi}\end{array} \right )
$$

From this it is really easy to see the transformation between the sets of unit vectors.

### Inner & Outer Product

![outer_product](images/OuterProductUnitVectors.jpg){ align="right" width="300"}

In order to work with e.g. the angular momentum, it is useful to be able to work out outer products between two vectors in polar coordinates.

The unit vectors we use are orthonormal vectors, i.e., all have length 1 and are mutually at right angles. That is: Cartesian ones are, and polar ones are, but when they get mixed, they are not necessarily perpendicular. A few simple rules between the unit vectors suffice for inner and outer products.

$$
\begin{array}{ll} 
              &\hat{x} \cdot \hat{x} = 1 & \hat{x} \cdot \hat{y} = 0 & \hat{y}\cdot\hat{y}=1 \\
              &\hat{r} \cdot \hat{r} = 1 & \hat{r} \cdot \hat{\phi} = 0 & \hat{\phi}\cdot\hat{\phi} =1\\
              &\hat{x} \times \hat{y} = \hat{z} & \hat{y} \times \hat{z} = \hat{x} & \hat{z} \times \hat{x} = \hat{y} \\
              &\hat{r} \times \hat{\phi} = \hat{z} & \hat{\phi} \times \hat{z} = \hat{r} & \hat{z} \times \hat{r} = \hat{\phi}
            \end{array}
$$

From this rule set you can set that you can compute the cross product of two vectors in polar cooridnates with the same method as in Cartesian coordinates (as bboth for an orthonormal basis).  

### Integration 

If we want to compute the potential of gravity that is easy if the force is $\vec{F}=-mg$ as we assume on earth, but difficult if the force is $\vec{F}(\vec{r})=-G \frac{m_1 m_2}{r^2}\hat{r}$ in general. The problem is that computation of the inner product $\vec{F}\cdot d\vec{s}$ for the potential "lives" in Cartesian space, but the force in polar coordinate space.

We need to transfer the inner product $\cdot d\vec{s}$ to polar coordinates, then we can do it easily. We start by writing $d\vec{s}$ in the basis $\hat{x},\hat{y}$ and filling in the coordinate transformation for $x,y$ firstly

$$
\begin{array}{rcl}
d\vec{s} &=& dx\, \hat{x} + dy\, \hat{y}\\
&=& d(r \cos\phi) \,\hat{x} + d(r \sin\phi) \,\hat{y}\\
&=& (dr \cos\phi - r\sin\phi d\phi) \,\hat{x} + 
(dr\sin\phi+r\cos\phi d\phi) \,\hat{y}\\
\end{array}
$$

Secondly, we fill in $\hat{x}=\cos\phi \hat{r}-\sin\phi\hat{\phi},\ \hat{y}=\cos\phi \hat{r}+\sin\phi\hat{\phi}$ from above, using the orthonormality of $\hat{r},\hat{\phi}$ to work out the above, we arrive (after short computation) at

$$
d\vec{s} = dr\,\hat{r}+rd\phi \,\hat{\phi}
$$ 

Now the integration of the gravitational force is easy

$$
V(\vec{r})=-\int -G \frac{m_1 m_2}{r^2}\hat{r} \cdot (dr\,\hat{r}+rd\phi \,\hat{\phi}) = G \frac{m_1 m_2}{r}
$$

### Velocity and Acceleration

Since we are dealing with dynamics, we also need to have a representation of velocity and acceleration in polar coordinates.

That is quite straightforward from the definition by applying the prodcut rule:

$$
\vec{v} \equiv \frac{d\vec{r}}{dt} = \frac{d}{dt} \left ( r \hat{r} \right ) = \frac{dr}{dt} \,\hat{r} + r \frac{d\hat{r}}{dt}
\equiv \dot{r}\,\hat{r} + r\dot{\hat{r}}
$$

Before proceeding: we have here the time derivative of the unit vector $\hat{r}$. But isn't that zero? Unit vectors don't change over time, only coordinates do, isn't it? Wrong. The polar unit vectors are defined using the coordinate $\phi$, and this coordinate certainly changes over time.

We can find out what $\frac{d\hat{r}}{dt}$ is by going back to Cartesian unit vectors - they don't change with time: they are fixed in space, doing the time derivative and transforming back:

$$
\begin{array}{l} \frac{d\hat{r}}{dt}
&= \frac{d}{dt} \left ( \cos \phi \,\hat{x} + \sin \phi \,\hat{y} \right ) \\
&= -\sin \phi \frac{d\phi }{dt} \,\hat{x} + \cos \phi \frac{d\phi }{dt} \,\hat{y} \\
&= \frac{d\phi }{dt} \left ( -\sin\phi \,\hat{x} + \cos\phi \,\hat{y} \right ) \\
&= \frac{d\phi }{dt} \hat{\phi} = \dot{\phi}\hat{\phi}
            \end{array}
$$

We summaries that

$$
\vec{v} \equiv \frac{d\vec{r}}{dt} = \dot{r} \,\hat{r} + r \dot{\phi} \,\hat{\phi}
$$

and we thus have two equivalent ways of representing the velocity:

$$
\begin{array}{l} \vec{v} 
&= \dot{x}\,\hat{x} + \dot{y}\,\hat{y} = v_x \hat{x} + v_y \hat{y}\\
&= \dot{r} \,\hat{r} + r \dot{\phi} \,\hat{\phi} = v_r \,\hat{r} + v_\phi \,\hat{\phi}
            \end{array}
$$

---

Does it make any sense that $\dot{\hat{r}} = \dot{\phi} \hat{\phi}$?

![cartesian2](images/drhatdt.jpg){ align="right" width="300" }

Yes, it does. If the position vector changes, then in general the angle $\phi$ will change as well. That will change the orientation (not the length) of $\hat{r}$.

In a time interval $dt$, the position changes from $(r,\phi )$ to $(r+dr, \phi + d\phi )$. Hence, the unit vector $\hat{r}$ changes its angle by $d\phi$. Its length, obviously, stays the same: it is a unit vector. Thus, it will only change in a direction perpendicular to itself, that is in the direction of $\hat{\phi}$.

$$
\hat{r}(t+dt) = \hat{r}(t) + 1\cdot d\phi \hat{\phi} \rightarrow
$$

$$
\frac{d\hat{r}}{dt} = \lim_{dt \rightarrow 0} \frac{\hat{r}(t+dt) - \hat{r}(t)}{dt} = \frac{d\phi}{dt} \hat{\phi}
$$

---


We could do the same transformation trick for $\frac{d\hat{\phi}}{dt}$, but here we additionally want to show another way how to go further that we could also have used for $\dot{\hat{r}}$.

Please note that we can rewrite $\frac{d\hat{\phi}}{dt} = \frac{d\hat{\phi}}{d\phi} \frac{d\phi}{dt}$

$$
\begin{array}{l} 
\frac{d\hat{\phi}}{d\phi} &=& \frac{d}{d\phi} (-\sin\phi \hat{x}+\cos\phi \hat{y}) \\
&=& -\cos\phi \hat{x} - \sin\phi\hat{y} = -\hat{r}
                        \end{array}
$$

The relation $\dot{\hat{\phi}} = -\dot{\phi}\hat{r}$ is needed for the acceleration. We just need to redo what we did for the velocity with an additional time derivative and replacing the derivative of the unit vectors

$$
\vec{a}\equiv \frac{d}{dt} \left (  \dot{r}\,\hat{r} + r\dot{\phi}\,\hat{\phi} \right )
$$

$$
\begin{array}{l} \vec{a} 
&= \ddot{r}\hat{r} + \dot{r}\dot{\hat{r}} + \dot{r}\dot{\phi}\hat{\phi} +r\ddot{\phi}\hat{\phi} + r\dot{\phi}\dot{\hat{\phi}}\\
&= \left ( \ddot{r}-r\dot{\phi}^2\right ) \hat{r} + \left ( 2\dot{r}\dot{\phi}+r\ddot{\phi}\right ) \hat{\phi}
            \end{array}
$$


### Derivatives

Sometimes it is handy to compute derivatives not in Cartesian but planar polar coordinates, e.g. for a central force/potential or the rotation. For example the gradient of a scalar function in polar coordinates $V(r,\phi,z)$ is given as

$$
\nabla V(r,\phi,z) = \partial_r V \,\hat{r} + \frac{1}{r}\partial_\phi V\,\hat{\phi} + \partial_z V \,\hat{z}
$$

The rotation in planar polar coordinates for a vector $\vec{F}(r,\phi,z)$ is 

$$
\nabla \times \vec{F} = 
\left ( \frac{1}{r}\partial_\phi F_z - \partial_z F_\phi\right ) \hat{r} + 
\left ( \partial_z F_r - \partial_r F_z\right ) \hat{\phi} + 
\frac{1}{r}\left ( \partial_r (rF_\phi)-\partial_\phi F_r\right ) \hat{z} 
$$

---

You can derive the transformation for the gradient e.g the following way. Realize that the gradient value in both CS must be the same.

$$
\nabla V = \partial_x V\,\hat{x} + \partial_y V\,\hat{y} + \partial_z V\,\hat{z} = a_1 \partial_r V\,\hat{r} + a_2\partial_\phi V\,\hat{\phi} + a_3 \partial_z V\,\hat{z}
$$

Now we only need to find the constants $a_1,a_2,a_3$.  We do this here for $a_2$ only. First we multiple the above equation with $\hat{\phi}$. This removes the $\hat{r},\hat{z}$ components on right side. 

$$
a_2\partial_\phi V = \partial_x V\,\hat{x}\cdot\hat{\phi} + \partial_y V\,\hat{y}\cdot\hat{\phi} + \partial_z V\,\hat{z}\cdot\hat{\phi}
$$ 

The inner products of the unit vectors we can compute from their definition, i.e. $\hat{x}\cdot\hat{\phi} = -\sin\phi, \hat{y}\cdot\hat{\phi} = \cos\phi,\hat{z}\cdot\hat{\phi} = 0$.

$$
a_2\partial_\phi V = -\sin\phi\, \partial_x V + \cos\phi \,\partial_y V
$$

NB: For $a_3$ you see that approach gives directly $a_3=1$. To move further we need $\partial_\phi V$ as a function of $x,y,z$. For this we must use the chain rule

$$
\frac{\partial V(x,y,z)}{\partial \phi} = \frac{\partial V}{\partial x} \frac{\partial x}{\partial \phi} + \frac{\partial V}{\partial y} \frac{\partial y}{\partial \phi} +\frac{\partial V}{\partial z} \frac{\partial z}{\partial \phi}
$$

Computing the partial derivatives of the coordinates can be done from their definition, i.e. $\frac{\partial x}{\partial \phi}=-r\sin\phi, \frac{\partial y}{\partial \phi}=r\cos\phi$ and $\frac{\partial z}{\partial \phi}=0$.

If we fill this into our earlier equation for $a_2$ we obtain

$$
a_2 \left ( -r\sin\phi\, \partial_x V + r\cos\phi\, \partial_y V \right ) = -\sin\phi\, \partial_x V + \cos\phi \,\partial_y V
$$

from which we see $a_2=\frac{1}{r}$.

You see, it is quite a bit of work, but doable. 

### Worked Examples

1. Use the rules for outer products of the unit vectors to show that in a polar coordinate system, the angular momentum for a particle moving in the $xy$-plane can be written as $\vec{l} = m r^2 \frac{d\phi}{dt} \hat{z}$.

	The angular momentum is given by $\vec{l}=\vec{r} \times \vec{p}$. In polar coordinates the postion vector is $\vec{r}=(r,0,0)$ and the momentum vector $\vec{p}=(mv_r,mv_\phi,0)$. Now we compute the cross product as always as $(\hat{r}, \hat{\phi},\hat{z})$ form an orthogonal basis
	
	$$
	\vec{l} \equiv \vec{r} \times \vec{p} = 
	\left ( \begin{array}{c}
	r\\ 0 \\ 0
	\end{array} \right )\times
	\left ( \begin{array}{c}
	mv_r \\ mv_\phi \\0
	\end{array} \right )
	=rmv_\phi \,\hat{z}= mr^2 \dot{\phi} \,\hat{z}
	$$
	
	If you first compute the cross product in Cartesian coordinates and then transform this is a lot of work, choosing the basis wisely early on paid off.

1. Circular motion with constant velocity

	Consider a mass moving on a circle with constant velocity (magnitude only of course) and constant angular velocity $\dot{\phi}\equiv\omega=const.$
	
	Find the equation of motion.
	
	![Circle motion](images/Circle_motion.png){ width="150" align="right"}

	In polar coordinates this is very easy as we computed the acceleration already before in this representation $\vec{a}=\left ( \ddot{r}-r\dot{\phi}^2\right ) \hat{r} + \left ( 2\dot{r}\dot{\phi}+r\ddot{\phi}\right ) \hat{\phi}$. For the given motion we have $r=R=const. \Rightarrow \dot{r}=\ddot{r}=0$ and as the angular velocity is constant $\dot{\phi}=\omega \Rightarrow \ddot{\phi}=0$ and we have remaining
	
	$$
	\vec{a} = -R\omega^2 \hat{r}
	$$

	The equation of motion is therefore $m\vec{a}=-mR\omega^2 \hat{r}$. The force is directed towards the center of the circle, which makes sure to keep the mass on a circular orbit. The force is called *centripedal* force. Typically this force is exerted by a string holding the mass.
	
	If you want to solve this problem with Cartesian coordinates, that is also possible, but more work. Start by writing down $x=R\cos\phi, y=R\sin\phi$ and compute the time derivaties $\ddot{x}=-R\cos\phi\, \dot{\phi}^2-R\sin\phi\,\ddot{\phi}, \ddot{y}=-R\sin\phi\, \dot{\phi}^2+R\cos\phi\, \ddot{\phi}$. From this compute $ma_x+ma_y = -m\omega^2 (x\hat{x}+y\hat{y})=-mR\omega^2\hat{r}$. Note: $R\hat{r}=x\hat{x}+y\hat{y}$.

### Exercises

It is essential to master working with polar coordinates. So, practice and make sure that when you have to solve a classical mechanics problem, you don't struggle with the polar coordinates at the same time.

1. Show that for a particle (starting from rest) moving at a constant angular acceleration $\alpha$ along a circle, its acceleration is given by $\vec{a} =-r \alpha^2 t^2 \hat{r} + \alpha r \hat{\phi}$. Consequently, the radial part of the force responsible for this accelerated circular motion becomes time-dependent. Whereas the tangential part is constant.


