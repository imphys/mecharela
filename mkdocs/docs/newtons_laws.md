# Newton's Laws

!!! note "Textbook & Collegerama"
    - Chapter 1: sections 1.1 and 1.2
    - TN1612TU_01b & _02

!!! summary "Learning goals"
    - Clearly state Newton's three laws

Newton formulated three laws, that govern the motion of objects. They can be formulated as follows:

**Newton 1**: An object is in a state of uniform motion if no net force acts on it.

**Newton 2**: $m \vec{a} = \sum_i \vec{F}_i \text { or } \frac{d\vec{p}}{dt} = \sum_i \vec{F}_i$ <br> This is a more precise form of Newton 1. The sum of the forces is equal to the change of momentum.

**Newton 3**: $\vec{F}_{1 \rightarrow 2} = - \vec{F}_{2 \rightarrow 1} \text { or } action = - reaction$

Much of physics stands on these simple laws. We will start practicing step by step and will investigate the implications of them.

## Time 

In Newtons mechanics time does not have a preferential direction. That means, in the equations derived based on the three laws of Newton, we can replace $t$ by $-t$ and the motion will have different sign, but that is. The path/orbit will be same same, but traversed in opposite direction. Also in special relativity this stays the same.

In daily life we experience a clear distinction between past, present and future. This difference is not present in this lecture at all. Only by the [second of law thermodynamics](https://en.wikipedia.org/wiki/Second_law_of_thermodynamics){ target="_blank"} the time axis obtains a direction, more about this in the second year class on *Statistical Mechanics*.

## Exercises

![Stamp Newton](images/StampNewton.jpg){ align="right" width="110" }

1. Close this web page (or don't peak at it :smile:) and write down Newton's laws. Explain in words the meaning of each of the laws. Try to come up with several, different ways of describing what is in these equations.

    *Stamp: "Sir Isaac Newton" by Scott Billings; CC BY-NC-ND 2.0*

1. On a bicycle you apply a force to the peddles to move forward, right?

    ![bicycle ride](images/BicycleRide.jpg){ align="right" width="120" }

    * Which force actually moves you forward, where is it located and who/what is providing that force?

    * Make a sketch and draw the relevant force. Give a different color to the force that actually propels you.

    * Think for a minute about the nature of this force: are you surprised?

    N.B. Consider while thinking about this problem: what would happen if you were biking on an extremely slippery floor, e.g. an icy road in the winter?

1. You are stepping from a boat onto the shore. Use Newton's laws to describe why you will end up in the water.

    N.B. Calculations are not required, but focus on the physics and describe in words why you didn't make it to the jetty.
