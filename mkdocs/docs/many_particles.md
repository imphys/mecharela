# Many-Body System

!!! note "Textbook & Collegerama"
    - Chapter 5: sections 5.1 - 5.5
    - TN1612TU_15

!!! summary "Learning goals"
    - State and describe N2, energy and momentum for many particle systems
   

We have seen that we could reduce the two-body problem of sun-earth to a single body question via the concept of reduced mass. That allowed us to solve the motion of the earth and the sun around their center of mass without replacing the sun with a force center fixed in space.

A natural question is: can we repeat this 'trick' and reduce a 3 to 2 to 1 particle problem etc.? Can it be done for $N$ particles?

It turns out that, in general, this is not possible even for three particles. The French mathematician and physicist [Henri Poincaré](https://en.wikipedia.org/wiki/Henri_Poincar%C3%A9){ target="_blank"} worked on the [three-body problem](https://en.wikipedia.org/wiki/Three-body_problem){ target="_blank" }; he was the first person to stumble upon what is now called *deterministic chaos* in the search of a solution to the three body problem.

## Linear Momentum

We can, however, find some basic features of $N$-body problems. In the figure, a collection of $N$ interacting particles is drawn.

![ManyParticles](images/ManyParticles.jpg){ align="right" width="200" }

Each particle has mass $m_i$ and is at position $x_i(t)$. For each particle, we can set up N2:

$$
m_i \ddot{\vec{x}}_i = \vec{F}_{i, ext} + \sum_{i \neq j} \vec{F}_{ji.}
$$

Summing over all particles and using that all mutual interaction forces form "action = -reaction pairs", we get:

$$
\sum_i m_i \ddot{\vec{x}}_i = \sum_i \vec{F}_{i, ext} \Leftrightarrow \sum_i \dot{\vec{p}}_i = \sum_i \vec{F}_{i, ext}
$$

The second part can be writen as:

$$
\frac{d\vec{P}}{dt} = \sum_i \vec{F}_{i, ext} \text{    with    } \vec{P} \equiv \sum_i \vec{p}_i
$$

In other words: the total momentum changes due to external forces. If there are no external forces, then the total momentum is conserved. This happens quite a lot actucally, if you consider e.g. collisions or scattering.

## Center of Mass

Analogous to the two-particle case, we see from the total momentum that we can pretend that there is a particle of total mass $M=\sum_i m_i$ that has momentum $\vec{P}$, i.e., it moves at velocity $\vec{V} \equiv\frac{\vec{P}}{M}$ and is located at position:

$$
\vec{V} = \frac{d\vec{R}}{dt} =\frac{\sum m_i \frac{d\vec{x}_i}{dt}}{\sum m_i} \Rightarrow \vec{R} = \frac{\sum m_i \vec{x}_i}{\sum m_i}
$$

Continuing with the analogy, we define relative coordinates:

$$
\vec{r}_i \equiv \vec{x}_i - \vec{R}
$$

and have a similar rule constraining the relative positions:

$$
\sum m_i \vec{r}_i  = 0
$$

## Energy

In terms of relative coordinates, we can write the kinetic energy as a part associated with the center of mass and a part that describes the kinetic energy with respect to the center of mass, i.e., 'an internal kinetic energy'.

$$
\begin{eqnarray}
            E_{kin} &\equiv &\sum \frac{1}{2} m_i v_i^2 \\
                    &= &\frac{1}{2} M \dot{\vec{R}}^2 + \sum \frac{1}{2} m_i \dot{\vec{r}}_i^2 \\
                    &= &E_{kin,cm} + E'_{kin}
            \end{eqnarray}
$$

For the potential energy, we may write:

$$
V = \sum V_i + \frac{1}{2} \sum_{i \neq j} \left ( V_{ij} + V_{ji} \right )
$$

with $V_i$ the potential related to the external force on particle $i$ and $V_{ij}$ the potential related to the mutual interaction force from particle $i$ exerted on particle $j$ (assuming that all forces are conservative).

## Angular Momentum

The total angular momentum is, like the total momentum, defined as the sum of the angular momentum of all particles:

$$
\vec{L} = \sum \vec{l}_i = \sum \vec{x}_i \times \vec{p}_i
$$

We can write this in the new coordinates:

$$
\vec{L} = \vec{R} \times \vec{P} + \sum \vec{r}_i \times \vec{p}_i = \vec{L}_{cm} + \vec{L}'
$$

Again, we find that the total angular momentum can be seen as the contribution of the center of mass and the sum of the angular momentum of all individual particles as seen from the center of mass.
