# Non inertial frames of reference

!!! note "Textbook & Collegerama"
    - Chapter 7: sections 7.1, 7.2, 7.3
    - TN1612TU_21, _22

!!! summary "Learning goals"
    - Understand the origin of fictitious forces
    - Derive centrifugal and Coriolis force
    - Set up equations of motion in (non) inertial frames

## Linear accelerated frames

We have two frames of reference $S$ and $S'$, where $S'$ is linearly accelerated with respect to $S$ in the $x$-direction with acceleration $A_x$. Writing down the coordinate transformations for these 2 frames is easy

$$
\begin{array}{rcl}
t' &=& t\\
x' &=& x- \frac{1}{2}A_x t^2\\
y' &=&y\\
z' &=& z
\end{array}
$$

Now we know which form Newton's laws, in particular N2, takes in $S'$.

$$
\frac{d}{dt}\vec{p}' = m' \frac{d}{dt} \vec{v}' = m \frac{d}{dt} \vec{v}'
$$

with the postulate $m'=m$ (which is correct also in relativity as you will see below).

$$
\begin{array}{rcl}
v'_{x'}\equiv \frac{d}{dt'}x' &=& \frac{d}{dt}x- A_xt\\
a'_{x'}\equiv  \frac{d}{dt'}v'_{x'} &=& \frac{d}{dt}v_x- A_x = a_x-A_x
\end{array}
$$

Now we can connect N2 in $S'$ to $S$

$$
\frac{d}{dt}\vec{p}' = m \left ( \frac{d}{dt}\vec{v}- \vec{A}\right ) = \sum \vec{F} - m\vec{A}
$$

All of a sudden the change of momentum is not given by the sum of the forces, but an extra term enters depending on the acceleration of the frame! This term has the form of a force $-m\vec{A}$ and is called *fictitious* or *inertial* force. The word *fictitious* is opposite to a "real" fundamental force such as electromagnetism, the weak and strong interaction (however, you still feel the force, it does something, the wording is a bit misleading). Luckily it turns out we can use N2 also in non-inertial frames _but_ we need to add the fictitious or inertial forces to make it work out. The force is, however, still real such that you can measure it and it has effect.

Note: all fictitious forces are proportional to the mass $m$! This has lead Einstein to consider gravity to be a fictitious force (or why they are also called *inertial* forces).

### Examples of fictitious/inertial forces

Some of the forces you experience in daily live are actually of the class "fictitious force"

- *Centrifugal force*; you get pushed outwards in a car driving a corner
- *Gravity*; you fall from a tree
- *Tidal forces*; oceans have low and high tide


A bit more unknown, but relevant for the formation of large scale currents and weather is the *Coriolis force*. Below the centrifugal and Coriolis force will be derived.

### The tides

If we start with the observation that the tides occur twice a day, twice low-tide and twice high-tide in equal time intervals. The idea that the moon attracts water to its side only, cannot be the explanation as then you only have a water "bulge" on the moon side, and while the earth rotates under the water bulge this would only give 1 tide per day, but we see 2!

The situation apparently must be as in the sketch, where we see the earth-moon system from the north pole (ignoring the inclined rotation axis). There needs to be 2 bulges of water that are static in this picture: one on the moon facing side and one on the far side of the moon due to the equal times of the tides. While the earth rotates around its axis in one day, this then gives 2 tides per day. But how can the gravity of the moon push away the water? That is very counter intuitive, but it is correct.

![Earth & Moon](images/earth-moon.png){ align="center" width="250" }

The earth and moon rotate around their common CM. The CM lies at about $r_1=5,000$ km, inside the earth radius of $R=6,380$ km ($M_{earth}/M_{moon}=81$). The common CM and rotation point is called [Barycenter](https://en.wikipedia.org/wiki/Barycenter){ target="_blank"} in astronomy. The moon accelerates the whole earth (including the water) towards the moon with gravitational pull $\vec{A}$. That is the force an observer  would feel at the earth center. On the moon facing side this force is just a bit *larger*, while on the far side the force is slightly *smaller*. Seen from the earth there is a larger force on the facing side and objects behave as if they feel an additional force that pull them towards the moon, on the far side.

![Earth & Moon](images/earth-moon2.png){ align="right" width="350" }

To calculate the tidal force we use the equation of fictitious force $m\ddot{\vec{r}}=\sum \vec{F}-m\vec{A}$. The acceleration of the earth by the moon is given by $\vec A=-G \frac{M_{moon}}{d_0^2}\hat{d_0}$, with $d_0$ the distance moon to center of the earth, the origin of our CS. Filling this into the equation of motion for an object with mass $m$ at position $\vec{r}$ on the surface of the earth, we obtain

$$
\begin{array}{rcl}
m\ddot{\vec{r}}&=&\sum \vec{F}-m\vec{A}\\
&=&\left (m\vec{g}-G \frac{mM_{moon}}{d^2}\hat{d}  \right ) + G \frac{mM_{moon}}{d_0^2} \hat{d_0}
\end{array}
$$

The earth gravity $\vec{g}$ is here the measured acceleration (including the centrifugal acceleration). The gravitational pull of the earth and moon on the mass is the first term in the bracket and the fictitious force is the second term. The force exerted by the moon is called *tidal force* and is given by

$$
\vec{F}_{tidal} = -GmM_{moon} \left ( \frac{\hat{d}}{d^2}-\frac{\hat{d}_0}{d_0^2} \right )
$$

If we now consider a point $P$ (negative $x$-axis) on the facing side of the moon $\hat{d}$ and $\hat{d}_0$ are aligned in the same direction, with $d<d_0 \Rightarrow F_{tidal}<0$ (towards the moon, in the negative $x$-direction). For a point Q on the far side of the moon (on the positive $x$-axis), we have again $\hat{d} \| \hat{d}_0$, but $d>d_0 \Rightarrow F_{tidal}>0$ (away from the moon). This explains that the moon creates two bulges rather than one. 

In astronomy these tidal forces are very important when studying rotating two bodies systems. Do not forget that each body exerts the tidal force on the other body! Depending on the relative mass of the two bodies and their distance, there will be a range where the tidal force is as strong as the gravitational forces holding the smaller of the bodies together. This limit is called the [Roche limit](https://en.wikipedia.org/wiki/Roche_limit){ target="_blank" }. If the tidal forces are larger than the gravitational force holding the smaller body together it will be ripped apart. This holds for moons orbiting a planet, but also for binary star systems. Inside the Roche limit of a planet/star there cannot be another orbiting star, but then rings are formed (from debris of the disintegrated body).

Note: If you obtain information on the origin of the tides from the internet or a popular program, these might be plainly wrong or just not right quite some time. We did not discuss the centrifugal force at all? Why? Typically popular explanations mix in this force in an inappropriate way (or another).

??? Answer

	The centrifugal force has shaped the surface of the earth to be not a sphere but a bit ellipsoidal. When the earth was fully fluid this reshaping was easy and then it cooled down. The rotating earth with the water is therefore is equilibrium. The surface of the water is an equipotential line of the combined gravitational field of the earth and the centrifugal potential. 
	
### Tidal locking and the dark side of the moon

![Dark_Side_of_the_Moon](images/Dark_Side_of_the_Moon.png){ align="right" width="200"}

From the earth we always see the same side of the moon. The moon rotates such that it revolves around itself in one month, the same time it needs to rotate around the earth, therefore from the earth we only see the same side. This is not happenstance!

In fact this happens due to tidal forces inducing small deformations on the other body (considering the bodies solid only, with no liquid now). On these deformations the gravitational pull acts now as a torque, reducing the rotation speed of the other body until it is in *tidal locking*. Over millions of years, the tidal forces first, bring one body into locking then the other. 

The total angular momentum of the two body system (earth-moon) must, of course, be conserved! The angular momentum a body looses is transferred to its orbital angular momentum. Sometimes this therefore referred to as *spin-orbit locking*.

NB: note the similar wording in many Germanic languages: moon - month (eng.), maan - maand (dutch), Mond - Monat (deu.), m&aring;ne - m&aring;nad (swe.) 

### Gravity as a fictitious force

As the form of a fictitious force is $-m\vec{A}$ and the force in a uniform gravitational field is $-m\vec{g}$ it is not so strange to think in this direction.

Einstein came up with the following Gedankenexperiment: If you are in a closed box elevator, you cannot distinguish if the elevator is in a uniform gravitational field of strength $\vec{g}$ or if it is accelerated with $\vec{g}$ (by some alien pulling it with a starship). Stated differently, a free-falling observer in a box cannot decide between acceleration or gravity. This directly makes it clear that heavy mass (measured with a balance; occurring in $F_g=-\frac{mM}{r^2}$) and inertial mass (measured with a scale; occurring in $\vec{F}=ma$) are the same on a conceptual level. This is know as the [*weak equivalence principle*](https://en.wikipedia.org/wiki/Equivalence_principle){ target="_blank"} which guided Einstein during the development of the general theory of relativity. (Gravitational) acceleration is there a consequence of the curvature of spacetime.

## Rotating frames

![Rotating frame](images/rotframe.png){ align="right" width="200"}

We place ourself in a frame of reference rotating with constant angular speed $\dot{\phi}=\omega =const$ around the $z$-axis. We will find the *centrifugal* and *Coriolis* force appearing as fictitious forces by inspecting N2 in a rotating frame.

![Rotating frame](images/rotframe2.png){ align="right" width="300"}

We start out by writing down the coordinate transformation, then the velocity and acceleration to find the relation for N2 between the two frames of reference. The transformation between the inertial frame $S$ and the rotating frame $S'$ in terms of coordinates is (with angle $\phi (t)$)

$$
\begin{array}{lcr}
x &=& \cos\phi \,x' - \sin\phi \,y'\\
y &=& \sin\phi \,x' + \cos\phi \,y'\\
\end{array}
$$

Now we need to take the time derivate of these equations to find the velocities (with product and chain rule)

$$
\begin{array}{lcl}
\dot{x} &=& -\sin\phi\, \dot{\phi}x' +\cos\phi\, \dot{x}' - \cos\phi\, \dot{\phi}y' -\sin\phi\, \dot{y}' \quad (*)\\
\dot{y} &=& \cos\phi\, \dot{\phi}x' +\sin\phi\, \dot{x}' - \sin\phi\, \dot{\phi}y' +\cos\phi\, \dot{y}'\\
\end{array}
$$

As these equations must hold for any angle $\phi$ we choose $\phi=0$

$$
\begin{array}{lcr}
\dot{x} &=& \dot{x}' - \dot{\phi}y' =\dot{x}' - \omega y'\\
\dot{y} &=& \dot{y}'+\dot{\phi}x' = \dot{y}'+\omega x'\\
\end{array}
$$

with the substitution $\omega=\dot{\phi}$. We see here the cross product

$$
\vec{\omega}\times\vec{r}_{S'} = 
\left ( \begin{array}{c} 0 \\ 0\\ \omega\end{array}\right ) \times
\left ( \begin{array}{c} x' \\ y'\\ 0\end{array}\right ) =
\left ( \begin{array}{c} -\omega x' \\ +\omega y'\\ 0\end{array}\right )
$$

Therefore we find for the connection of the velocities

$$
\vec{v}_S = \vec{v}_{S'}+\vec{\omega}\times\vec{r}_{S'}
$$

To get the acceleration we need again to calculate the time derivative of the equations $(*)$, remember that $\ddot{\phi} =0$ (by our assumption $\omega =const$), and then we set $\phi=0$. We then obtain (after quite some work)

$$
\begin{array}{lcr}
\ddot{x} &=& \ddot{x}'-2\omega \dot{y}' -\omega^2 x'\\
\ddot{y} &=& \ddot{y}'+2\omega \dot{x}' -\omega^2 y'\\
\end{array}
$$

The last term can be identified by the double cross product

$$
\vec{\omega}\times (\vec{\omega}\times\vec{r}_{S'}) = 
\left ( \begin{array}{c} 0 \\ 0\\ \omega\end{array}\right ) \times
\left ( \begin{array}{c} -\omega y' \\ +\omega x'\\ 0\end{array}\right ) =
\left ( \begin{array}{c} -\omega^2 x' \\ -\omega^2 y'\\ 0\end{array}\right )
$$

Summarizing we obtain for the acceleration

$$
\vec{a}_{S} = \vec{a}_{S'} + 2 \vec{\omega}\times\vec{v}_{S'} + \vec{\omega}\times (\vec{\omega}\times\vec{r}_{S'})
$$

Or in terms of force and rearranging

$$
m\vec{a}' = \vec{F} - 2m \vec{\omega}\times\vec{v}' - m\vec{\omega}\times(\vec{\omega}\times\vec{r}') \quad (**)
$$

The last extra term to the acceleration is the *centrifugal force* term. It is directed along the position vector $\vec{r}$ from the rotation axis. The first additional term is the *Coriolis force* that can only occur if an object has velocity $v'\neq 0$ in the rotating frame (see for the examples below). 

Note: the "-" in-front of the forces can easily be converted into a "+" by changing the order of the cross product! Be careful here. $m\vec{a}' = \vec{F} + 2m \vec{v}'\times\vec{\omega} + m\vec{\omega}\times(\vec{r}'\times\vec{\omega})$ is also correct, different books or some random internet site might use different notation.


You can visualize the necessity of such a fictitious force for example as follows. Imaging an alien is watching from space a train traveling straight north from Delft on the surface of the Earth. As the train moves north, the earth rotates a bit, so in effect for the alien the train not only moves up (north) but also a bit to the right (east), while the train in the rotating frame of reference of a passenger thinks to move only north but at the same time the right rail on the track gets worn out faster!

---

If the frame is also accelerating ($\omega \neq const.$) an additional fictitious force is appearing called the *Euler force*. It has the form $-m\dot{\vec{\omega}} \times \vec{r}$.

---

You can derive the above result more compactly in vector form if you apply the [transport theorem](https://en.wikipedia.org/wiki/Transport_theorem){ target="_blank" } from vector calculus twice. The theorem states that the change of a vector $u$ evaluated in a frame of reference rotating with constant speed $\vec{\omega}$ is given by

$$
\left [ \frac{d}{dt} \vec{u}\right ]_S = \left [ \frac{d}{dt} \vec{u}\right ]_{S'}+\vec{\omega} \times \vec{u}_{S'}
$$

You can see that this relation holds by consider how a unit vector transforms in a rotating CS. From [the rigid body](rigid_body.md#angular-velocity-and-acceleration) we know $\vec{v}=\vec{\omega}\times \vec{r}$. Now we apply this to the unit vectors $\hat{x},\hat{y},\hat{z}$ which gives us 

$$
\frac{d\hat{x}}{dt} = \vec{\omega} \times \hat{x},
$$ 

etc. for the other unit vectors. Applying this to a general vector $\vec{u}=u_x \hat{x} + u_y \hat{y} + u_z \hat{z}$ we find with the product rule 

$$
\begin{array}{rcl}
\frac{d\vec{u}}{dt} &=& \frac{\partial u_x}{\partial t}\hat{x} + u_x \frac{d\hat{x}}{dt} + 
\frac{\partial u_y}{\partial t}\hat{y} + u_y \frac{d\hat{y}}{dt} +
\frac{\partial u_z}{\partial t}\hat{z} + u_z \frac{d\hat{z}}{dt} \\
&=& \left ( \frac{\partial u_x}{\partial t}\hat{x} + \frac{\partial u_y}{\partial t}\hat{y} + \frac{\partial u_z}{\partial t}\hat{z} \right )
+ \left ( u_x \frac{d\hat{x}}{dt} + u_y \frac{d\hat{y}}{dt} + u_z \frac{d\hat{z}}{dt} \right )\\
&=& \left [ \frac{d\vec{u}}{dt} \right ]_{S'} + \vec{\omega} \times \vec{u}
\end{array}
$$

The first term is the derivative in the rotating CS (which is now also defined by this equation) and the last term is given by the change of the unit vectors.

Applied to our problem of the acceleration this gives the same equation as above of course

$$
\begin{array}{lcr}
\left [ \frac{d^2}{dt^2} \vec{r}\right ]_S &=& \left [ \frac{d}{dt} \right ]_S \left [ \frac{d}{dt} \vec{r}\right ]_S= \left [ \frac{d}{dt} \right ]_S (\vec{v}_{S'}+\vec{\omega}\times \vec{r}_{S'})\\
&=& \vec{a}_{S'} +  \vec{\omega}\times\vec{v}_{S'} + \vec{\omega}\times (\vec{v}_{S'}+\vec{\omega}\times\vec{r}_{S'})
\end{array}
$$

### Foucault's pendulum

![Foucault](images/Pendule_de_Foucault.JPG){ align="left" width="150" }

The pendulum of Foucault is just a pendulum really. It is a good experiment to demonstrate the rotation of the earth. In order to derive the equation of motion and work out the orbit of the pendulum, we start with a pendulum under the influence of gravity and apply the rotating frame of reference transformation derived above. Note: the book has a number of typos in the derivation - here, we try to avoid these. 

The force on the pendulum (length $l$) is given by

$$
\vec{F}_T = \frac{T}{l}x'\hat{x} + \frac{T}{l}y'\hat{y} + (\frac{T}{l}z'-mg)\hat{z}
$$

![Coordinate system for Foucault](images/CS_foucault.png){ align="right" width="200" }

with $T$ the tension force in the pendulum. Writing down the equation of motion in the frame of the rotation earth

$$
\vec{F}_T = m\ddot{\vec{r}}' +2m \vec{\omega} \times \dot{\vec{r}}'
$$

The centrifugal term of equation $(**)$ disappeared. Why is it not needed? Where did it go? Try to reason it before looking at the answer.

??? Answer

	Note: The centrifugal term of equation $(**)$ is indeed there, but it is integrated into the effective gravitational term $m\vec{g}$ already of the force on the pendulum, where $\vec{g}$ is the *observed* free-fall acceleration for that latitude. The observed $\vec{g}$ includes the reduction of $\vec{g}_0$ (from the mass of the earth) by the outward directed centrifugal force.
	
Let's first work out the cross product of the Coriolis force. Looking at the coordinate system in the figure we can write down the rotation vector $\vec{\omega}$ in the CS located in Delft ($\hat{x},\hat{y},\hat{z}$), $\omega \perp \hat{y}$ with $\hat{y}$ in the screen and cross it with the velocity

$$
\vec{\omega}\times \dot{\vec{r}}' = 
\left ( \begin{array}{c}
-\omega \cos\varphi \\0\\ \omega \sin\varphi
\end{array} \right ) \times
\left ( \begin{array}{c}
\dot{x}' \\ \dot{y}' \\ \dot{z}'
\end{array} \right ) =
\left ( \begin{array}{c}
-\dot{y}'\omega \sin\varphi \\ -(-\dot{z}' \omega \cos\varphi - \dot{x}' \omega \sin\varphi) \\ -\dot{y}\omega \cos\varphi
\end{array} \right ) 
$$

Here the angle $\varphi$ is the latitude on the earth (Delft $\varphi \sim 52^o$). Now we can write down the equations of motion for the 3 components

$$
\begin{array}{rcl}
\frac {T}{ml}x' &=& \ddot{x}'-2\omega\sin\varphi\, \dot{y}' \\
\frac {T}{ml}y' &=& \ddot{y}'+2\omega\sin\varphi\, \dot{x}' + 2\omega\cos\varphi\, \dot{z}' \\
\frac {T}{ml}z'-g &=& \ddot{z}'-2\omega\cos\varphi\, \dot{y}'
\end{array}
$$

If we use again the small amplitude approximation of the pendulum, we can use $z'=l$ as the height stays about constant and thus $\ddot{z}'= \dot{z}'\approx 0$ with $T=-mg$. We only need to look at the $x,y$ plane now. With constants $\alpha\equiv g/l$ with unit [1/s$^2$] and $\beta\equiv \omega \sin\varphi$ with unit [1/s] we have

$$
\begin{array}{rcl}
\ddot{x}'-2\beta \dot{y}' +\alpha x' &=&0 \\
\ddot{y}'+2\beta \dot{x}' +\alpha y' &=&0
\end{array}
$$

A solution to this coupled differential equation is 

$$
\begin{array}{rcr}
x'(t) &=& A \cos(\beta t) \cos (\Omega t)\\
y'(t) &=& -A \sin(\beta t) \cos (\Omega t)\\
\end{array}
$$

with $\Omega^2 \equiv \alpha+\beta^2$ with $\Omega$ unit [1/s]. Note: you could solve the coupled differential equation by introduction $\eta\equiv x+iy$ to obtain one complex equation which can be solved directly with a complex exponential.

Finally, to analyze the motion of oscillation we make a coordinate transformation to polar coordinates $r',\theta'$ in the CS fixed to Delft

$$
\begin{array}{rcll}
r'(t) &=& \sqrt{x'^{2}+y'^2} &= A \cos (\Omega t)\\
\theta'(t) &=& \arctan(y'/x') &=-\beta t
\end{array}
$$

The motion in radial direction is an oscillation with frequency $\Omega = \sqrt{g/l + \omega^2 \sin^2\varphi}$. With $\omega=0$ it must reduce to the [pendulum](pendulum.md#equation-of-motion) frequency for small angles or the [harmonic oscillator](harmonic_oscillator.md#undamped-harmonic-oscillator) that we treated before without rotation. Check! 

The plane of oscillation is rotating linearly a function of time as the angle $\theta' (t)=-\beta t$. The speed of rotation depends on $\beta\equiv \omega \sin\varphi$ on the latitude $\varphi$. To get a feeling for the rotation, have a look at the periods $T$ of rotation of the pendulum plane

$$
\frac{2\pi}{T} = \omega \sin\phi = \frac{2\pi}{d}\sin\varphi \Rightarrow T=\frac{d}{\sin\varphi}
$$

with earth's rotation speed $\omega=2\pi /d$ and $d=60\cdot 60\cdot 24$ the time of one day in seconds. For $\varphi=52^o$ we obtain a rotation of the plane of oscillation of $11^o$ per hour. At the poles the rotation is the shortest $T=d$ and on the equator "the longest" as there the plane stays constant.

[Animation of the Foucault pendulum](https://www.youtube.com/watch?v=Ax6Pij-CI6c){ target="_blank" } from a fixed and rotating frame of reference.

[A bit of explanation ](https://www.youtube.com/watch?v=aMxLVDuf4VY){ target="_blank" } about the rotation of the pendulum.

### Euler's equations 

*This part goes beyond the content of our first year course and is not part of the examination.*

Up to now we implicitly always assigned a rotation axis to the [moment of inertia](rigid_body.md#moment-of-inertia). In our formalism $I=\int_M r^2 dm$, $r$ was the distance to the rotation axis $\vec{\omega}$ where it was necessary that $\vec{r}\perp \vec{\omega}$. 
To generalize the description to rotations around arbitrary axis we first need to extend the moment of inertia.

Starting from 

$$
\vec{L}=\sum m_s r_s \times (\vec{\omega} \times \vec{r}_s)
$$

we can write the total angular momentum as $\vec{L}\equiv {\bf I} \vec{\omega}=\sum_i I_{ij}\omega_i$ with 

$$
I_{ij} \equiv \sum_s m_s (r^2 \delta_{ij}-r_ir_j),\quad i,j=1,2,3 (=x,y,z)
$$

with $r^2 = r_1^2 +r_2^2 +r_3^2 = x^2 +y^2 +z^2$ and $\delta_{ij}$ the [Kronecker delta](https://en.wikipedia.org/wiki/Kronecker_delta){ target="_blank" }.
From the construction it is clear that $I_{ij}$ has 9 components but the off-diagonal components are symmetric ($I_{xy}=I_{yx}, I_{xz}=I_{zx}, I_{yz}=I_{zy}$), and only 6 unique components remain. The first component, for example, is $I_{xx}=\sum_s m_s(y^2+z^2)$. That is consistent with our earlier definition as for a rotation around the $x$-axis the distance to this axis is given by $\sqrt{y^2+z^2}$.

As the moment of inertia ${\bf I}$ is a real-valued symmetric tensor it can be brought to *principal axis*. You will learn this in the class *Linear Algebra* under the term [eigen decomposition of a matrix](https://en.wikipedia.org/wiki/Eigendecomposition_of_a_matrix){ target="_blank" }. In plain words this means that there exists an orthonormal coordinates system (given by the principal axes) where the tensor only has diagonal elements ($I_{i}=I_{ii}=I_{ij}\delta_{ij}$) and that you can find this coordinate system by rotation. Each of the diagonal elements then corresponds to the moment of rotation around that axis. We will make use of this property shortly.

We now will study rotations not in the lab frame but in a corotating frame (just as we did with the CM system for collisions). To transfer "N2" of rotation $\vec{\Gamma} = \dot{\vec{L}}$  to a rotating frame $S'$, we make use of the transport theorem introduced above. 

$$
\vec{\Gamma}=\frac{d\vec{L}}{dt} = \left [ \frac{d\vec{L}}{dt}\right ]_{S'} + 
\vec{\omega} \times \vec{L}
$$

Substituting $\vec{L}= {\bf I} \vec{\omega}$

$$
\vec{\Gamma}={\bf I} \dot{\vec{\omega}} + \vec{\omega}\times ({\bf I} \vec{\omega})
$$

If we now bring the inertia tensor on principal axis ($I_{ij}\delta_{ij}\equiv I_i$) the above equation can be evaluated in this coordinate frame

$$
\vec{\Gamma} = 
\left ( 
\begin{array}{c}
I_1 \dot{\omega}_1 \\
I_2 \dot{\omega}_2 \\
I_3 \dot{\omega}_3
\end{array}
\right ) +
\left ( 
\begin{array}{c}
{\omega}_1 \\
{\omega}_2 \\
{\omega}_3
\end{array}
\right ) \times
\left ( 
\begin{array}{c}
I_1 \omega_1 \\
I_2 \omega_2 \\
I_3 \omega_3
\end{array}
\right )
$$

If we work out the above and assume no external torque we arrive at the *Euler equations for a rotating object*, they describe the time evolution of the rotations of a 3D object.

$$
\begin{array}{rcl}
I_1 \dot{\omega}_1 &=& \omega_2 \omega_3 (I_2-I_3)\\
I_2 \dot{\omega}_2 &=& \omega_1 \omega_3 (I_3-I_1)\\
I_3 \dot{\omega}_3 &=& \omega_1 \omega_2 (I_1-I_2)\\
\end{array}
$$

You see that the rotations around one axis can be coupled to the rotations around others axes depending on the relative size of the principal moments of inertia. 

In general it is now also unclear if a rotation can be stable or not. I.e. does a rotation around an axis change over time while there is no external torque? From the conservation of angular momentum $\dot{\vec{L}}=0$ we would expect this of course, but let's have a closer look below.

### Dzhanibekov effect 

For an object with three different principal moments of inertia $I_1 < I_2 < I_3$ (without loss of generality) it turns out that rotation around the second (intermediate) axis is unstable. This is called the *Dzhanibekov effect* or *intermediate axis theorem*. You can see a [cool example here](https://www.youtube.com/watch?v=1x5UiwEEvpQ){ target="_blank" } here.

We can analyze this behavior with the help of the Euler equations.

We start by investigating a rotation around axis 1, $\omega_1$. Will rotations around other axes be initiated by the coupling of the equations? and if so, how will they evolve? Initially we have $\omega_1 \gg \omega_2, \omega_3$ and start with the first Euler equation

$$
\dot{\omega}_1 = \omega_2 \omega_3 \frac{I_2-I_3}{I_1} = 0
$$

or in other words, $\omega_1=const.$. If we now take the time derivative of the second Euler equation, and substitute the above and the third equation, we arrive at

$$
\ddot{\omega}_2 = \frac{I_3-I_2}{I_1}(\dot{\omega}_1{\omega}_3 + {\omega}_1\dot{\omega}_3) =
\frac{I_3-I_2}{I_1}\frac{I_1-I_2}{I_3} \omega_1^2 \omega_2
$$

The first term $\frac{I_3-I_2}{I_1}>0$ while $\frac{I_1-I_2}{I_3}<0$ and $\omega_1^2=const.$ we can rewrite the equation as

$$
\ddot{\omega}_2  + c\omega_2 =0
$$

with some constant $c>0$. This is the equation of the [undamped harmonic oscillator](harmonic_oscillator.md#undamped-harmonic-oscillator) with solution $\omega_2(t)\sim \cos(t)$. In terms of stability, the amplitude of the rotation $\omega_2$ is bound, i.e. it does not grow over time.  We can work out the same conclusion for $\omega_3$. Therefore the rotation around $\omega_1$ is stable.

If we follow the same steps for an initial rotation around the intermediate axis 2 we find $\dot{\omega}_2=0$ and then again take the time derivative of the first Euler equation we find

$$
\ddot{\omega}_1 = 
\frac{I_2-I_3}{I_1}\frac{I_1-I_2}{I_3} \omega_2^2 \omega_1
$$

with $\frac{I_2-I_3}{I_1}<0$ and $\frac{I_1-I_2}{I_3} <0$, therefore the differential equation for $\omega_1$ is

$$
\ddot{\omega}_1 - k\omega_1 =0
$$

with some constant $k>0$. The seemingly small difference is a minus sign instead of a plus sign compare to earlier. However, this causes the solution to change from a sine/cosine to a real exponential $\omega_1(t)\sim e^{\sqrt{k}t}+e^{-\sqrt{k}t}$. This means that very quickly the rotation amplitude around $\omega_1$ will increase. The same behavior is observed for $\omega_2$. This causes the object to "flip over" very quickly and it not only rotates around $\omega_2$ anymore. 
![Intermediate axis theorem](images/tennis.gif){ align="right" width="300" } 
The rotation around $\omega_2$ is unstable. If you seen the [movie](https://www.youtube.com/watch?v=1x5UiwEEvpQ){ target="_blank" }, you now also understand why the rotating nut is flipping from its initial rotation so quickly. In the same manner you find that rotation around $\omega_3$ is stable, only the intermediate axis has this "problem".

You can try this at home with a tennis racket.


## Worked Example

A charged particle of mass $m$ and charge $q$ is moving in a static uniform magnetic field $\vec{B}=(0,0,B_z)$. On this particle the [Lorentz force](https://en.wikipedia.org/wiki/Lorentz_force){ target="_blank" } is acting $\vec{F}_L = q\vec{E}+q\vec{v}\times \vec{B}$. As the force acts perpendicular to $\vec{v}$ and $\vec{B}$ the magnitude of the velocity cannot change, but only its direction. The particle will move in a circle, or a spiral along the $z$-direction if the particle has an initial velocity. We can see this directly from the equation of motion

$$
\begin{array}{rcl}
m \dot{\vec{v}} &=& q\vec{v}\times \vec{B}\\
\vec{v} \cdot m \dot{\vec{v}} &=& \vec{v} \cdot (q\vec{v}\times \vec{B}=0\\
&\Rightarrow & \frac{1}{2} m\frac{d}{dt}\vec{v}^2=0 \Rightarrow | \vec{v} |=const.
\end{array}
$$

If we define the angular frequency of motion with $\vec{\omega}\equiv \frac{q}{m} \vec{B}$ with  $\vec{B}=(0,0,B_z)$ we can evaluate the cross product to find 

$$
\begin{array}{rcr}
\dot{v}_x &=& \omega v_y \\
\dot{v}_y &=& -\omega v_x
\end{array}
$$

The solution to this couple differential equation is 

$$
\begin{array}{rcl}
v_x &=& v_0 \sin (\omega t)\\
v_y &=& v_0 \cos (\omega t)\\
\end{array}
$$

Integrating this from velocity to position, gives a circle.

Now we choose to sit in the frame $S'$ of the rotating particle. The frame is rotating and potentially moving with constant initial velocity $v_z$. As the particle is standing still $v'=0$ the magnetic part of the Lorentz force $\vec{v}\times \vec{B}=0$! Going back to equation $(**)$

$$
m\vec{a}' = \vec{F} - 2m \vec{\omega}\times\vec{v}' - m\vec{\omega}\times(\vec{\omega}\times\vec{r}')
$$

We have $m\vec{a}'=0$ in $S'$ and $\vec{\omega}\times\vec{v}'=0$ as $\vec{v}'=0$, the centrifugal force is $m\omega^2 r' \hat{r}' \neq 0$ ($r'$ the distance of the particle to the rotation axis and $\hat{r}'$ the unit vector pointing towards the center) and equals the electric part of the Lorentz force $0=F_L -  m\vec{\omega}\times(\vec{\omega}\times\vec{r}')$. The observer $S'$ sees an electric field of strength $\vec{E}=\frac{m}{q}\omega^2 r'\hat{r}'$! The electric field is not constant in space but radially pointing inwards to the rotation axis. The strength depends on the $B$-field by $\omega$.

More importantly, in the rotating frame there is no magnetic field to cause the motion, but an electric field! This example shows that electric and magnetic fields are in fact two sides of the same thing. If you interpret it as magnetic or electric depends on the frame! The underlying physics is electromagnetic.

## Examples

1. Observe the Coriolis effect for the flow around a low pressure area in this video from [National Geography](https://www.nationalgeographic.org/encyclopedia/coriolis-effect/#what-is-the-coriolis-effect){ target="_blank" }

1. [Teufelsrad auf dem Oktoberfest](https://de.wikipedia.org/wiki/Teufelsrad_(Fahrgesch%C3%A4ft)){ target="_blank" }

1. The article entry on [wikipedia](https://en.wikipedia.org/wiki/Fictitious_force){ target="_blank" } about fictitious forces is nice.




