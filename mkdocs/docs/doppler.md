# Transformation of velocities & Doppler effect

!!! note "Textbook & Collegerama"
    - Chapter 12: section 12.2
    - TN1612TU_26

!!! summary "Learning goals"
    - Know the relativistic velocity transformation formulas
    - Understand the relativistic Doppler shift (difference to classical shift)
    - Understand that the cosmic background radiation is the after glow of the light matter decoupling shortly after the Big Bang
     
## Velocities

Imagine we have two space ships moving each with a speed of $\frac{3}{4}c$ as shown below. What is the speed that either the red or yellow space ships sees for the other space ship speed?

![2 Spaceships](images/addvel.png){ width="400"}

For the GT we have [derived the transformation](galilean_transformation.md#velocity-is-relative-acceleration-is-absolute) to be 

$$
v'_{x'} = v_x-V
$$

Here $v_x=\frac{3}{4}c$ and $V=-\frac{3}{4}c$, therefore $v'_{x'} = \frac{3}{2}c >c$. That is not possible as nothing can move faster than the speed of light, [as we will see later](4impuls.md). In addition we found $v'_{y'}=v_y, v'_{z'}=v_z$.

We need to derive the transformation/addition formula for velocities with the LT. Let us start from

$$
u'_{x'} = \frac{x'_2-x'_1}{t'_2-t'_1} 
=\frac{\Delta x'}{\Delta t'}\quad (*)
$$

with 

$$
\begin{array}{rcl}
x'_2-x'_1 &=& \gamma \left ( x_2 -\frac{V}{c} ct_2 \right ) -\gamma \left ( x_1 -\frac{V}{c} ct_1 \right )\\
&=& \gamma (x_2-x_1)-\gamma \frac{V}{c}(ct_2-ct_1)
\end{array}
$$

and

$$
\begin{array}{rcl}
ct'_2-ct'_1 &=& \gamma \left ( ct_2 -\frac{V}{c} x_2 \right ) -\gamma \left ( ct_1 -\frac{V}{c} x_1 \right )\\
&=& \gamma (ct_2-ct_1)-\gamma \frac{V}{c}(x_2-x_1)
\end{array}$$

From the last line it is clear that also the $y,z$ components of the velocity $\vec{u}$ will be influenced by the transformation although the motion is only along $x$. Substituting the expressions for the space and time difference into equation $(*)$, we obtain


$$
\begin{array}{rcl}
u'_{x}&=&  \displaystyle{\frac{\gamma \Delta x - \gamma \frac{V}{c}\Delta ct}{\gamma \Delta ct - \gamma \frac{V}{c}\Delta x} = \frac{\frac{\Delta x}{\Delta t}-V}{1-\frac{V}{c^2}\frac{\Delta x}{\Delta t}}}\\
&=& \displaystyle{\frac{u_x-V}{1-\frac{Vu_x}{c^2}}}
\end{array}
$$

For the transverse components $y,z$, we obtain due to the change of the time interval

$$
u'_{y'} = \frac{\Delta y}{\gamma \Delta ct - \gamma \frac{V}{c}\Delta x} = \frac{u_y}{\gamma \left (1-\frac{Vu_x}{c^2}\right )}
$$

In the limit of $u_x,V\ll c$ both formulas reduce to the Galileo transformation as required. For $u_x\to c$ and $V\to -c$ the combined velocity will stay smaller than $c$. Check.

The formula for the velocity transformation/addition are not so easy to remember. Later you will see how to derive them from the [transformation properties of the 4-velocity](4impuls.md#revisit-3-velocity-transformation), which is easy to remember. In addition the formula follows also directly from the interpretation of [LT as rotations](4vector.md#lt-as-a-rotation) in Minkowski space. 

For our example of the two approaching space ships,  $u_x=-\frac{3}{4}c, V=\frac{3}{4}c$ we find for the speed of the yellow approaching the red ship

$$
\frac{-\frac{3}{4}-\frac{3}{4}}{1+\frac{3}{4}\frac{3}{4}}c=-\frac{24}{25}c <c
$$

This is again smaller than $c$. For the other ship we find of course the same, but with different sign.


??? "Velocity addition with $\beta\equiv u/c$"

	If we state all velocities (for the 1D case) as $\beta_1 = \frac{u}{c}$ and $\beta_2 = -\frac{V}{c}$ than the addition takes on an easy form 
	
	$$
	\beta = \frac{\beta_1 + \beta_2}{1+\beta_1 \beta_2}
	$$ 

## Doppler effect

The [Doppler effect](https://en.wikipedia.org/wiki/Doppler_effect){ target="_blank"} is well-known from waves. You will learn the details about this in the second year class *Introduction to Waves*. 

You know it from daily life. If a car is passing you at high speed, the frequency of the sound you hear changes from approaching to driving away from you. The received frequency $f_{obs}$ by you is higher than the emitted frequency $f_{src}$ while the car is approaching, and smaller when it drives off.  

Here we investigate the effect of a moving source that is emitting light with $f_{src}$ (electro-magnetic waves). This is one of the few cases where the relativistic case is easier than the classical effect. In the latter it matters if the source is moving or the medium. For EM-waves, however, there is no medium (aether) [as we have seen](special/#hypothesis-of-the-aether) which simplifies things.

![Car & Doppler](images/doppler.png){ align="right" width="350"}

For the case of an observer with speed $v_{obs}$ and speed of sound in the medium $u$ and moving source $v_{src}$ (e.g. car) the classical formula of the frequency shift is

$$
f_{obs} = f_{src}\frac{u\pm v_{obs}}{u\mp v_{src}}
$$

where for the stationary observer & medium, we have $+/-$ and for the moving observer and stationary source $-/+$.

The origin of the observed frequency shift of a moving source is visible in the picture. In the direction of motion, more wave maxima arrive per unit time, as the car is moving closer between two wave maxima.

For the relativistic effect we consider a moving source with velocity $\vec{u}$ with observer $S'$ relative to $S$. The frequency of the source is $f_0=\frac{1}{T_0}$ in the rest frame, with $T_0$ the period of the oscillation.

![Doppler Relativistic](images/doppler2.png){ width="300"}


If we now consider the situation for $S$ as in the figure to the right. The position of the source $\vec{r}_0$ is indicated with the star $*$. 

![Doppler Relativistic](images/doppler3.png){ align="right" width="350"}

In the time interval we apply the [time dilation](timedilation.md#time-dilation) $\Delta t= \gamma \Delta t'=\frac{\gamma}{f_0}$ that the two maxima arrive at $S$, the source moves from $\vec{r}_0$ to $\vec{r}_1=\vec{r}_0+\vec{u}\frac{\gamma}{f_0}$. 

If we consider 2 consecutive wave maxima that are emitted in $S'$ and received in $S$

- 1st maxima in $S'$ at $t'_0$, that is received in $S$ at $t_1=t_0 +\frac{r_0}{c}$. The additional time $\frac{r_0}{c}$ is needed for the light to travel from $\vec{r}_0$ to the observer at the origin of $S$.
- 2nd maxima in $S'$ at $t'_0+\frac{1}{f_0}$, the is received in $S$ at $t_2=(t_0+\frac{\gamma}{f_0})+\frac{r_1}{c}$.

To move further we split the velocity of the source into a radial component (in line to the observer in $S$) and a tangential part perpendicular $\vec{u}=\vec{u}_r+\vec{u}_t =u_r \hat{r} +u_t \hat{t}$. If the distance $r_0 \gg \vec{u} \frac{\gamma}{f_0}$ then the distance $r_1 = r_0 + u_r \frac{\gamma}{f_0}$. 

Note that we could drop the vector notation here from the exact relation above. Classically only the radial component is relevant as only the distance matters.

With this simplification on the distances we can compute $t_2$

$$
t_2 = (t_0+\frac{\gamma}{f_0})+\frac{r_1}{c} \approx t_0+\frac{\gamma}{f_0} + \frac{r_0+u_r \frac{\gamma}{f_0}}{c}
$$

For the frequency $f$ in $S$ we now subtract the two arrival times 

$$
\frac{1}{f} =t_2-t_1 =  \frac{\gamma}{f_0}+ \frac{u_r}{c}\frac{\gamma}{f_0}
$$

Rewriting this into a ratio of the emitted and received frequency, we obtain for the relativistic Doppler effect

$$
\frac{f_0}{f} = \gamma \left (1+\frac{u_r}{c} \right ) = \frac{1+\frac{u_r}{c}}{\sqrt{1-\frac{u^2}{c^2}}}
$$

Please observe that the $\gamma$ factor is of $\gamma(u)$ that means even for only tangential movement $(u_r=0)$ there is a Doppler shit.

### Cosmic background radiation

The most well-known frequency shift is the red-shift from the expanding universe. 

The astronomer [Edwin Hubble](https://en.wikipedia.org/wiki/Edwin_Hubble){ target="_blank"} first found in the 1920s that the universe does not only consists out of our own galaxy, the milky way, but there must be (many) other galaxies, which were called *nebula* at that time. Second he could show that all farer away galaxies move away from us by measuring the Doppler shift of well-known emission lines of stars and their distance from periodic intensity variation of Cepheid Variable stars. It turned out that the distance of the galaxies $d$ was roughly linearly proportional to the red-shift which is again linearly related to the the radial velocity $v$ as we derived. This is known now as Hubble's law $v=H_0 d$ with the Hubble constant $H_0 \sim 70\ km/s/Mpc$). Further away galaxies move faster away, but why? And why is no galaxy approaching us?

At end of the 1920s [Georges Lemaître](https://en.wikipedia.org/wiki/Georges_Lema%C3%AEtre){ target="_blank"} applied Einstein general theory of relativity to cosmology and found that the universe must be expanding, while it started in a "primeval atom", now known as the *Big Bang*. He could explain the red-shift relation from the expanding universe hypothesis.

The Big Bang theory was highly debated early on, in particular by Einstein, but is now fully accepted. The strongest experimental evidence was the discovery of the *cosmic background radiation* in 1965 (by accident).

The whole cosmos is nearly uniformly filled by a background radiation of about 2.7 K (wavelength in the $\mu$m range) with small inhomogeneities as shown in the picture by the Planck satellite around 2013.

![CMB](images/cmb.jpg){ width="450"}

This radiation is the red shifted radiation from around 380,000 years after the Big Bang when the universe became transparent. At that time the temperature had dropped (due to the adiabatic expansion) to around 3000 K, at which protons and electrons can form stable hydrogen atoms $p+e^- \to H$. This event is called *recombination*. At higher temperatures photons are scattered from the free electrons (and protons) constantly, effectively the photons have a very short mean free path and the universe is opaque. At the recombination temperature all of a sudden the photon could travel without strong scattering, the universe was transparent. The 3K cosmic background radiation that we measure today is the red-shifted version of this 3000 K light. It gives us a glimpse of how the universe looked at that time. Apart from the background radiation there were no other light sources in the universe as stars had not formed yet, the [Dark Ages](https://en.wikipedia.org/wiki/Chronology_of_the_universe#Dark_Ages){ target="_blank"} of the universe began.

The red-shift here is actually caused by the expansion of the universe itself (the universe expands causing the photons' wavelength to expand). NB: Time in cosmology is often given in units of red-shift (e.g. the red-shift for recombination is $z=1089$).

??? "Wavelength temperature relation"

	How can we relate the wavelength of electro-magnetic radiation to temperature?
	
	Matter emits electro-magnetic radiation depending on its temperature. This relation is given by [Planck's law](https://en.wikipedia.org/wiki/Planck%27s_law){ target="_blank"} with which quantum mechanics started in 1900 as he considered *black body radiation*. The emitted spectral density per solid angle depends on the thermal energy $kT$ and is given by

	$$
	u(\lambda, T) = \frac{2hc}{\lambda^5}\frac{1}{e^{hc/\lambda kT}-1}
	$$

	Here for the first time $h$, Planck's constant, was introduced to quantize energy packages $hf$ of oscillation.

	Phenomenologically the relation between the peak of the spectrum and the temperature was found by [Wien](https://en.wikipedia.org/wiki/Wilhelm_Wien){ target="_blank"} already earlier to be $\lambda_{peak} = \frac{b}{T}$ with $b$ Wien's constant $b\sim 2900\ \mu m\cdot K$.
	
	NB: If you buy a light bulb for a lamp, then a temperature is indicated on the packaging, e.g. 2700 K for "warm white", 4000 K for "cool white" to describe the light color. Quantum physics at your local super market.

## Examples

1. 21 cm line of hydrogen in [radio astronomy](https://en.wikipedia.org/wiki/Hydrogen_line){ target="_blank"}. The proton and electron in the hydrogen atom both have a magnetic dipole moment related to their spin. The total quantum mechanical wavefunction can have 2 states for the spins, parallel or anti parallel, where anti parallel is energetically favorable. The transition between these two states is forbidden to first order (you will learn more about this in the three courses on *Quantum mechanics* in the second and third year). By [Fermi's golden rule of quantum mechanics](https://en.wikipedia.org/wiki/Fermi%27s_golden_rule){ target="_blank"} that means the probability that it happens per second is very small, here $10^{-15}s^{-1}$ or that the lifetime of the excited state is very long $\sim 10^7$ years. As space is vast and there is much hydrogen, however, this still happens a lot such that we can observe it.<br>
	Due to the very long life time, the emission line is very sharp, i.e. it has a small natural spectral broadening. This can be seen from [Heisenberg's uncertainty principle](https://en.wikipedia.org/wiki/Uncertainty_principle){ target="_blank"} $\Delta E \Delta t \sim \hbar$. If $\Delta t$ is very large, then $\Delta E$ is small and the spectral line is very sharp. Therefore shifts to this line must come from Doppler shifts which can be used to measure speeds accurately. 
