# Time dilation & length contraction

!!! note "Textbook & Collegerama"
    - Chapter 11: section 11.3 - 11.7 
    - TN1612TU_25

!!! summary "Learning goals"
    - Time is not absolute
    - Simultaneity is relative
    - Length contracts
    - Time dilates
    - Understand the twin or barn paradox
    
Here we have a look at the consequences of axioms 1 & 2. We know how two observers $S$ and $S'$ (moving away with $V$) transform their respective coordinates into each other, by the [Lorentz transformation](lorentz.md). We will look at the consequences for time and space coordinates.

  
## Relativity of simultaneity

From the Lorentz transformation it is clear that time is not universal anymore ($ct'\neq ct$ in general). This is a large step from Newton and Galileo. Now the time coordinate is mixed somehow with the space coordinates depending on the speed $V$.

Let us consider 2 events in the reference frame of $S$; event A with coordinates $(ct_1,x_1)$ and event B with ($ct_2,x_2)$. If the two events in $S$ are simultaneous $t_1=t_2; ct_1-ct_2=0$ then in $S'$ they are in general not! Simultaneity is relative!

$$
\begin{array}{rcl}
ct'_1 &=& \gamma \left ( ct_1 -\frac{V}{c}x_1\right )\\
ct'_2 &=& \gamma \left ( ct_2 -\frac{V}{c}x_2\right )\\
\Rightarrow ct'_1 -ct'_2 &=& \gamma (ct_1-ct_2) + \gamma \frac{V}{c}(x_1-x_2) 
\end{array}
$$

Even though the first term $(ct_1-ct_2)=0$ the second term $(x_1-x_2)$ is never zero unless $x_1=x_2$, and $ct'_1-ct'_2 \neq 0$ in general. 

In words: The events A and B that are simultaneous for $S$, are never simultaneous for $S'$, unless the events are happening at the same place.


*Relativit&auml;t der Gleichzeitigkeit* as Einstein called it, is the first very counterintuitive consequence by simple application of the Lorentz transformation. Our brains are no trained and build to cope with this aspect of nature. There is just no evolutionary advantage to it as all relevant speeds are much smaller than the speed of light.

## Time dilation

We investigate how time intervals between a stationary and a moving  observers are transformed. We can expect that these time intervals are not the same.

![Moving clock](images/timedilation1.png){ align="right" width="350"}

If you consider the sketch to the right, we see how time intervals are counted for a moving observer and for an observer in the rest frame. A light ray is traveling between 2 mirrors. This up and down traveling of the light is a counter for the time. If you have never thought how time is measured, think a bit how a clock actually does that. Today, the second is defined as a (very large) number of tiny energy transitions (vibrations) of the Caesium-133 atom (see [Wikipedia](https://en.wikipedia.org/wiki/Atomic_clock){ target="_blank"}). 

If you consider the time the light travels for the observer $S$ at rest for the moving clock, we can compute it via Pythagoras to $L^2+\frac{V^2}{4}t^2=\frac{c^2}{4}t^2$

![Moving clock](images/timedilation2.png){ align="right" width="130"}

We can solve this for the time $t$ that the stationary observer $S$ puts to the moving clock

$$
t= \frac{2L/c}{\sqrt{1-\frac{V^2}{c^2}}} = \gamma \frac{2L}{c}> \frac{2L}{c}
$$

We see directly that the time the stationary observer $S$ records is larger than the moving observer $S'$ itself which is just $2L/c$ (the time in his rest frame)! The time interval gets longer/dilated by the $\gamma$-factor.

$$
\Delta T = \gamma \Delta T_0
$$

with $\gamma=\frac{1}{\sqrt{1-\frac{V^2}{c^2}}} >1$ and $T_0$ the *proper time* or *eigen time* in the rest frame.

Note: a time interval is also the counting of your heart. That means the moving observer ages more slowly compared to the observer at rest. See the [examples](timedilation.md#Examples) for some experimental evidence of the time dilation.  

## Length contraction

The length of moving objects becomes smaller/contracted for the observer at rest. To explain this effect, we consider a moving rod with velocity $V$ and with length $L_0$ in the rest frame. The length is measured by the difference of two events in space time the front and the back of the rod $L_0=x'_2-x'_1$.
Now we transform the events with the Lorentz transformation $x'=\gamma (x-\frac{V}{c}ct)$ to events in $S$  

$$
L_0 =x'_2-x'_1 = \gamma (x_2-x_1)-\gamma \frac{V}{c}(ct_2-ct_1) 
$$

As we measure $x_1,x_2$ at the same time in $S$, we have $ct_2=ct_1$.

$$
L_0=\gamma (x_2-x_1)=\gamma L \Leftrightarrow L=\frac{L_0}{\gamma}
$$

The length of the moving object observed by the stationary observer is not the same as the length in the rest frame.  The length observed by the stationary observer $S$ gets smaller/contracted by $\gamma>1$ compared to the length in the rest frame of $S'$: $L<L_0$.



## Paradox: twins and barns

There are many variants of the following paradox. The word *paradox* already implies that there is only an apparent contradiction, not a real one. Here we will formulate the paradox with a ladder & barn and resolve it, but you can also think about it as a train & tunnel, or tank & trench etc. The resolution is always the same.

As an example we consider a ladder of rest length $L_l=26$ m and a barn of rest length $L_b = 10$ m. Obviously, the ladder does not fit in the barn.

Now consider that the ladder is moving with velocity $V=\frac{12}{13}c\ (\gamma =\frac{13}{5})$ towards the barn.

- For an observer in the barn, the length of the ladder is contracted to $L_l/\gamma = 26\cdot\frac{5}{13}=10$ m exactly fitting in the barn which in his rest frame 10 m. 
- For an observer moving with the ladder, the barn gets contracted to $L_b/\gamma= 10\cdot\frac{5}{13}=50/13 \sim 4$ m, being much too small to fit in the ladder. The ladder in his rest frame is 26 m.

![Barn & Ladder](images/barnladder.png){ width="450"}

We have applied the Lorentz transformation or length contraction (time dilation) and the concept of relativity correctly, but something seems wrong! The physical outcome must be the same for both observers, but one observer claims the ladder perfectly fits into the barn, the other say it does not!

You can have the same paradox not with length contraction, but time dilation, then it is called the *twin paradox*. We discuss the twin paradox later in the framework of [Minkowski-diagrams](4vector.md#the-twin-paradox).

??? Solution

	The key to the resolution of the paradox is always the relativity of simultaneity. In this instance of the paradox with the barn and ladder. Both observers are right but do not agree when the measurements are done.

Here we analyze the situation in detail using the Lorentz transformation. Later you can analyze it again qualitatively using a [Minkowski-diagram](4vector.md#the-ladder-barn-revisited) which is quite insightful.

Our above "analysis" was a bit short. We need to consider how both observers would actually *measure* things in their respective frames of reference and in which order they happen. It turns out that both points of view are correct, but with a twist. We define 4 events to analyze the situation.

1. Event 1:	right end ladder at left door barn
2. Event 2:	right end ladder at right door barn
3. Event 3:	left end ladder at left door barn
4. Event 4:	left end ladder at right door barn (not really needed)

![Barn & Ladder](images/bl_1.png){ width="300"}
![Barn & Ladder](images/bl_2.png){ width="300"}

Note: the size of the ladder in the sketch above is of course open for debate between the two observers :-)

Observer Barn ($B$) will conclude that the ladder fits inside the barn and actually is inside the barn if Event 3 is earlier then Event 2, according to observer $B$'s clock. If, however, Event 3 is later than Event 2, the ladder does not fit. Similarly, observer Ladder ($L$) will draw the same conclusions, but based on the clock of observer $L$.

Let's analyze these events. We will denote the coordinates of observer $B$ as $(ct,x)$ and those of observer $L$ as $(ct', x')$. Both observers agree that they will call the position of the left door the origin, that is $x_{LD} = x'_{LD} = 0$. Moreover, they agree that at the moment the right end of the ladder is at the left door, they will set their clocks to 0. Remember: according to observe $B$, the length of the ladder is $L_{0L}/ \gamma$ = 10 m, which happens to be the size of the barn according to $B$. We anticipate that $B$ will conclude that the ladder fits.

Next, we need to give the events their space-time coordinates, e.g. in $B$'s frame and transform these coordinates according to the LT to $L$'s frame. This is done below, where we used: $L_{0B}$ = proper length of barn, i.e. in the rest frame of the barn and $L_{0L}$ = proper length of ladder, that is in the rest frame of the ladder. Note: $V/c = 12/13 \Rightarrow \gamma = 13/5$

| Event | Barn $(ct,x)$ | Ladder $(ct',x')$ |
| --- | :--- | :--- |
| 1 | $(0, 0)$ | $(0, 0)$ |
| 2 | $(\frac{c}{V}L_{0B}, L_{0B})$ | $(\frac{c}{V}\frac{L_{0B}}{\gamma}, 0)$ |
| 3 | $(\frac{c}{V}L_{0B}, 0)$ | $(\gamma \frac{c}{V}\frac{L_{0B}}{\gamma}, -L_{0L})$ |

As we see, according to $B$, the left and right end of the ladder are exactly at the same moment at the left and right door of the barn, respectively (time coordinate of events 2 & 3 $ct_2=ct_3=\frac{c}{V}L_{0B}$). Consequently, observer $B$ measures that the ladder (just) fits into the barn as anticipated by us. 

However, if we look at events 2 & 3 according to $L$, we see that $L$ measures that the right end of the ladder is much earlier at the right door (event 2 $ct'_2=\frac{c}{V}\frac{L_{0B}}{\gamma}$), than the left end is at the left door (event 3 $ct'_3>ct'_2$). So, according to $L$, when the ladder hits the right end of the barn, the left part of the ladder is still left from the left door, thus outside the barn. The ladder does not fit. 

The paradox is, that both observers are right. Again we see demonstrated that simultaneous for one does not necessarily mean simultaneous for another. Very counter intuitive and yet: very true. 

As you see both observers do not agree where the ladder is when the left door is closed, where for the barn observer the door closes at the same time, this does not happen for the ladder observer. 

## Worked Example

This problem became known through [John Bell](https://en.wikipedia.org/wiki/John_Stewart_Bell){ target="_blank" }.

??? "Why you absolutely need to know John Bell"

	John Bell became famous by the [inequalities](https://en.wikipedia.org/wiki/Bell%27s_theorem){ target="_blank" } that have his name attached. Bell's theorem from 1964 started to end (post mortem) the twist between Einstein and Bohr about quantum mechanics in favor for Bohr. In 1935 Einstein, Polodsky and Rosen came up with a [paradox](https://en.wikipedia.org/wiki/Einstein%E2%80%93Podolsky%E2%80%93Rosen_paradox){ target="_blank" }, named EPR paradox after their names, that seemed to show that quantum mechanics cannot be "complete" (.i.e *the real thing* describing reality). Bell's inequalities allowed to experimentally test who was right, and Einstein was fundamentally wrong. In 2022 the Nobel Prize in Physics was awarded to Clauser, Aspect and Zeilinger for their efforts to experimentally show that the Bell's inequalities are violated (and Bohr was right). In Delft Roland Hanson performed a *loophole-free Bell test* in 2015 which was big news. 
	
	Why is this so important? It touches the heart of what is reality, is it deterministic and/or local now that quantum mechanics turned out *to be* the real thing? How we see reality now boils down to how we interpret quantum mechanics - and that is difficult to comprehend. The Copenhagen interpretation is so frustrating as the wave function collapses at measurement, however, the many-world interpretation that avoids the collapse is also not very appealing as it needs an infinite number of universes. This remains one of the important open ends in physics.
	
In this thought experiment we have two space ships $B$ and $C$ initially at rest and space ship $A$  as observer. $B$ and $C$ are connected by a tight but fragile string between them. $A$ simultaneously signals $B$ and $C$ to accelerate equally, and $B$ and $C$ will have the same velocity at every time from the start. 

![Rockets](images/Bell_paradox.png){ width="300"}

Question: Will the string between $B$ and $C$ break eventually?

??? "Answer"
	
	Yes.
	
Explanation:

One might think that the whole assembly of the two ships $B$ and $C$ and string undergo length contraction together, thus the string would not break, but that is incorrect.
	
* As seen from $A$'s rest frame,  $B$ and $C$ will have at every moment the same velocity, and so remain displaced one from the other by a fixed distance. The tying will not be long enough anymore due to length contraction and therefore break.
* The distance between $B$ and $C$ in the rest frame of $B$ or $C$ *increases* however as the acceleration from neither of them is simultaneous (if you work this out the relativity of simultaneity is the issue)! The thread breaks also in their frame. 

If you got this wrong, do not worry, most people do (that is trained physicists). 

If you think about this example for a bit, it becomes clear that relativistic acceleration is very troublesome for the structural integrity of extended objects! Another problem for our hopes of space travel to far away places.

## Examples

1. Myon production in the upper atmosphere

	[Myons](https://en.wikipedia.org/wiki/Muon){ target="_blank"} are elementary particles of the lepton family, the heavier brother of the electron. Myons decay via $\mu^- \to e^- + \bar{\nu}_e + \nu_\mu$ (or $\mu^+ \to e^+ + \nu_e + \bar{\nu}_\mu$. NB: You need the neutrinos to conserve lepton number) with a mean lifetime of $\tau=2.2\ \mu$s. Muons are generated in the upper atmosphere (20 km) when a high energetic cosmic ray hits a nuclei as decay products. The speed of the muons is about $v=0.99 c$. If you compute velocity times lifetime $\tau v < 1$ km, then we conclude that nearly no muons should be detectable on the ground (assuming no other process interferes in the muons path). But we do? How is this possible? 
	
	- You can solve this by considering the time dilation for an earth observer, as the lifetime is with respect to the rest frame! The lifetime for an earth observer is therefor stretched to $\gamma \tau\sim 16\ \mu$s. Therefore myons only need to travel about 4 lifetimes, and a decent fraction ($1/16$) can still be measured on the earth surface. 
	-  You can also reason via length contraction of the path the myons travel 20 km$/\gamma$.
	
2. Special relativistic correction to GPS timing

	[GPS](https://en.wikipedia.org/wiki/Global_Positioning_System){ target="_blank"} uses satellites orbiting the earth at a lower altitude to determine the position. If you receive the signals from 4 or more satellites, you can compute your position by triangulation, e.g. measurement of time difference of the received signals. To this end you need a very precise timing of the signals. The satellites velocity is "slow" with $v=4 \cdot 10^3$ m/s, and thus $\gamma \sim 10^{-5}\ll 1$. But the error in time measurement accumulates and due to time dilation even this small $\gamma$-factor will increase within 1 hour to a time error of $10^{-7}$ s or a position error of about 100 m. This would not be useful for navigation in a city and would required a recalibration of the system every few minutes. Later we see that a [general relativistic effect](beyondNewton.md#mass-slows-time) is even more prominent!
	
3. Relativistic correction to wavelength of electrons in a TEM

	In a standard Transmission Electron Microscope the electrons are accelerated via electric potential differences of up to 300 kV. Assuming that particles have a wavelength via the idea of de Broglie $E=mc^2=pc=h\frac{c}{\lambda} \Rightarrow \lambda = \frac{h}{p}$ we can use electrons as waves to image and magnify as with a normal light microscope. The smallest detail you can image with waves imaging in the far-field is given by the diffraction or [Abbe resolution limit](https://en.wikipedia.org/wiki/Diffraction-limited_system){ target="_blank"} to $d\sim \frac{\lambda}{2}$. For microscopy with visible light ($\lambda\sim$ 500 nm) this limit is a hard restriction. For electrons of low speeds we can use $\lambda = \frac{h}{mv}$, but for 300 kV acceleration the speed would be already larger than $c$! Later in the course you learn how to compute the [relativistic momentum](4impuls.md#energy-momentum-relation), filling in the numbers and the rest mass of the electron of 511 keV we obtain $\lambda \sim 2$ pm. About 10% *smaller* than from classical considerations. The diffraction limit to resolution is not an issue practically for the electrons as the distances between atoms in a solid are typically $>10$ pm.
