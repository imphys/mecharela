# Two-Body Problem

!!! note "Textbook & Collegerama"
    - Chapter 2: section 2.4.
    - Chapter 5: sections 5.1 - 5.5
    - TN1612TU_11 & _14

!!! summary "Learning goals"
    - Describe the two-body problem and reduce it to a one-body problem
    - Use the center of mass and reduced mass
    - Describe Kepler's laws using the reduced mass

So far, we have dealt with the motion of an individual point-like particle under the influence of forces. Even considering the solar system, we simplified it to a single body (or more precise single point) problem by replacing the Sun with a force field that does not move and considering the Earth as a point.

![TwoParticles](images/TwoParticles.jpg){ align="right" width="230" }

However, by the account of Newton's third law, the Earth exerts also a force on the Sun. Therefore, the Sun has to move as well; thus, we must revisit the Earth-Sun analysis and incorporate that the Sun isn't fixed in space.  The *two-body problem* is state hereby as:

Particle $m_1$ feels an external force $\vec{F}_1$ and an interaction force from particle two, $\vec{F}_{21}$. Similarly for particle $m_2$.

Consider the situation in the figure:

$$
m_1 \ddot{\vec{x}}_1 = \vec{F}_1 + \vec{F}_{21}
$$

$$
m_2 \ddot{\vec{x}}_2 = \vec{F}_2 + \vec{F}_{12}
$$

Add the two equations and use N3: $\vec{F}_{12} = - \vec{F}_{21}$:

$$
m_1 \ddot{\vec{x}}_1 + m_2 \ddot{\vec{x}}_2 = \vec{F}_1 + \vec{F}_{2} \Leftrightarrow
$$

$$
\dot{\vec{P}} = \vec{F}_{1} + \vec{F}_2
$$

with $\vec{P} \equiv \vec{p}_1 + \vec{p}_2$. In words, it is as if a particle with momentum $\vec{P}$ responds to the external forces but does not react to internal forces (the mutual interaction).

## Center of Mass

It is now logical to assign the total mass $M=m_1+m_2$ to this fictitious particle action on the external forces and assign it to the center of the mass (CM) of this particle $\vec{p}_1+\vec{p}_2=\vec{P}=M\vec{V}$

Its velocity $\vec{V}$ and position $\vec{R}$ then follow as:

$$
\begin{array}{rcl}
\vec{V} &=& \frac{m_1 \frac{d\vec{x}_1}{dt} + m_2 \frac{d\vec{x}_2}{dt} }{m_1 + m_2} \\
\Rightarrow \vec{R} &=& \frac{m_1 \vec{x}_1 + m_2 \vec{x}_2}{m_1 + m_2}
\end{array}
$$

It turns out to be convenient to define relative coordinates with respect to the center of mass position (see also the figure):

![CM coordinates](images/CM_r1r2.png){ align="right" width="300"}

$$
\vec{r}_1 \equiv \vec{x}_1 - \vec{R} \text{ and } \vec{r}_2 \equiv \vec{x}_2 - \vec{R}
$$

Via the external forces, we can 'follow' the center of mass position, i.e. $\vec{R}$. From the CM as new origin, we can find the position of the two particles.

A helpful rule is found from:

$$
\begin{array}{l}
            m_1 \vec{r}_1 + m_2 \vec{r}_2 = \\
            =m_1 \left ( \vec{x}_1 - \vec{R} \right ) + m_2 \left ( \vec{x}_2 - \vec{R} \right ) \\
            =m_1  \vec{x}_1 + m_2 \vec{x}_2 - (m_1 + m_2 ) \vec{R} = 0
            \end{array}
$$

$$
\Rightarrow m_1 \vec{r}_1 + m_2 \vec{r}_2 = 0 
$$

This has an important consequence: if we know $\vec{r}_1$, we know $\vec{r}_2$, and vice versa. Note: the directions of $\vec{r}_1$ and $\vec{r}_2$ are always opposed and the center of mass $\vec{R}$ is located somewhere on the connecting line between $m_1$ and $m_2$.

Note: in the case of no external forces $\vec{F}_1=\vec{F}_2=0$ and only internal forces $\vec{F}_{12} \neq 0$ the CM moves according to N1 with constant velocity $(\dot{\vec{P}}=0)$.

## Energy

In terms of relative coordinates, we can write the kinetic energy as a part associated with the CM and a part that describes the kinetic energy with respect to the CM, i.e., 'an internal kinetic energy.'

$$
\begin{array}{rcl}
            E_{kin} &\equiv &\frac{1}{2} m_1 v_1^2 + \frac{1}{2} m_2 v_2^2 \\
                    &= &\frac{1}{2} m_1 \left ( \dot{\vec{r}}_1 + \dot{\vec{R}} \right )^2 + \frac{1}{2} m_2 \left ( \dot{\vec{r}}_2 + \dot{\vec{R}} \right )^2 \\
                    &= &\frac{1}{2} M \dot{\vec{R}}^2 + \frac{1}{2} m_1 \dot{\vec{r}}_1^2 + \frac{1}{2} m_2 \dot{\vec{r}}_2^2
            \end{array}
$$

For the potential energy, we may write:

$$
V = \sum V_i + \frac{1}{2} \sum_{i \neq j} \left ( V_{ij} + V_{ji} \right )
$$

With $V_i$ the potential related to the external force on particle $i$ and $V_{ij}$ the potential related to the mutual interaction force from particle $i$ exerted on particle $j$ (assuming that all forces are conservative).

## Angular Momentum

The total angular momentum is, like the total momentum, defined as the sum of the angular momentum of the two particles:

$$
\vec{L} = \vec{l}_1 + \vec{l}_2 = \vec{x}_1 \times \vec{p}_1 + \vec{x}_2 \times \vec{p}_2
$$

We can write this in the new coordinates:

$$
\vec{L} = \vec{R} \times \vec{P} + \vec{r}_1 \times \vec{p}_1 + \vec{r}_2 \times \vec{p}_2 = \vec{L}_{cm} + \vec{L}'
$$

We find: that the total angular momentum can be seen as the contribution of the CM and the sum of the angular momentum of the individual particles as seen from the CM.

## Reduced Mass

Suppose that there are no external forces. Then the equation of motion for both particles reads as:

$$
\begin{array}{rcl}
          m_1 \ddot{\vec{x}_1} &= & \vec{F}_{12}\\
          m_2 \ddot{\vec{x}_2} &= & \vec{F}_{21} = -\vec{F}_{12}
          \end{array}
$$

If we divide each equation by the corresponding mass and subtract one from the other we get:

$$
\frac{d^2}{dt^2} ( \vec{x}_1 - \vec{x}_2 ) = \left ( \frac{1}{m_1} + \frac{1}{m_2} \right ) \vec{F}_{12}
$$

Note that the interaction force $\vec{F}_{12}$ is a function of the relative position of the particles, i.e., $\vec{x}_1 - \vec{x}_2 = \vec{r}_1 - \vec{r}_2$.

Introduce $\vec{r}_{12} \equiv \vec{r}_1 - \vec{r}_2 = \vec{x}_1 - \vec{x}_2$, then we obtain:

$$
\frac{d^2}{dt^2}  \vec{r}_{12}  = \left ( \frac{1}{m_1} + \frac{1}{m_2} \right ) \vec{F}_{12}(\vec{r}_{12})
$$

As a final step, we introduce the *reduced mass* $\mu$:

$$
\frac{1}{\mu} \equiv \frac{1}{m_1} + \frac{1}{m_2} \Leftrightarrow \mu = \frac{m_1 m_2}{m_1 + m_2} 
$$

And we can reduced the two-body problem to a single-body problem, by writing down the equation of motion for an imaginary particle with reduced mass.
 
$$
\mu \frac{d^2 \vec{r}_{12}}{dt^2} = \vec{F}_{12}  
$$

### Back to the Two-Body Problem

Once we solved the problem for the reduced mass, it is straightforward to go back to the two particles. It holds that:

$$
m_1 \vec{r}_1 + m_2 \vec{r}_2 = 0 
$$

$$
\vec{r}_2 = - \frac{m_1}{m_2} \vec{r}_1 \quad\&\quad
\vec{r}_2 = \vec{r}_1 - \vec{r}_{12}
$$

$$
\begin{array}{rcl}
          \vec{r}_1 &= &\frac{m_1}{m_1 + m_2} \vec{r}_{12} \\
          \vec{r}_2 &= &-\frac{m_1}{m_1 + m_2} \vec{r}_{12}
          \end{array} 
$$

## Kepler Revisited

![KeplerRevisited](images/KeplerRevisited.jpg){ align="right" width="270" }

Now that we have seen how to deal with the two-body problem, we can return to the motion of the Earth around the Sun. This is obviously not a two-body problem, but a many-body problem with many planets.

However, we can approximate it to a two-body problem: we ignore all other planets and leave only the Sun and Earth. Hence, there are no external forces. Consequently, the CM of the Earth-Sun system moves at a constant velocity. And we can take the CM as our origin.

We have to solve the reduced mass problem to find the motion of both the Earth and the Sun:

$$
\mu \frac{d^2 \vec{r}_{12}}{dt^2} = -\frac{Gm_e m_s}{r_{12}^2} \hat{r}_{12}
$$

Note: this equation is almost identical to the original Kepler problem. All that happened is that $m_e$ on the left hand side got replaced by $\mu$.

Everything else remains the same: the force is still central and conservative, etc.

### Where is the CM located?

![EarthSunCoG](images/EarthSunCoG.jpg){ align="right" width="180" }

We can easily find the center of mass of the Earth-Sun system. Chose the origin on the line through the Sun and the Earth (see fig.)

<br/>

$$
R = \frac{m_s x_s + m_e x_e }{m_s + m_e} = x_s + \frac{m_e}{m_s + m_e} (x_e - x_s)\approx x_s + 450km
$$

In other words: the Sun and Earth rotate in an ellipsoidal trajectory around the center of mass that is 450 km out of the center of the Sun. Compare that to the radius of the Sun itself: $R_s = 7 \cdot 10^5$ km. No wonder Kepler didn't notice. The common CM and rotation point is called [Barycenter](https://en.wikipedia.org/wiki/Barycenter){ target="_blank"} in astronomy.

### Exoplanets

However, in modern times, this slight motion of stars is a way of trying to find orbiting planets around distant stars. Due to this small ellipsoidal trajectory, sometimes a star moves away from us, and sometimes it comes towards us. This moving away and towards us changes the apparent color of the emission of molecules or atoms  by the [Doppler effect](doppler.md). This is a periodic motion, which lasts a 'year' of that solar system. Astronomers started looking out for periodic changes in the apparent color of the light of stars. One can also look for periodic changes in the brightness of a star (which is much, much harder than looking at spectral shifts of the light). If a planet is directly between the star and us, the intensity of the starlight decreases a bit. And they found one, and another one, and more and hundreds... Currently, more than [5,000 exoplanets](https://exoplanets.nasa.gov/) have been found.

* Changing color of star light due to a period motion induced by a planet orbiting the star ([movie from NASA](https://exoplanets.nasa.gov/alien-worlds/ways-to-find-a-planet/#/1)).

    <video id="myVideo" width="600px" controls>
        <source src="../movies/RadialVelocity.mp4" type="video/mp4">
    </video>

* Changing intensity of star light due to a period passage of a planet orbiting the star ([movie from NASA](https://exoplanets.nasa.gov/alien-worlds/ways-to-find-a-planet/#/2)).

    <video id="myVideo" width="600px" controls>
        <source src="../movies/transit-method-single-planet.mp4" type="video/mp4">
    </video>

* Changing intensity of star light due to a period passage of more than one planet orbiting the star ([movie from NASA](https://exoplanets.nasa.gov/alien-worlds/ways-to-find-a-planet/#/2)).

    <video id="myVideo" width="600px" controls>
        <source src="../movies/transit-method-multiple-planets.mp4" type="video/mp4">
    </video>

## Exercise

1. In the [Rutherford scattering](scattering.md#rutherford-the-nucleus) analysis, we implicitly used only the mass of the $\alpha$-particle when considering the momentum change $\Delta p/p$. Would it have been correct to use the reduced mass then? If yes, redo the analysis with the reduced mass. 
