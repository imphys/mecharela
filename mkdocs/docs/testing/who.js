window.addEventListener('load', eventWindowLoaded, false);

var kleur = '#E4FCFF'; //back ground color left canvas
var facecolor = "white";
var Fboys_x = 5; //x-force applied by the boys
var Fboys_y = 5; //y-force applied by the boys
var Fboys, Fboys_max;
var Fgirl = 15; //force applied by the girl
var Delta = 0.03;
var Lrope = 5;

var x0 = 300;
var y0 = 200;
var y = 0;
var L = 600;
var r, g, b;

var t = 0;
var t_stop = 1;
var dt = 1000;

var Logical = true;


function eventWindowLoaded() {
    clearit();
    //drawSituation();
}

function canvasSupport() {
    return Modernizr.canvas;
}

function updateSlider_Fgirl(slideAmount) {
    //get the element
    var display = document.getElementById("chosen_Fgirl");
    //show the amount
    display.innerHTML = slideAmount;
    Fgirl = slideAmount;
    clearit();
}

function updateSlider_delta(slideAmount) {
    //get the element
    var display = document.getElementById("chosen_delta");
    //show the amount
    display.innerHTML = slideAmount;
    Delta = slideAmount;
    Delta = Delta / 100;
    clearit();
}

function rope(y) {
    context.strokeStyle = "red";
    context.beginPath();
    context.lineWidth = 4;
    context.moveTo(x0, y0);
    context.lineTo(x0 + L / 2, y0 + y);
    context.lineTo(x0 + L, y0);
    context.stroke();
}

function ruitjespatroon() {
    for (var i = 0; i < 60; i++) {
        context.strokeStyle = "#aeaeae";
        context.lineWidth = 1;
        context.beginPath();
        context.moveTo(20 * i, 500);
        context.lineTo(20 * i, 0);
        context.stroke();
    }
    for (var i = 0; i < 25; i++) {
        context.strokeStyle = "#aeaeae";
        context.beginPath();
        context.moveTo(0, 20 * i);
        context.lineTo(1200, 20 * i);
        context.stroke();
    }
}

function boy(y) {
    context.fillStyle = facecolor;
    context.strokeStyle = "black";
    context.beginPath();
    context.lineWidth = 3;
    context.arc(228, 153, 30, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(237, 152, 5, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(220, 152, 5, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(228, 165, 10, Math.PI, 0, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(228, 153, 30, Math.PI * 1.85, Math.PI * 1.15, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = facecolor;
    context.strokeStyle = facecolor;
    context.moveTo(234, 141);
    context.lineTo(239, 134);
    context.lineTo(244, 141);
    context.fill();
    context.closePath();

    // second boy
    context.fillStyle = facecolor;
    context.strokeStyle = "black";
    context.beginPath();
    context.lineWidth = 3;
    context.arc(968, 153, 30, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(977, 152, 5, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(960, 152, 5, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(968, 165, 10, Math.PI, 0, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(968, 153, 30, Math.PI * 1.85, Math.PI * 1.15, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = facecolor;
    context.strokeStyle = facecolor;
    context.moveTo(954, 141);
    context.lineTo(959, 134);
    context.lineTo(964, 141);
    context.fill();
    context.closePath();

    // girl
    context.fillStyle = "black";
    context.strokeStyle = "black";
    context.beginPath();
    context.lineWidth = 1;
    context.arc(x0 + L / 2, y0 + y + 108, 40, Math.PI * 2.1, Math.PI * 0.9, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "white";
    context.lineWidth = 3;
    context.arc(x0 + L / 2, y0 + y + 100, 30, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(x0 + L / 2 + 9, y0 + y + 99, 5, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(x0 + L / 2 - 9, y0 + y + 99, 5, 0, Math.PI * 2, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(x0 + L / 2, y0 + y + 112, 10, Math.PI, 0, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "black";
    context.lineWidth = 1;
    context.arc(x0 + L / 2, y0 + y + 100, 30, Math.PI * 1.85, Math.PI * 1.15, true);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.fillStyle = "white";
    context.strokeStyle = facecolor;
    context.moveTo(x0 + L / 2 + 6, y0 + y + 100 - 12);
    context.lineTo(x0 + L / 2 + 11, y0 + y + 100 - 19);
    context.lineTo(x0 + L / 2 + 16, y0 + y + 100 - 12);
    context.fill();
    context.closePath();
}

function arrow(qq) {
    var qq, som;
    context.beginPath();
    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.moveTo(x0 + L / 2, y0 + Delta * 1000);
    som = y0 + Number(qq) + Number(Delta * 1000);
    context.lineTo(x0 + L / 2, som);
    context.lineTo(x0 + L / 2 - 5, som - 5);
    context.moveTo(x0 + L / 2, som);
    context.lineTo(x0 + L / 2 + 5, som - 5);
    context.font = "16px Arial";
    context.fillStyle = "blue";
    context.fillText(Fgirl + "N", x0 + L / 2 + 7, som + 5);

    context.moveTo(x0 + L / 2, y0 + Delta * 1000);
    context.lineTo(x0 + L / 2 - Fboys_x / 8, y0 + Delta * 1000 - Fboys_y);
    context.lineTo(x0 + L / 2 - Fboys_x / 8 + 5, y0 + Delta * 1000 - Fboys_y + 5);
    context.moveTo(x0 + L / 2 - Fboys_x / 8, y0 + Delta * 1000 - Fboys_y);
    context.lineTo(x0 + L / 2 - Fboys_x / 8 + 5, y0 + Delta * 1000 - Fboys_y - 5);
    context.fillText(Math.round(Fboys, 0) + "N", x0 + L / 2 - Fboys_x / 8 - 10, y0 + Delta * 1000 - Fboys_y + 21);

    context.moveTo(x0 + L / 2, y0 + Delta * 1000);
    context.lineTo(x0 + L / 2 + Fboys_x / 8, y0 + Delta * 1000 - Fboys_y);
    context.lineTo(x0 + L / 2 + Fboys_x / 8 - 5, y0 + Delta * 1000 - Fboys_y + 5);
    context.lineTo(x0 + L / 2 + Fboys_x / 8 - 5, y0 + Delta * 1000 - Fboys_y - 5);

    context.stroke();
}

function drawSituation() {
    Canvas = document.getElementById("canvasOne");
    context = Canvas.getContext("2d");

    sketchSituation();

}

function sketchSituation() {
    context.fillStyle = kleur; //set background color
    context.fillRect(0, 0, Canvas.width, Canvas.height);
    //Box
    context.strokeStyle = "#000000";
    context.lineWidth = 1; // border color
    context.strokeRect(1, 1, Canvas.width - 2, Canvas.height - 2);

    ruitjespatroon();

    rope(0);
    facecolor = "white";
    boy(0);
}


function startMotion() {

    Canvas = document.getElementById("canvasOne");
    context = Canvas.getContext("2d");

    var t = 0;
    if (!canvasSupport()) {
        return;
    }
    clearit();

    function drawScreen() {
        context.fillStyle = kleur; //set background color
        context.fillRect(0, 0, Canvas.width, Canvas.height);
        //Box
        context.strokeStyle = "#aeaeae"; // border color
        context.strokeRect(1, 1, Canvas.width - 2, Canvas.height - 2);
        ruitjespatroon();

        //--------------draw situation on canvas
        rope(Delta * 1000);
        Fboys_y = Fgirl / 2;
        Fboys_x = Lrope / 4 / Delta * Fgirl;
        Fboys = Math.sqrt(Fboys_x * Fboys_x + Fboys_y * Fboys_y);
        Fboys_max = Math.sqrt(25 / 2 * 25 / 2 + Lrope / 4 / 0.01 * 25 * Lrope / 4 / 0.01 * 25);
        r = 255;
        g = 255 - Math.floor(255 * Fboys / Fboys_max);
        b = g;
        facecolor = "rgb(" + r + "," + g + "," + b + ")";
        boy(Delta * 1000);
        arrow(Fgirl);
    }

    function gameLoop() {
        if (t < t_stop & Logical) {
            window.setTimeout(gameLoop, dt);
            drawScreen();
        }
        t = t + dt / 1000;
    }

    gameLoop();
}

function clearit() {
    Canvas = document.getElementById("canvasOne");
    context = Canvas.getContext("2d");
    context.fillStyle = "white"; //set background color
    context.fillRect(0, 0, Canvas.width, Canvas.height);

    context.fillStyle = kleur; //set background color
    context.fillRect(0, 0, Canvas.width / 2 - 50, Canvas.height);
    //Box
    context.strokeStyle = '#000000'; // border color
    context.strokeRect(1, 1, Canvas.width - 2, Canvas.height - 2);

    Logical = true;

    sketchSituation();
}

function Reset() {
    t = t_stop;
    Logical = false;
}
