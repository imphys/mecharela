# Angular Momentum & Torque

!!! note "Textbook & Collegerama"
    - Chapter 2: section 2.3
    - TN1612Tu_05 second half

!!! summary "Learning goals"
    - Define angular momentum and torque
    - State the analogy of N2 for rotations

Angular momentum is defined as

$$
\vec{l} \equiv \vec{r} \times \vec{p}
$$

Note that $\vec{l}$ is a vector that is *always* perpendicular to both the position vector $\vec{r}$ and the momentum vector $\vec{p}$.

The significance of $\vec{l}$ becomes clear if we consider its change with time:

$$
\frac{d\vec{l}}{dt} = \frac{d}{dt}\left ( \vec{r} \times \vec{p} \right )= \underbrace{\frac{d\vec{r}}{dt} \times \vec{p}}_{=0} + \vec{r} \times \frac{d\vec{p}}{dt} =\vec{r} \times \vec{F}
$$

The rate of change of angular momentum is equal to the torque ($\vec{\Gamma}\equiv \vec{r}\times \vec{F}$) acting on the object.  Or, rephrased: if no net torque is applied, then the angular momentum is conserved. Sometimes you see the letter $\tau$ for the torque.

Angular momentum has the sound of 'rotation'. We use the word 'angular' also in front of velocity: angular velocity, which is the change of an angle as coordinate with time. A formal definition of angular velocity is $\omega = \frac{d\theta}{dt}$, where we have ignored its vector character for the time being.

However, angular momentum is not reserved for rotations only. A point particle moving in a straight line will have angular momentum. Think shortly why, then see also the [example](angular_momentum.md#Example) below.

## Torque & Analogy to N2

If we define the torque $\vec{\Gamma}$ as the force action on a particle about the origin

$$
\vec{\Gamma}\equiv \vec{r} \times \vec{F}
$$ 

then we can write down an equation similar to N2 $(\dot{\vec{p}}=\vec{F})$ but now for angular motion

$$
\dot{\vec{l}}=\vec{\Gamma}
$$

where the force is replaced by the torque and the linear momentum by the angular momentum.

NB: Note that the torque and angular moment change if we choose a different origin as this changes the value of $\vec{r}$.

## Reminder cross product

Here some recap for the cross product. See also the [wiki](https://en.wikipedia.org/wiki/Cross_product){ target="_blank"} page.

$$
\vec{c} = \vec{a} \times \vec{b} \equiv \| a \| \|b\| \sin\theta \, \hat{n}
$$

Here $\theta$ is the angle between $\vec{a}$ and $\vec{b}$, and $\hat{n}$ is a unit vector normal to the plane spanned by $\vec{a},\vec{b}$ with direction given by the *right-hand rule*. 

![Right hand rule](images/Right_hand_rule.png){ align="right" width="200"}

From the definition it is clear that $\| \vec{a}\times\vec{b}\|$ is the area of the parallelogram spanned by $\vec{a},\vec{b}$.

![Area cross product ](images/area_cross.png){ align="right" width="200"}

The cross product is bilinear, anti commutative $(\vec{a}\times\vec{b} = -(\vec{b}\times\vec{a}))$ and distributive over addition. 

The formula is for computation in an orthonormal basis is

$$
\left ( \begin{array}{c}a_1\\ a_2\\ a_3 \end{array}\right ) \times 
\left ( \begin{array}{c}b_1\\ b_2\\ b_3 \end{array}\right ) 
= \left ( \begin{array}{c}a_2b_3 - a_3b_2 \\ 
- (a_1b_3-a_3b_1) \\ a_1b_2-a_2b_1 \end{array}\right )
$$

How to *remember* this rule for the cross product in Cartesian or polar coordinates see [here](potential_energy#worked-example).

The formula can be derived from the cross product for orthonormal basis vectors, e.g. $\hat{x},\hat{y},\hat{z}$

$$
\begin{array}{rcl}
\hat{x} \times \hat{y} &=& \hat {z}\\
\hat{y} \times \hat{z} &=& \hat {x}\\
\hat{z} \times \hat{x} &=& \hat {y}
\end{array}
$$

Notice the cyclic structure of the equations.

## Examples

1. Consider a point particle (mass $m$) moving without any force acting on it. According to Newton's 2nd law, its momentum will not change: $\vec{F} = 0 \Rightarrow\vec{p} = const$.

	Furthermore, the angular momentum is a constant too:

	![free_angular](images/FreeParticleAngularMomentum.jpg){ align="left" width="300" }

	$$
	\vec{F} = 0 \rightarrow \vec{r} \times \vec{F} = 0 \rightarrow \frac{d\vec{l}}{dt} = 0
	$$
	
	But clearly, the position of the particle $\vec{r}(t)$ is a function of time.
	How can it be that $\vec{l} = \vec{r} \times \vec{p} = const$? Think first before peeking at the small instruction movie below.
	
	
	<video id="myVideo" width="500px" controls>
	  	<source src="../movies/FreeParticleAngularMomentum.mp4" type="video/mp4">
		Your user agent does not support the HTML5 Video element.
	</video>
	
1. Throwing a basketball

	As seen in class: one person throws a basketball to another via a bounce on the ground, the basketball starts to spin after hitting the ground although initially it did not.
	
	![Basket Ball](images/basketball.png){ width="350"}

	When the ball hits the ground a friction force is acting on the ball. This force will apply a torque on the ball. The friction is directed opposite to the direction of motion. The arm $\vec{r}$ from the center of the ball to where the force is acting, is downwards. Using the right-hand rule we find that the torque is pointing in the plane of the screen, and thus the rotation is clockwise (forwards spin).
	
	The forwards momentum of the ball is reduced by the action of the force. The upwards components is just flipped by the bounce on the ground. Therefore the outgoing ball is bouncing up at a steeper angle than it is was incoming.

1. Conservation of angular momentum & spinning wheel

	As seen in class. We have a student sitting on a chair that can rotate (swivel chair). The student is holding a bicycle wheel in horizontal position. 
	
	![Student on Wheel](images/studentwheel.png){ width="100" align="right"}
	
	Once the student starts to spin the wheel while sitting on the chair, the student  will start to rotate in the opposite direction (with smaller angular velocity, [later](rigid_body.md#moment-of-inertia) you understand why their speeds are different). Now you can understand that before the angular momentum of the student-wheel system is zero, and it has to remain so after he spins the wheel. To compensate the angular momentum pointing up (counter clockwise rotation), an angular momentum pointing down (clockwise rotation) of the same magnitude must occur.

## Exercises

1. A point particle (mass $m$) is initially located at position $P=(x_0,H,0)$. At $t = 0$, it is released from rest and falls in a force field of constant acceleration $\vec{a}=(0,a,0)$ that acts on the mass. 

    * Analyze what happens to the angular momentum of $m$.

1. The same question, but now the particle has an initial velocity $\vec{v} = (v_0 ,0,0)$.

1. Similar situation: can you find an example of a falling object for which the angular momentum stays constant? Ignore friction with the air. Why is the latter statement important?
