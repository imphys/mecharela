# 4-Momentum & $E=mc^2$

!!! note "Textbook & Collegerama"
    - Chapter 13 & 14: section 13.5 - 13.9, 14.2 - 14.6
    - TN1612TU_28

!!! summary "Learning goals" 
    - Know the energy momentum relation
    - Know the definitions of 4-velocity and 4-momentum
    - Understand the construction of 4-velocity and 4-momentum to be 4-vectors
    - Understand the difference between proper time $d\tau$ and frame time $dt$
    
## Proper time

Before we can move to velocity or momentum we need to think about time again, as for velocity we need to take the derivative of the position with respect to time.

We know that the distance $ds^2$ is LT invariant, as is $c^2$, therefore we can combine both into another invariant - of time 

$$
d\tau^2 \equiv \frac{ds^2}{c^2} = dt^2-\frac{1}{c^2}(dx^2+dy^2+dz^2) 
$$

The naming of $d\tau$ as *proper time* or *Eigenzeit* is because for the rest frame $(dx=dy=dz=0)$ we have

$$
d\tau^2 = dt^2
$$

For a particle moving with 3-velocity $\vec{u}=(u_x,u_y,u_z)=(\frac{dx}{dt}, \frac{dy}{dt}, \frac{dz}{dt})$ we can relate the proper time $d\tau$ to the frame/coordinate time $dt$ as

$$
d\tau^2 =  dt^2\left [ 1-\frac{1}{c^2} \left (
\left (\frac{dx}{dt} \right )^2+
\left (\frac{dy}{dt} \right )^2+
\left (\frac{dz}{dt} \right )^2
\right) \right ]
$$

Here we identify the magnitude of the 3-velocity $u$. In other words

$$
\frac{d\tau^2}{dt^2}=1-\frac{u^2}{c^2} \Rightarrow dt = \gamma (u) d\tau
$$

The proper time interval relates to the frame time via the $\gamma$-factor for the velocity $u$.

## 4-velocity

Now we can tackle the 4-velocity. In other to make any sense we must define an invariant, therefore the 4-velocity $\vec{U}$ is

$$
U^\mu \equiv \frac{dX^\mu}{d\tau} 
$$

where the derivative of the 4-position vector is taken with respect to the proper time $\tau$. We obtain the relation to the 3-velocity $\vec{u}$ just from filling in $d\tau = dt/\gamma(u)$

$$
U^\mu = \gamma (u) \left ( \frac{dct}{dt}, \frac{dx}{dt}, \frac{dy}{dt}, \frac{dz}{dt}
\right ) = (\gamma (u) c, \gamma (u)\vec{u})
$$

4-velocity transfers between frames moving with speed $V$ as given by the [4-vector transformation](4vector.md#4-vector) as $\vec{U}$ is a 4-vector.

### Be careful with 4-vector interpretation

We compute the inner product of $\vec{U}$ with itself $U^2 = \gamma^2(u) (c^2-u^2)$. That is a LT invariant of course. Therefore we choose the frame such that $u=0$, or in other words $U^2=c^2$. The 4-velocity length is constant! That is not intuitive at all. Even stranger as the vector has constant length, it follows that the 4-velocity is always perpendicular to the 4-acceleration.

$$
\frac{d}{d\tau}U^2 = 2\vec{U}\cdot \frac{d}{d\tau}\vec{U}=0
$$

The counter intuitive stuff happens of course due to the pseudo-Euclidean metric.

### Revisit 3-velocity transformation

[Earlier we transformed the velocity](doppler.md) $u$ of a particle in $S$ to $S'$ which was moving with $V$. This was quite complicated and the formula is difficult to remember. However, there is no need to remember the formula, you can always derive it from the transformation of the 4-velocity.

For the 4-velocity $\vec{U}=(\gamma(u)c,\gamma(u)\vec{u})$ we can write down the [LT of a 4-vector](4vector.md#4-vector) between $S$ and $S'$.

$$
\begin{array}{rcl}
\gamma(u') c &=& \gamma(V) \left ( \gamma(u)c - \frac{V}{c}\gamma(u)u_x\right )\\
\gamma(u') u'_x &=& \gamma(V) \left ( \gamma(u)u_x - \frac{V}{c}\gamma(u)c\right )\\
\gamma(u') u'_y &=& \gamma(u)u_y\\
\gamma(u') u'_z &=& \gamma(u)u_z
\end{array}
$$

If we now divide the second of these equations by the first we obtain

$$
\frac{u'_x}{c} = \frac{\frac{u_x}{c}-\frac{V}{c}}{1-\frac{Vu_x}{c^2}}
$$

and if we divide the third of these equations by the first we obtain

$$
\frac{u'_y}{c} =\frac{\frac{u_y}{c}}{\gamma(V) \left( 1-\frac{Vu_x}{c^2}\right )}
$$

Just what we have derived [before](doppler/#velocities), but now in a way that you can always do this on the spot if you know the definition of the 4-velocity and the LT of a 4-vector.

## 4-momentum

If we postulate that the mass $m$ is LT invariant we can define the 4-momentum simply by

$$
\vec{P} = m\vec{U} = (m\gamma (u)c,m\gamma(u)\vec{u})\equiv (P^0,\vec{p})
$$

with the 3-momentum $\vec{p}=m\gamma(u)\vec{u}=m\frac{d\vec{x}}{d\tau}$.

!!! warning "mass a LT invariant"
	The mass $m$ _does not_ change as a function of velocity $\vec{u}$. You still sometimes see $\tilde{m}\equiv\gamma(u)m$ and with this $\vec{P}=(\tilde{m}c,\tilde{m}\vec{u})$. That is not practical as it mixes kinetic energy with inertial mass.
	
### Conservation of 4-momentum

For collisions now the total 4-momentum is conserved (per component)

$$
\sum_{i,before} \vec{P}_i = \sum_{j,after} \vec{P}_j
$$

If the total momentum is conserved than this must hold for the components $(m\gamma (u)c,\vec{p})$. 

## E=mc&sup2;

*The* most famous equation in physics.

We will derive it by looking at [N2](newtons_laws.md) in its relativistic form.

$$
\vec{F} = \frac{d\vec{p}}{dt} = \frac{d}{dt}(m\gamma(u)\vec{u}) = m\frac{d\vec{u}}{d\tau}
$$

Kinetic energy was [defined as work done on a mass](work.md#kinetic-energy). We again start from that and fill in N2 and take it step by step

$$
\begin{array}{rcl}
\Delta E_{kin} &=& \displaystyle{\int_1^2 \vec{F}\cdot d\vec{r} = \int_1^2 \vec{F}\cdot \vec{u}dt} \\
&=& \displaystyle{\int_1^2 \frac{d}{dt}(m\gamma(u)\vec{u})\cdot \vec{u}dt}\\
&=& \displaystyle{m\int_0^{\tilde{u}} \vec{u}\cdot d\gamma(u)\vec{u}}
\end{array}
$$

This integration is more difficult than what we had before as the $\gamma(u)$ factor appears additional in the differential (for small velocities we have $\gamma(u)=1$ and  we just get $\frac{1}{2}mu^2$ as before). Now we apply integration by parts

$$
\begin{array}{rcl}
\Delta E_{kin} &=& \displaystyle{m[\vec{u}\cdot \gamma(u)\vec{u}]_0^\tilde{u} - m\int_0^{\tilde{u}} \gamma(u)\vec{u} \cdot d\vec{u}}\\
&=& \displaystyle{m\gamma (\tilde{u})\tilde{u}^2 -  m\int_0^{\tilde{u}} \frac{\vec{u} \cdot d\vec{u}}{\sqrt{1-\frac{u^2}{c^2}}}}\\
&=& \displaystyle{m\gamma (\tilde{u})\tilde{u}^2 -  m\int_0^{\tilde{u}}\frac{\frac{1}{2}du^2}{\sqrt{1-\frac{u^2}{c^2}}}}\\
&=& \displaystyle{m\gamma (\tilde{u})\tilde{u}^2 - mc^2 \left[\sqrt{1-\frac{u^2}{c^2}}\right]_0^\tilde{u}}\\
&=& m\gamma (\tilde{u})\tilde{u}^2 - mc^2\left ( - \sqrt{1-\frac{\tilde{u}^2}{c^2}}+1 \right )\\
&=& m\gamma (\tilde{u})\tilde{u}^2 + \frac{mc^2}{\gamma(\tilde{u})}-mc^2\\
&=& \displaystyle{-mc^2+mc^2\gamma(\tilde{u}) \left ( \frac{\tilde{u}^2}{c^2}+1-\frac{\tilde{u}^2}{c^2} \right ) }\\
&=& mc^2(\gamma(\tilde{u})-1)
\end{array}
$$

??? "Integration by parts" 

	Easy to remember integration by parts formula, from the product rule
	
	$$
	\begin{array}{rcl}
	(fg)' &=& f'g+fg' \\
	\Rightarrow \int (fg)' &=& \int f'g + \int fg'\\
	\int f'g &=& [fg] - \int fg'
	\end{array}
	$$
	
	(Here we used $f'=d\gamma(u)\vec{u}$ and $g=\vec{u}$.)

If we now inspect the limiting cases for the velocity

$$
\Delta E_{kin} = mc^2(\gamma(u)-1)
$$

- particle at rest: $u=0 \Rightarrow \gamma (u)=1 \Rightarrow \Delta E_{kin}=0$
- small velocity $\frac{u}{c}\ll 1 \Rightarrow \gamma (u)=1+\frac{1}{2}\frac{u^2}{c^2}+{\cal{O}}(\frac{u^4}{c^4}) \Rightarrow \Delta E_{kin}=\frac{1}{2}mu^2$

The limiting cases work out. Very reassuring.

We can add a constant (LT invariant) to the kinetic energy $E=E_{kin}+mc^2 = m\gamma(u)c^2$. Adding constants to the energy/potential is always allowed as only the change of it is physically relevant (or the relative energies). The reason for *this* constant will be apparent below as this allows to include the energy in 4-momentum nicely.

We obtain

$$
E=m\gamma(u) c^2
$$

or in the rest frame $(u=0 \Rightarrow \gamma(u)=1)$

$$
E=mc^2
$$

With this energy $E=m\gamma(u)c^2$ we can define the 4-momentum as follows (we had $\vec{P}=(m\gamma(u)c,\vec{p})$)

$$
\vec{P}=\left ( \frac{E}{c},\vec{p} \right )
$$

??? "4-momentum with a different energy?"
	
	With a different energy (addition of another constant to $E_{kin}$ than what we did above) the length of the 4-momentum would not be LT invariant and $\vec{P}$ not a 4-vector. If we would have used $E=mc^2(\gamma -1)$ then $P^2$ would not be LT invariant. You see this by computing $P^2=\frac{E^2_{kin}}{c^2}-p^2c^2=m^2c^2(2-2\gamma)$.

And we have finally derived *the* most famous equation in physics. We will use, however, $E=m\gamma(u)c^2$ mostly exclusive as we are not always in the rest frame. The equation says essentially that mass is the same as energy. They are different manifestations of the same thing. A particle has energy in itself at rest without being in any potential. 

NB: As gravitation acts on mass, it should also act on energy if they are the same! This is indeed the case, also photons, massless particles, feel gravity. More about that in Einstein's [theory of general relativity](beyondNewton.md#general-relativity). 

### Mass in units of energy

The mass of an electron $m_e = 9.13\cdot 10^{-31}$ kg is often given as $512$ keV, [kilo electron Volts]. Mass of all elementary particles is given actually in units of eV.

One electron volt is 

$$
1 eV = 1.6\cdot 10^{-19} C \cdot 1V =  1.6\cdot 10^{-19} J
$$

The conversion to mass via $E=mc^2$

$$
m_e c^2 = 8.2 \cdot 10^{-14}J = \frac{8.2 \cdot 10^{-14}}{1.6\cdot 10^{-19}} = 512 keV
$$

### The fame

The origin of the fame is probably twofold. 

- Firstly, mass is not longer  conversed as was a central pillar in Newton's mechanics. It can be converted. This was shocking for *physicists only*.
- Secondly, when mass is actually converted into energy e.g. in a nuclear fission bomb or inside the sun with nuclear fusion, the effect is immense. The drop of the two nuclear bombs (little boy and fat man) on Hiroshima and Nagasaki made the equation inglorious world-known;  life changing for *all people*. 
- Einstein's rock start status helped certainly quite a bit.

## Energy-momentum relation

The 4-momentum is, of course, a 4-vector and therefore [$P^2$ is LT invariant](4vector.md#lorentz-invariants). Let us have a look at the outcome with $\vec{P}=\left ( \frac{E}{c},\vec{p} \right )$

$$
\begin{array}{rcl}
P^2 = \frac{E^2}{c^2}-p^2 &=& m^2\gamma^2(u) c^2 - m^2\gamma^2(u)u^2\\
&=& m^2\gamma^2(u)c^2 \left ( 1-\frac{u^2}{c^2}\right ) = m^2c^2\\
\Rightarrow E^2-p^2c^2 &=& m^2c^4
\end{array}
$$

Indeed, we find that $P^2$ is LT invariant as $m$ and $c$ are LT invariants. Rearranging the equation, we obtain

$$
E^2 = (mc^2)^2 + (pc)^2
$$

This converts back to $E=mc^2$ in the rest frame.

![Einstein triangle](images/einsteintriangle.png){ align="right" width="350"}

You can visualize the energy momentum relation with the Einstein triangle shown here, as the relation has the form of $c^2=a^2+b^2$. With the kinetic energy as $E_{kin}=mc^2(\gamma(u)-1)$. $E=E_0+E_{kin}\equiv mc^2 +E_{kin}$.

### LT invariance of P&sup2;

Above we found a very useful, but bit hidden relation in the derivation

$$
P^2 = m^2c^2
$$

This is of course LT, as $m$ and $c$ are LT invariants (and the momentum is a 4-vector), but more importantly we can use this for computations of relativistic [collisions](reldyn.md#example-compton-scattering). By the conservation of 4-momentum we can of course compute all collisions by equating the 4 components of the momentum before and after the collision. It is often, however, mathematically easier to write down the conservation of momentum and then square it. Because you can write down $P^2=m^2c^2$ directly, this saves often computations.

### Photons

For photons we have the energy given by $E=
\hbar \omega$ and the momentum as $p= \frac{\hbar\omega}{c}$. Substituting this into the energy-momentum relation, we find

$$
E^2 = (pc)^2+(mc^2)^2 \Rightarrow m=0
$$

The 4-momentum of a photon is 

$$
\vec{P} = P^\mu = \left ( \frac{E}{c},\vec{p} \right ) = \left ( \frac{\hbar \omega}{c}, \frac{\hbar \omega}{c} \right )
\left ( \frac{h \nu}{c}, \frac{h \nu}{c} \right )
$$

It is directly clear that for photons the LT invariant $P^2=0$.

NB: photons do not have mass. Do not get confused with $E=mc^2$.

### Rest frame of a photon?

Does a photon have a rest frame? It travels with the speed of light $c$ (obviously) in all frames.

The answers is no and we give three good arguments.

- A rest frame implies that in this frame the object is at rest. But for a photon, traveling at $c$, which is LT invariant, there is no frame at which it is at rest, but only frames with $v=c$.
- The proper time of a photon is $d\tau^2 = dt^2 -\frac{1}{c^2}d\vec{x}^2$	but this is always equal to 0! A photon does not experience the passage of time, therefore it is reasonable to state that do not have a rest frame.
- In the hypothetical rest frame for a photon there would be no electro-magnetic radiation/interaction be possible. In this frame e.g. the interaction between electrons would be zero.


## Speed of light as limiting velocity

The $\gamma$ factor increases strongly if the speed approaches the speed of light $u/c\to 1$ as can be seen in this plot

![Gamma versus u/c](images/gamma_v.png){ width="350"}

For a massive particle this has strong consequences. In the limit $u\to c$ the factor goes towards infinity. If we consider that the kinetic energy is $E=m\gamma(u)c^2$, the amount of work done to increase the speed increases with $\gamma$. Therefore no massive particle can move with the speed of light (or faster) as this would require an infinite amount of energy for the acceleration.

NB: $c$ is the speed of light in vacuum. In matter the speed of light $v$ is smaller than $c$, characterized by the *refractive index*  $n$ as $n=c/v$. This leads e.g. to refraction by [Snell's law](https://en.wikipedia.org/wiki/Snell%27s_law){ target="_blank"} at an interface. In matter the speed of massive particles can be larger than the speed of light there. This happens e.g. in a nuclear reactor when electrons move faster than the speed of light in water ($0.75c$). As water is a dielectric, the light waves generated from the response to the moving charge lag behind and a phenomena similar to a sonic boom is created. This phenomena is termed [Cherenkov radiation](https://en.wikipedia.org/wiki/Cherenkov_radiation){ target="_blank"}. If you have the opportunity to see it in a nuclear reactor, we highly recommend to take it. The color is a very intense deep blue.

## Worked Example

1. Momentum of an accelerated electron: compute the momentum and speed of an electron after acceleration in a potential of $V=300$ kV.

	From $E^2=(mc^2)^2+(pc)^2$ we have $p=\frac{1}{c}\sqrt{E^2-(mc^2)^2}$ and using $E=mc^2+E_{kin}$ we have
	
	$$
	p=\frac{1}{c}\sqrt{2mc^2 E_{kin}+E^2_{kin}}
	$$
	
	With $E_{kin}=300$ keV and $m_e=511$ keV. The speed can be computed from rearranging $E_{kin} = mc^2(\gamma -1)$ to $\frac{v}{c} = \sqrt{1- \frac{(mc^2)^2}{(E_{kin}+mc^2)^2}}=\sqrt{1-\frac{511^2}{811^2}}=0.77$. Please observe how practical it is to use the units eV!

1. Decay of a neutral kaon into three pions. $K^0 \to \pi^- + \pi^+ + \pi^0$. Show that the three pions trajectories are in one plane.

	In the rest frame of the kaon we have $\vec{p}_K=0$ before the decay. By conservation of momentum we have after the decay $\vec{p}_{\pi^-} + \vec{p}_{\pi^+} +\vec{p}_{\pi^0}=0$. A necessary and sufficient condition for three vectors $\vec{p}_1,\vec{p}_2,\vec{p}_3$ to lie in one plane is that $\vec{p}_1 \cdot (\vec{p}_2 \times \vec{p}_3)=0$ (Remember that this expression gives the volume of the parallelepiped spanned by the three vectors). From the conservation of momentum we have $\vec{p}_1 = -\vec{p}_2 -\vec{p}_3$. Now we can compute $(-\vec{p}_2 -\vec{p}_3)\cdot (\vec{p}_2 \times \vec{p}_3)=-\vec{p}_2\cdot(\vec{p}_2 \times \vec{p}_3)-\vec{p}_3\cdot(\vec{p}_2 \times \vec{p}_3)=0$. The two terms are each zero individually as the term in the bracket is perpendicular to $\vec{p}_2$ and $\vec{p}_3$ respectively. 
	
	If the trajectories in the rest frame of the kaon are in one plane, then they are also in one plane in all other frames. A coordinate transformation only shifts or rotates, which transfers a plane into a plane, but does not e.g. shear or bend a plane.
