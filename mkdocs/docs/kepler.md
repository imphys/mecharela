# Kepler's Laws & Newton

!!! note "Textbook & Collegerama"
    - Chapter 3: section 3.8
    - TN1612TU_06 & _07

!!! summary "Learning goals"
    - State and apply Kepler's laws
    - Understand how to derive Kepler's laws using Newton's laws

[Johannes Kepler](https://en.wikipedia.org/wiki/Johannes_Kepler){ target="_blank"} is best know for his work on the motion of the planets. From the data he got from [Tycho Brahe](https://en.wikipedia.org/wiki/Tycho_Brahe){ target="_blank"} (Kepler used to be Brahe's assistant), he stated his three famous laws:

**Kepler 1**: The orbit of a planet is an ellipse in a plane with the Sun in one of the focal points.

**Kepler 2**: A line segment joining a planet and the Sun sweeps out equal areas during equal intervals of time.

**Kepler 3**: The ratio of the square of period $T$ of the orbit of a planet and the cube of its semi-major axis $a$ is the same constant for all planets moving around the Sun: $\frac{T^2}{a^3} = const$.

## Kepler from Classical Mechanics

![GravityLaw](images/GravityLaw.jpg){ align="right" width="300" }

Kepler had found his famous laws empirically from data, but he had no theory that underpinned them. Newton could show that these laws follow naturally from his law of gravity (and of course N1, N2 and N3). A mass $m_1$ exerts a gravitational force $\vec{F}_{g,12}$ on a mass $m_2$:

$$
\vec{F}_{g, 12} = -G \frac{m_1 m_2}{r_{12}^2} \hat{r}_{12}
$$

with $\vec{r}_{12} = \vec{r}_2 - \vec{r}_1$ and $\hat{r}_{12} = \frac{\vec{r}_{12}}{r_{12}}$.

---

Below, we will, in coarse steps, go through the derivation of Kepler's laws from Newton's theory. Following Newton, we assume (for the time being) that the mass of the Sun is so much bigger than that of a planet that the motion of the Sun under the influence of the gravitational force exerted by the planet can safely be neglected. In other words: we can put the Sun in the origin of our coordinate system and expect it to stay there. [Later we come back](two_body_problem.md#kepler-revisited) to this assumption and check how to take into account that the earth is also pulling at the sun.

## The orbits lie in a plane

The first thing we want to establish is that the motion of a planet around the Sun is a 2-dimensional problem (and not 3D) as this makes the math easier.

We have the Sun at the origin and the position vector $\vec{r}$ pointing to the planet and its velocity $\vec{v}$. The vectors $\vec{r}, \vec{p}$ span some plane at a given instance in time. Because the Sun is fixed in the origin, the gravitational force from the Sun on the planet is:

$$
\vec{F}_g = -G \frac{M m}{r^2} \hat{r} \Rightarrow \text{  central force! }
$$

Consequently, $\vec{l} = const = \vec{r} \times \vec{p}$. The magnitude and the *direction* of the angular momentum are conserved and it is always perpendicular to the position and velocity vector. Thus the orbit of the planet will always stay in that plane. NB: this does not explain why all planets are in the same plane.

## Deriving the Second Law

"A line segment joining a planet and the Sun sweeps out equal areas during equal intervals of time." We found that the motion must be in a plane, hence we can use polar coordinates $r(t), \phi(t)$ to describe the position of the planet.

![Perkenwet](images/Perkenwet.png){ align="left" width="200" }

The angular momentum in [polar coordinates](central_forces.md#polar-coordinates) is:

$$
\vec{l} \equiv \vec{r} \times \vec{p} = 
\left ( \begin{array}{c}
r\\ 0 \\ 0
\end{array} \right )\times
\left ( \begin{array}{c}
mv_r \\ mv_\phi \\0
\end{array} \right )
=rmv_\phi \,\hat{z}= mr^2 \dot{\phi} \,\hat{z}
$$

Since $\vec{l}$ is constant, so must be its length: $l =mr^2 \dot{\phi} = const$.

![AreaPerkenWet](images/AreaPerkenWet.png){ align="left" width="200" }

The planet moves in $dt$ from $\vec{r} \rightarrow \vec{r} + d\vec{r}$ and $\phi \rightarrow \phi + d\phi$, sweeping out a small area $dA = \frac{1}{2} r\, rd\phi$, or:

$$
\frac{dA}{dt} =  \frac{1}{2} r^2 \dot{\phi} = \frac{l}{2m} = const !
$$

This is obviously an alternative way of stating K2, the Area Law. Note how little we needed to find this: the Sun is 'heavy' and hardly moves & gravity is a central force, that's it!

## Elliptic Orbits

With the help of statements (1) the orbits are planar, and (2) angular momentum is preserved, we give a glimpse of proving that the orbits are ellipses. See [below](kepler.md#exact-solutions-to-the-equation-of-motion) or the textbook (or your course notes) for an alternative derivation.

First, we find the potential that belongs to the gravitational force (since the force is central, we can anticipate that the potential only depends on the radial coordinate - think about this):

$$
V(r) = -\int_{ref}^{\vec{r}} \vec{F}_g \cdot d\vec{r} = - \int_{r_{ref}}^r Fdr = -G\frac{Mm}{r},
$$

where we took $r_{ref} \rightarrow \infty$.

Next, we start from the energy equation (with the velocity in [polar coordinates](central_forces.md#velcoity-and-acceleration)):

$$
E_{kin} + V(r) = \frac{1}{2} m \left ( \dot{r}^2 + r^2 \dot{\phi}^2 \right ) + V(r) = const = E
$$

As gravity is a central force, we can at any time use $l = mr^2 \dot{\phi} =const$. This allows us to eliminate $\dot{\phi}$:

$$
\frac{1}{2} m \dot{r}^2 +\frac{l^2}{2mr^2} + V(r) = const = E\quad (*)
$$

Now we have a 1-dimensional problem only in $r$ with an effective potential energy (shown as the black line in the figure):

$$
V_{eff}(r) = \frac{l^2}{2mr^2} -G\frac{Mm}{r}
$$

![EffectivePotentialKepler](images/EffectivePotentialKepler.png){ align="center" width="500" }

The first term in the potential is called the *centrifugal potential* as it includes the angular momentum.
Thus the equation of motion is:

$$
\frac{1}{2} m \dot{r}^2 + V_{eff}(r) = E
$$

### Qualitative solutions to the equation of motion

We can quickly identify various regimes for the solution depending on the energy $E$:

* $E<V_{min}$, no solution.
* $E = V_{min}$,  i.e., the minimum of $V(r)$. In this case there is nothing left for the radial part of the kinetic energy $\dot{r} = 0 \Rightarrow r=const$, thus the orbit is a circle (degenerated ellipse).

* $V_{min} < E <0$: the trajectory is bounded between some $r_a$ and $r_b$. 

    Just draw a horizontal line at some $E$-value in this range and you will see that this line intersects $U$ at two positions corresponding to line $r_a$ and $r_b$. Since the kinetic energy is never negative, the planet must be between these two $r$-values. The solutions in this energy range will give closed ellipses.

* $E \geq 0: r(t) \geq r_c$ with $r_c$ the intersection of the horizontal $E$ line for this case with $V(r)$: orbit is unbounded. The solutions in this energy range give hyperboles. 

    Note that $\dot{r}$ is the radial component of the velocity. If $\dot{r} = 0$, it doesn't mean that the planet velocity is zero! It can't be, because in this case the angular momentum would be zero, but it should always be present.
    
### Exact solutions to the equation of motion

We start from the energy equation $(*)$ 

$$
\frac{1}{2} m \dot{r}^2 +\frac{l^2}{2mr^2} -G\frac{mM}{r}= E
$$

and will now apply a series of very ingenious coordinate transformations to solve the equation of motion. Remember $\frac{dr}{dt}=\frac{dr}{d\phi} \frac{d\phi}{dt}$ with the substitution $u(\phi)\equiv 1/r(\phi) \Rightarrow r=1/u$ we can compute

$$
\frac{dr}{d\phi}=\frac{d}{d\phi}\frac{1}{u} = -\frac{1}{u^2}\frac{du}{d\phi}=-r^2 \frac{du}{d\phi} \Rightarrow \dot{r}= -r^2\dot{\phi}\frac{du}{d\phi} = -\frac{l}{m}\frac{du}{d\phi}
$$
    
with the constant angular momentum $l=mr^2\dot{\phi}$. If we substitute this in the energie equation, we obtain a 1D differential equation in $\phi$. This is already a bit easier as it is now in $u(\phi)$ not $r(t)$, but still the equation is quadratic

$$
\frac{l^2}{2m}\left ( \frac{du}{d\phi} \right )^2 + \frac{l^2}{2m}u^2 - GmMu=E
$$

With the constant $\alpha \equiv \frac{l^2}{GMm^2}$

$$
\alpha \left ( \frac{du}{d\phi} \right )^2 + \alpha u^2 -2u = \frac{2E}{GMm}
$$

The middle term is $\alpha u^2 -2u = \frac{1}{\alpha}(\alpha u-1)^2-\frac{1}{\alpha}$ and we again define $z\equiv \alpha u-1$

$$
\frac{1}{\alpha} \left ( \frac{dz}{d\phi} \right )^2 + \frac{1}{\alpha}z^2 -\frac{1}{\alpha}= \frac{2E}{GMm}
$$

Now we can see the elliptical form

$$
\left ( \frac{dz}{d\phi} \right )^2 + z^2 = \frac{2E\alpha}{GMm}+1 = \frac{2El^2}{(GMm)^2m}+1 \equiv e^2 
$$

The solution is now $z(\phi)=e \cos (\phi-\phi_0)$ and transforming back to $r=\frac{\alpha}{z+1}$ we obtain

$$
r(\phi) = \frac{\alpha}{e\cos (\phi-\phi_0)+1}
$$

This is the general form of a [conic section](https://en.wikipedia.org/wiki/Conic_section){ target="_blank"} (in Newton and Kepler's time that was an important (mathematical) topic to master as part of the classical/Greek education). For $e=1$ this is a parabol, $e>1$ a hyperbool and $e<1$ an ellipse ($e=0$ we get a circle). That is, only for $e<1$ is there a closed orbit, all other go to infinity. $e<1$ implies directly $E<0$ and if you look back to the effective potential we already have found this in the [qualitative analysis](kepler.md#Qualitative-solutions-to-the-equation-of-motion).

It is easier to see the elliptical form, if we transfer back to $x,y$ coordinates with $x=r\cos\phi,y=r\sin\phi$ and use the ellipse description  with origin at $(-ea,0)$

$$
\frac{(x+ea)^2}{a^2} + \frac{y^2}{b^2}=1
$$

With the long axis $a=\frac{\alpha}{1-e^2}=\frac{GMm}{2|E|}$ and the short axis $b^2=\alpha a=\frac{l^2}{GMm^2}a$. You can verify that for $e=0$ we find $a=b$ and the ellipse degenerates to a cricle.

---

You can also tackle the problem by starting from our earlier result on the [solution of the 1D equation of motion](conservation_laws.md/#integrating-the-equation-of-motion-in-1d).

$$
\begin{array}{rcl}
\dot{r} &=& \sqrt{\frac{2}{m}(E-U_{eff}(r))}\\
\dot{\phi} &=& \frac{l}{mr^2}
\end{array}
$$

By diving out the explicit time dependence of the equations we already have a differential equation in terms of $r(\phi)$.

$$
\frac{d\phi}{dr} = \frac{l}{r^2\sqrt{2m(E-U_{eff})}}
$$

By separation of variable we have the solution

$$
\phi-\phi_0 = l\int_{r_0}^{r(\phi)} \frac{dr}{r^2\sqrt{E-U_{eff}}}
$$

or 

$$
d\phi = \frac{l}{r^2}\frac{dr}{\sqrt{2m (E-\frac{k}{r}-\frac{l^2}{2mr^2})}}
$$

## Kepler 3

The area of an ellipse is $A=\pi ab$ and one period is $T$ (one year for the earth). From the derivation of the second law we found $A(t)=\frac{l}{2m}(t-t_0)$. Taking these together and using the connection of the short $b$ to the long axis $a$ from the elliptic orbits $b^2=a\frac{l^2}{GMm^2}$

$$
T = \frac{2\pi abm}{l} \Rightarrow T^2 = \frac{4\pi^2 m^2}{l^2}\frac{l^2a}{GMm^2}
$$

or in other words

$$
\frac{T^2}{a^3} = \frac{4\pi^2}{GM}=const.
$$

The ratio $\frac{T^2}{a^3}$ is the same for each planet.

## The sun as a point mass

In the above we assumed that the sun of mass $M_{sun}=2\cdot 10^{30}$ kg with a radius of $R_{sun}= 7\cdot 10^6$ km can be viewed as a point mass with the same mass for using Newton's law of gravity. We just plucked in the mass, we did not bother about the mass distribution at all!

In fact this is allowed as long as the mass distribution is spherically symmetric as you will see below. We start from the gravitational (1D) potential $V(x)=-G\frac{mm'}{x}$. For this potential holds $\nabla V = -\vec{F}_g=-G \frac{mm'}{x^2}\hat{x}$. The derivation has a number of clever tricks to work out the integral and optimal choice of coordinates.

In fact that actual mass distribution of the sun does not matter, even if it would be non spherical. That is because the size of the sun is small compared to the distance to the earth. Any differences in the gravitational force are absolutely tiny because that distance is so large (radius of the sun is $7\cdot 10^5$ km and the distance to the earth is $>10^8$ km.
. 
We consider a massive spherical shell with surface mass density $\sigma$ (per unit area). Now compute the potential at some point $P$ on the surface with coordinates $(r,\phi)$ with some distance $x$ from the center of the shell and distance $w$ from a point on the surface. The potential is given as

![Coordinates for spherical mass](images/sunpoint.png){ algin="center" width="350"}

$$
dV = -G\frac{m\sigma dA}{w} = -2\pi G \frac{m\sigma r^2\sin\theta \,d\theta}{w}
$$

with $m'=\sigma dA$, $dA=2\pi r \sin\theta\, rd\theta$ the revolution of the line element around $2\pi$. If we additionally use the relation $w^2=x^2+r^2-2xr\cos\theta$ to allow integration over $w$ rather than $\theta$, the differential is $wdw = xr \sin\theta\,d\theta$. This is very similar to what we have in the nominator above, thus

$$
dV = -2\pi G \frac{m\sigma rdw}{x} 
$$

If the point $P$ is outside the shell, the angles varies $\theta \in [0,\pi]$ or $w\in[x-r,x+r]$ and we can calculate the potential

$$
V(x) = -2\pi G \frac{m\sigma r}{x}\int_{x-r}^{x+r} dw = -\frac{4\pi Gm\sigma r^2}{x} 
$$

With the mass of the shell $M=4\pi r^2\sigma$ we finally obtain

$$
V(x) = -\frac{GMm}{x} 
$$

We see indeed that the mass distribution integrates out and we can deal with a point mass concentrated at the center of the shell. Hurray!

### Gravity inside a shell

The gravitational force *inside* a shell (hollow sphere) is *zero* - always - independent where inside of the shell you are. This result will pop up again in electrostatics where the electric force inside a charged spherical surface is zero. As the form of the gravitational and electric force are similar in their functional form $\vec{F}(\vec{r})\propto r^{-2}\hat{r}$ this is not surprising.
This a bit counter intuitive result is easy to see, if you apply [Gauss theorem](https://en.wikipedia.org/wiki/Gauss%27s_law_for_gravity){ target="_blank"}. 

$$
\int_S \vec{f}\cdot dA = \int_V \nabla 
\cdot \vec{f}\, dV =4\pi G M \quad (\text{or } \frac{q_1}{\epsilon_0})
$$

where $\vec{f}$ is the gravitational $\vec{f}=\frac{\vec{F}_g}{m}=-GM\frac{\hat{r}}{r^2}$ or electric field $\vec{f}=\frac{\vec{F}_E}{q_0}= \frac{1}{4\pi\epsilon_0}\frac{q_1 \hat{r}}{r^2}$. The mass $M$ (or charges $q$) is the sum of the mass *inside* the volume that the surface $S$ encloses. If the surface does not enclose any mass (or charge), as is the case inside the shell, then the flux ($\vec{f}\cdot dA$) and field must be zero!

You can also integrate out the force as [done here](http://hyperphysics.phy-astr.gsu.edu/hbase/Mechanics/sphshell2.html){ target="_blank"} but that is quite some work.

## Speed of the planets & dark matter

Starting from Kepler 3, we can compute the orbital speed of a planet around the sun

$$
\begin{array}{rcl}
T^2 &=& \frac{4\pi^2}{GM}a^3\\
\omega^2 &=& \frac{GM}{a^3}, \quad T=\frac{2\pi}{\omega} \text{ & }\omega=\frac{v}{r}, a\approx r\\
\Rightarrow v &=& \sqrt{\frac{GM}{r}}
\end{array}
$$

Indeed if we measure the speed of the planets in the solar system this prediction holds, the velocity drops with the distance from the sun as $\propto r^{-1/2}$ (see figure). As $M$ we use the mass of the sun here.

![Orbital speed](images/orbitalspeed.png){ width=450}

The distance is measured in [Astronomical Units [AU]](https://en.wikipedia.org/wiki/Astronomical_unit){ target="_blank"}, the distance from the earth to the sun (about 8.3 light minutes). Note that the earth is moving with an unbelievable 30 km/s, that is $10^5$ km/h! Do you notice any of that? We will use this motion later with the [Michelson-Morley](special.md) experiment.

If we plot the same speed versus distance curve not for the planets in our solar system, but for stars orbiting the center of our galaxy, the milky way, then the picture looks very different. The far away stars orbit at a much higher speed than expected and the form of the found curve does not match $\propto r^{-1/2}$. 

![Orbital speed](images/orbitmilkyway.png){ width=400}

This mismatch is not understood to this day! The mass $M$ here is calculated from the visible stars and the super massive black holes at the center of the galaxy. But even if the mass is calculated wrongly, the shape of the dependency does not match. It turns out, this mismatch is observed in all galaxies! Apparently the law of gravity does not hold for large distances *or* there must be extra mass that increases the speed that we do not see. This mismatch has lead to the postulation of [*dark matter*](https://en.wikipedia.org/wiki/Dark_matter){ target="_blank"} and [alternative formulation](https://en.wikipedia.org/wiki/Alternatives_to_general_relativity){ target="_blank"} for the laws of gravity. This is the most disturbing problem in physics today; second is probably the interpretation of [measurement](https://en.wikipedia.org/wiki/Measurement_in_quantum_mechanics){ target="_blank"} in quantum mechanics (collapse of the wave function/Kopenhagen interpretation of Quantum Mechanics; multiverse theories).  The majority of all matter in the universe is believed to be *dark*. And we have no clue what it could be! Most scientist even think it must be [non-baryonic](https://en.wikipedia.org/wiki/Baryon){ target="_blank"}, that is, other stuff than our well-known protons or neutrons. It remains most confusing.

The usual distance unit for distances in astronomy outside the solar system is not light years [ly], but [parsec](https://en.wikipedia.org/wiki/Parsec){ target="_blank"} [pc], or kpc, or Mpc. One parsec is about 3.3 ly (or $10^{13}$ km). Note: Visible stars to the eye are typically not more than a few hundred parsec away. The milkyway is perfectly visible to the naked eye as a band/stripe of "milk" sprayed over the night sky. But you cannot see it anywhere close to Delft, there is much too much light from cities and greenhouses. Go to Scandinavia in the winter ("wintergatan") or any place remote where there are little people. The reason you see a "band" in the night sky, is that the milky way is a spiral galaxy, sort of pancake shaped, and you see the band in the direction of the pancake. 
