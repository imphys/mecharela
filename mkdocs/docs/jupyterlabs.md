# Jupyter labs

!!! summary "Learning goals"
    - Learn how to solve equations of motion with a computer
    - First experience with numerical methods

There are only very few labs to practice. This is a first attempt to introduce solving physics problems with a computer already early on in the curriculum in the academic year 22/23.

## Installation of Jupyter lab enviroment
Before you can start you need to install a Jupyter lab instance on your computer. To this end please install [anaconda](https://www.anaconda.com/){ target="_blank"} or (fully graphical) [miniconda](https://docs.conda.io/en/latest/miniconda.html){ target="_blank"} (for the nerds). The only packages needed are numpy and matplotlib.

## Jupyter labs

An [introduction.ipynb](resources/ClassicalMechanicsSpecialRelativity-intro.ipynb) to get started with the topic. Here all is already filled in, it is for you to learn and see the concept how to tackle solving the equation of motion $F=ma$ (second order differential equation) with a computer to find $v(t)$ and $x(t)$ for a given force.
	
Below are exercises for you to work with. These exercises are also part of the *werk colleges*.
	
2. Falling with friction part 1	
	[Exercise1.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex1.ipynb)
3. Falling with friction part 2
	[Exercise2.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex2.ipynb)
4. Trajectory of a ball at an angle
	[Exercise3.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex3.ipynb)
5. Mass-spring system
	[Exercise4.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex4.ipynb)
6. Pendulum with a spring
	[Exercise5.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex5.ipynb)