# Open MechaRela Notes

Classical mechanics is the starting point of physics. Over the centuries, via [Newton's](https://en.wikipedia.org/wiki/Isaac_Newton){ target="_blank"} three fundamental laws formulated around 1687, we have built a solid framework describing the material world around us. On these pages, you will find a concise summary, demos and exercises for studying introductory classical mechanics. Moreover, we will consider the first steps of [Einstein's](https://en.wikipedia.org/wiki/Albert_Einstein){ target="_blank"} Special Theory of Relativity published 1905.

![Newton](images/Newton.jpg){ align=left height="100" }
![Einstein](images/Einstein.jpg){ align=left height="100" }

This material is made to support first year students from the BSc Applied Physics at Delft University of Technology during their course *Classical Mechanics and Relativity Theory*, MechaRela for short.

The textbook used in this course: [W.D. McComb, Dynamics and Relativity](https://www.bol.com/nl/nl/p/dynamics-and-relativity/1001004001115962/){ target="_blank"}, Oxford University Press.

With these notes our aim is to provide learning material that is:

* self-contained
* easy to modify and thus improve over the years
* interactive, provide additional demos and exercises next to the lectures

From previous years there are lecture recordings present from Collegerama of BSc TN: TN1612TU 2021/22. We link the corresponding recording with the online notes where possible.

## Reporting an issue

Although these notes have been checked there might be some errors or typos. We would appreciate it immensely if you would report these *bugs* when you find them. This can be done via the **Issues** menu on the GitLab repository:

* <https://gitlab.tudelft.nl/imphys/mecharela/-/issues>

*Note: you will need to login, preferably with your netid*

Your issue will be picked up by one of the authors. You are also very welcome to write any suggestions or addition you have for this website via these **Issues**.

Before we start with the content, have a look at how to solve a physics problem.

## How to solve a physics problem?

One of the most common mistakes made by 'novices' when studying problems in physics is trying to jump as quickly as possible to the solution of a given problem or exercise. For simple questions, this may work. But when stuff gets more complicated, it is almost a certain route to frustration.

There is, however, a structured way of problem solving, that is used by virtually all scientists and engineers. Later this will be second nature to you, and you apply this way of working automatically. It is called IDEA, an acronym that stands for:

![IDEA](images/IDEA.jpg){ align="center" width="400" }

* **Interpret** - First think about the problem. What does it mean? Usually, making a sketch helps. Actually  _always start with a sketch_;
* **Develop** - Build a model, from coarse to fine, that is, first think in the governing phenomena and then subsequently put in more details. Work towards writing down the equation of motion and boundary conditions;
* **Evaluate** - Solve your model, i.e. the equation of motion;
* **Assess** - Check whether your answer makes any sense (e.g. units OK?).

We will practice this in the course and we will see that it actually is a very relaxed way of working and thinking. We strongly recommend to apply this strategy for your homework and exams (even though it seems strange in the beginning).
The first two steps (Interpret and Develop) typically take up most of the time spend on a problem.

## Demos

Most pages contains some demos and examples. [My physics lab](https://www.myphysicslab.com/) website has many demos on a very diverse set of physics problems. Some we will link at the corresponding page. Visit the demos and play with them, try to understand what is happening and why, have fun!


## Concise Overview on Mechanics

For a most concise overview of the basics click on the image below and scroll over the tiles. This covers the mechanics part of the course only, but this is most often the hardest part of the students.

[![classical mechanics tiles](images/ClassicalMechanicsTiles.jpg)](NewtonBasics/NewtonBasics.html)

## Lecturers

* Prof. Rob Mudde
* Prof. Bernd Rieger, <a href="mailto:b.rieger@tudelft.nl">b.rieger@tudelft.nl</a>, Office F266 in the old physics building (number 22)
