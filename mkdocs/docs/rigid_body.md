# Rigid Body

!!! note "Textbook & Collegerama"
    - Chapter 6: sections 6.1 & 6.2
    - TN1612TU_17, _18, _ 19, _20

!!! summary "Learning goals"
    - Understand analogy angular and linear motion
    - Calculate moments of inertia
    - Derive equations of motion for combined rotation and linear motion
    - Understand precession

Note: Students find the motion of the rigid body the most difficult part of the course. Be warned.

## Finite-Sized Objects

Newtonian mechanics is, in its beginning, a theory for point particles. In some instances, reducing an extended object to a point is allowed as when considering the [gravity exerted by a spherical object](kepler.md#the-sun-as-a-point-mass) like the Sun and image the Sun as a point with the total mass concentrated there. In many practical cases, however, it also is not. 

![BallAndPointParticle](images/BallAndPointParticle.jpg){ align="right" width="200" }

For example, a ball rolling down a slope and comparing its motion to the frictionless sliding of a point particle along the slope. You will find that the rolling ball is slower than the sliding particle. Note that *rolling* always means that there must be some friction, otherwise the object would not rol, but slide. That is not a surprise if we consider the energy of both objects: the sliding point particle loses potential energy and gains an equal amount of kinetic energy (ignoring friction). The rolling ball, of course, also loses potential energy and gains kinetic energy. But it has to split this kinetic energy over kinetic energy associated with its translation and that associated with its rotating. Consequently, it has less kinetic energy of translation and thus translates with a lower velocity than the point particle.

## Constraining Forces

We have seen that the [total momentum](many_particles.md#linear-momentum) of a many-body system responds only to external forces. All internal forces, i.e., from one of the particles to another, always have a corresponding opposite force thanks to N3 action = -reaction. We don't need to know the details of these interaction forces.

We can exploit this by assuming that the interaction forces in a rigid body are such that they keep the relative position of the particles *fixed at all times*. I.e. the object's shape cannot change, it is called *rigid*. We can translate it; we can rotate it. But we cannot deform it - change the relative position of the particles that make up the object. This is something that we can apply to our mechanical problems in daily life. Most objects don't change shape if we put a force on them. Or put somewhat better: the change of shape is unnoticeable to us. In reality, there must be a small change in shape (causing acoustical waves) and the atoms making up a solid body always vibrate a bit as by their thermal energy.

## Angular Velocity and Acceleration

Rotation is always around some axis. This axis can be indicated by a vector. By assigning the axis a vector property we can implicitly define the rotation direction (clockwise/ counter clockwise) around the axis, otherwise it would not matter for the rotation axis if it points up or down. This axis can be fixed in space (e.g. a spinning wheel on a fixed axis) or change as a function of time, the rotation axis rotates itself around another axis (e.g. a spinning top at an angle to the normal).

![SolidBodyCylinder](images/SolidBodyCylinder.jpg){ align="right" width="200" }

For the moment we only consider rotation about a fixed axis,  see figure. We can describe the positions of all particles constituting the cylinder by specifying the location of its center of mass, the direction that the axis is pointing, and the angle of rotation $\theta$ around the axis.

As the angle $\theta(t)$ can change in time, we also need to consider the rate of change, the angular velocity.

$$
\omega \equiv \frac{d \theta}{dt}
$$

However, this does not tell us anything about the orientation of the axis around which the cylinder is rotating. For a given $\omega$ the axis can point in any direction. It is like telling you the magnitude of the velocity of an object but not giving you any clue in which direction that object is moving.

We can easily cure this by realizing that $\omega$ actually has a direction, like velocity. If we treat it like a vector, the direction of the vector will give us the orientation of the rotation axis. Thus, from now on, we will use the angular velocity vector $\vec{\omega}$. Here the [right hand rule](https://en.wikipedia.org/wiki/Right-hand_rule#Rotations){ target="_blank"} applies. Use your thumb in the $\vec{\omega}$ direction to determine the direction of positive rotation in the direction of the other fingers. I.e. if the thumb is sticking up, counter clockwise is the positive direction. 

The vector angular acceleration is:

$$
\vec{\alpha} \equiv \frac{d \vec{\omega}}{dt}
$$

The relation between the angular velocity $\vec{\omega}$ and the linear velocity of a particle $s$ constituting the rigid body is given by

$$
\vec{v}_s = \vec{\omega} \times \vec{r}_s
$$

![AngularVelocity](images/ang_vel2.png){ align="right" width="200" }

as the angular velocity is the same for all particles and the linear velocity must increase with distance from the rotation axis $\vec{r}_s$. Please note the vector character of this equation. All the quantities are vectors and the cross product relates them.

## Moment of Inertia

Now we consider the angular momentum of one particle $s$: $\vec{l}_s=\vec{r}_s \times \vec{p}_s = \vec{r}_s \times m_s(\vec{\omega}\times \vec{r}_s)=m_sr^2_s\vec{\omega}$. For the total angular momentum we find

$$
\vec L = \sum \vec{l}_s = \sum m_s r^2_s \vec{\omega}
$$

assuming $\vec{\omega}\perp \vec{r}_s$. The sum over the mass and positions is called *moment of inertia*

$$
I\equiv \sum m_s r^2_s = \int_M r^2 \, dm
$$

Note: $r$ here indicates the _distance to the rotation axis_ through the CM! Axes not through the CM are [treated below](rigid_body.md#parallel-axes-theorem). The moment of inertia represents a characteristic property of the shape and mass distribution of a specific solid body. It represents its resistance against change in rotation speed around a certain axis, as  we see here:

$$
\vec L = I \vec{\omega}\quad \& \quad\dot{\vec{L}} = \vec{r} \times \vec{F} = \vec{\Gamma}_{ext}
$$

Now we see the analogy to N2 ($m\dot{\vec{v}}=\vec{F}$):

$$
I\dot{\vec{\omega}} = \vec{\Gamma}
$$

Thus $I$ takes over the roll of the mass $m$ as the object property resisting a change in motion. The unit of $I$ is [kg m$^2$], $\dot{\vec{\omega}}$ is [1/s$^2$] and $\Gamma$ is [kg m$^2$/s$^2$].

In the absence of an external torque $\Gamma =0$ it follows that $\dot{\vec{\omega}}=0$, i.e. the angular velocity stays constant in magnitude and direction. The rotation axis remain constant. This is in complete analogy to N2 and N1. As an example think about earth's rotation axis. To first order there is no external torque, therefore the rotation axis remains inclined while the earth orbits the sun. In turn this leads e.g. to different seasons away from the equator.

The moment of inertia is a property with respect to the rotation around a given axis. We will specify the quantity always with respect to one specific rotation axis. You can avoid this specification if you compute the moment of inertia for "all axis at the same time". It is clear that $I$ cannot be a scalar property then. From $I\dot{\vec{\omega}} = \vec{\Gamma}$ we see that if we apply a vector ($\vec{\omega}$) to $I$, then it must return a vector ($\vec{\Gamma}$), therefore $I$ needs to be a "second rank tensor". See section 6.4 in the book for this extension and [here](beyondNewton.md#inertia-tensor).

---

The equation relating the angular momentum to the angular velocity via the moment of inertia $\vec L = I \vec{\omega}$, assumes that we are in the CM system. If that is not the case, the equation needs an extra term to include the angular momentum of the CM

$$
\vec{L} = \vec{R} \times \vec{P} + I_{CM}\vec{\omega}
$$

### Parallel axes theorem

This theorem (known as Steiner's theorem) lets you compute the moment of inertia $I'$ through an axis parallel to an axis through the CM with distance $d$ (of which you know the moment of inertia $I$). Consider the situation as in the sketch.

![Steiner theorem](images/steiner.png){ align="right" width="250" }

$$
\begin{array}{rcl}
I' &= &\sum m r_i^{'2}=\sum (\vec{d}+\vec{r}_i)\cdot (\vec{d}+\vec{r}_i)\\
&=& \sum m_i d^2 + \sum 2m_i \vec{d}\cdot \vec{r}_i + \sum m_i r^2\\
&=& Md^2 + 2\vec{d} \cdot \sum m\vec{r}_i + \sum m_i r_i^2\\
&=& Md^2 + I_{CM}
\end{array}
$$

The term $\sum m\vec{r}_i=0$ by definition of the relative positions in the [CM frame](many_particles.md#center-of-mass) and the last term is just the moment of inertia through an CM axis.

### Radius of Gyration

The radius of gyration $R_G$ is defined by assigning an easy to visualize (and compareable) moment of inertia to an irregular shaped object with known moment of inertia $I$ and mass $M$. The moment of a point mass $M$ at distance $R$ from the rotation axis is $MR^2$ of course, therefore we can define the *radius of gyration*

$$
R_G = \sqrt{I/M}
$$

You know at what distance from the rotation axis a point of the same mass has an equivalent moment of inertia. For example in biophysics this is used to describe the dimensions of a [polymer chain](https://en.wikipedia.org/wiki/Radius_of_gyration){ target="blank" }.

## Kinetic Energy of Rotation

We can computed the kinetic energy of rotation from $E_{kin} = \sum \frac{1}{2} m_s v^2_s = \sum \frac{1}{2} m_s \vec{v}_s\cdot \vec{v}_s$ and substituting $\vec{v}_s = \vec{\omega} \times \vec{r}_S$

$$
E_{kin} = \sum \frac{1}{2} m_s r^2_s \omega^2 = \frac{1}{2}I\omega^2
$$

under the assumption $\vec{\omega} \perp \vec{r}$, i.e. the moment of inertia $I$ corresponds to the rotation around the axis given by $\vec{\omega}$. Again this can be generalized to rotations around any axis, but requires then the introduction of the [inertia tensor](beyondNewton.md#inertia-tensor) such that $E_{kin}=\frac{1}{2}\vec{\omega}I\vec{\omega}$ remains a scalar.

Please notice the analogy to the kinetic energy of linear motion $\frac{1}{2}mv^2$, the linear velocity is replaced by the angular velocity and the mass by the moment of intertia. Exactly what you would expect.

## Gyroscope & Precession

![tol coordinates](images/tol_coord.png){ align="right" width="200" }

Rotating bodies are of large interest as the earth itself is rotating, but also technically for [gyroscopes](https://en.wikipedia.org/wiki/Gyroscope){ target="blank" }. Here we consider a spinning top. Although this is a children's toy, the physics are quite involved. The description of the motion of the earth around the sun is governed by the same ideas. Note that the rotation axis of the earth is inclined to the plane it moves around the sun by 23.5 degrees, but as there is no external torque to first approximation, the axis remains stable while the earth circles the sun (due to $I\dot{\vec{\omega}}=\vec{\Gamma}=0$). This leads e.g. to different seasons on the northern and southern hemisphere. To second order there is a small torque due to the non-uniformity of gravitational field from the sun and the moon on the earth, resulting a similar motion as for the spinning top under the influence of gravity.

Looking at the figure of the spinning top under the influence of gravity we see the there is a torque applied to the system.

$$
I\dot{\vec{\omega}}= \vec{\Gamma} = \vec{R}\times m\vec{g}
$$

The rotation axis is parallel to $\vec{R}$, therefore $(\vec{R}\times m\vec{g}) \perp \vec{\omega}$

$$
\begin{array}{l}
\Rightarrow \vec{\omega} \cdot I\dot{\vec{\omega}}=\vec{\omega}\cdot (\vec{R}\times m\vec{g}) = 0\\
\Rightarrow \vec{\omega} \perp \dot{\vec{\omega}}\\
\frac{1}{2}\frac{d}{dt}\vec{\omega}^2=\vec{\omega}\cdot\dot{\vec{\omega}}=0\\
\Leftrightarrow \|\vec{\omega}\| = const.
\end{array}
$$

We find that the angular velocity remains constant in magnitude and only the direction can change! Therefore the axis $\hat{\omega}$ must move in a circle (around the normal); the axis describes a cone in space. This type of motion is called *precession*. See here for a movie of [precessing gyroscope](https://en.wikipedia.org/wiki/Precession){ target="_blank"}.

If we now want to write down the equations of motion for the spinning top, we write $\dot{\vec{\omega}}=\omega_0 \dot{\hat{\omega}}$ as only the direction changes.

$$
\begin{array}{rcl}
I\omega_0 \dot{\hat{\omega}} &=& R\hat{\omega} \times (-mg\hat{z})\\
\Rightarrow \dot{\hat{\omega}} &=& \frac{mgR}{I\omega_0} \hat{z} \times \hat{\omega} = \vec{\Omega}_p \times \hat{\omega}
\end{array}
$$

with the precession frequency $\vec{\Omega}_p = \frac{mgR}{I\omega_0} \hat{z}$ around the normal as seen by $\hat{z}$. Now we write out the cross product in the components

$$
\begin{array}{rcl}
\frac{d}{dt}\hat{\omega}_x &=& -\Omega_p \hat{\omega}_y\\
\frac{d}{dt}\hat{\omega}_y &=& +\Omega_p \hat{\omega}_x\\
\frac{d}{dt}\hat{\omega}_z &=& 0
\end{array}
$$

Eliminating either the $x$ or the $y$ component gives a differential equation for the axis components

$$
\frac{d^2}{dt^2}\hat{\omega}_x + \Omega_p^2 \hat{\omega}_x =0
$$

with (harmonic) solution

$$
\hat{\omega}_x (t) = A \sin (\Omega_p t) + B \cos (\Omega_p t)
$$

Now we again see that the $\vec{\omega}$ axis rotates with angular frequency $\Omega_p$ around the $\hat{z}$ axis. As $\frac{d}{dt}\hat{\omega}_z = 0$ the height of the axis remain stable, and it moves over a cone. We can see that the larger $\omega_0$, the smaller $\Omega_p$ and the more stable the gyroscope will be. (Ask yourself: What do we mean with stable here? And why is this the case?) The opening angle of the cone is given by the initial tilt of the spinning top - it does not change under the influence of gravity. Gravity only induces the precession (a motion perpendicular to the action of the gravity via the torque).

As the gravitational pull of the sun and moon on the earth is not uniform, also the earth experiences a torque and precesses. The precession periode of the earth is very long compare to modern human history with 26,000 years, but short compared to geological times.

## Comparison linear & rotational motion

$$
\begin{array}{lll}
\text{moment of inertia }&I & \text{mass }m\\
\text{torque}& \vec{\Gamma}=\vec{r}\times\vec{F} & \text{force } \vec{F}\\
\text{angular momentum}&\vec{L}=I\vec{\omega} & \text{momentum } \vec{p}=m\vec{v}\\
\text{equation of motion}& \vec{\Gamma}=I\dot{\vec{\omega}}=\dot{\vec{L}} & \vec{F}=m\dot{\vec{v}}=\dot{\vec{p}} \\
\text{work}& dW=\vec{\Gamma}\cdot d\theta & dW=\vec{F}\cdot ds\\
\text{energy}&E_{kin}=\frac{1}{2}I\omega^2 & E_{kin}=\frac{1}{2}mv^2\\
\end{array}
$$

## Worked Examples

1. Moment of inertia of a massive cylinder, constant density, length $L$ with outer radius $b$ and inner radius $a$ that is rotating around the long axis.

    $$
    I=\int_M r^2\, dm
    $$

    For bodies with constant density *always* first transform the mass element $dm$ into a volume element $dm=\rho dV$. The volume element in cylindrical coordinates, with $r$ the distance from the rotation axis is $dV = L 2\pi r \, dr$

    $$
    I = 2\pi \rho L \int_a^b r^3\,dr = 2\pi \rho L \frac{1}{4}(b^4-a^4)
    $$

    Now we can simplify this always by bringing in the total mass of the object, here that is $M=\rho L\pi (b^2-a^2)$. We obtain

    $$
    \begin{array}{rcl}
    I &=& \frac{1}{2}M(b^2+a^2) \\
    I_{full}  &=& \frac{1}{2} MR^2  (a=0)\\
    I_{thinwall}&=& MR^2  (a=b)
    \end{array}
    $$

    Notice that a thin walled cylinder has a higher moment of inertia than a full cylinder of the same mass. That is because the distribution of the mass with respect to the rotation axis matters!

1. A solid round disk is hanging from the ceiling suspended via a massless staff. The system is oscillation around the equilibrium position just as with the [pendulum](pendulum.md) treated earlier.

    ![pendulum with disk](images/slingerklok.png){ align="right" width="150" }

    If we want to derive the equation of motion for the system, we need to setup "N2" for rotation

    $$
    I\dot{\vec{\omega}} = \sum \vec{\Gamma} = \vec{x} \times \vec{F}_g 
    $$

    The torque applied by gravity is pulling the disk inward and we have $\vec{x} \times \vec{F}_g = -\ell Mg\sin\varphi$. Furthermore, we see that the disk (with $I_{CM}=\frac{1}{2}MR^2$ just think of the disk as a very thin full cylinder which we calculated above) is rotating not around an CM axis (which is perpendicular to the screen plane), but around the  an axis perpendicular to the screen through the pivot point. This axis is parallel to the CM axis and we can apply Steiner's theorem to compute the appropriated moment of inertia as $I=M\ell^2+I_{CM} = M\ell^2 + \frac{1}{2}MR^2$. Substituting these findings into "N2" together with $\omega=\dot{\varphi}$, we obtain

    $$
    (M\ell^2 + \frac{1}{2}MR^2) \ddot{\varphi}+\ell Mg \sin\varphi =0
    $$

    If you compare this to the [equation](pendulum.md#equation-of-motion) of motion we obtained earlier for the pendulum only the extra term $\frac{1}{2}MR^2$ has entered accounting for the moment of inertia of the disk.

1. We will compute the moment of inertia of a solid sphere, radius $R$ and mass $M$. There are two approaches a) via setting up the problem in spherical coordinates and the volume element or b) via sectioning the sphere into thin disks of different radii.
    1. Via the volume element.
        ![Sphere inertia volume element](images/Isphere_volume.png){ align="right" width="200" }

        Realize that $r$ is the distance to the rotation axis and $s$ is the distance to the volume element $dV$ (typically many mistakes happen in this step), therefore the volume element is given by $dV=s^2 \sin\theta \,ds d\theta d\varphi$

        <br/><br/>

        $$
        \begin{array}{rcl}
        I&=&\displaystyle{\rho \int_V r^2 \,dV = \rho \int_V d\varphi\, d\theta \sin \theta \, ds\, s^2 (s \sin\theta)^2}\\
        &=&\displaystyle{\rho \int_0^{2\pi} d\varphi \int_0^\pi d\theta \sin^3 \theta \int_0^R ds\, s^4}\\
        &=& \rho 2\pi \frac{4}{3}\frac{1}{5}R^5 = \frac{2}{5}MR^2 
        \end{array}
        $$

        This approach only works if you can evaluate the finite integral over $\sin^3\theta$ which is not straight forward, but doable (and is equal to $4/3$).

    1. Here we slice the sphere in thin disks ($I_{disk}=\frac{1}{2}MR^2$) as shown in the figure ![Sphere inertia volume element](images/Isphere_slice.png){align="center" width="200"} 

        $$
        I=\int_V r^2\, dm=\int_{-R}^{+R} I_{disk}
        $$

        The distance to the rotation axis is $r$ and we integrate over $z$. Now we have

        $$
        I_{disk} = \frac{1}{2}r^2dm = \frac{1}{2}r^2 \pi\rho  r^2\,dz
        $$

        This gives us a much easier integral to solve now and we arrive at the same result as before

        $$
        I = \frac{1}{2}\rho\pi \int_{-R}^{+R} r^4\,dz = \frac{1}{2}\rho\pi \int_{-R}^{+R} (R^2-z^2)^2\,dz =\frac{2}{5}MR^2
        $$

1. A massive cylinder mass $M$, radius $R$ is rolling down an inclined slope (inclination angle $\theta$) without slipping. Compute the linear acceleration down the slope.

	![Cylinder down](images/cylinderrolling.png){ width="250"}

	There are several ways towards the solution and we will follow the idea of energy conversation (such that we do not need to worry too much about the forces/torque). Other approaches are via the friction force and torque from $\vec{F}=m\vec{a}$ and $\vec{\Gamma}=I\dot{\vec{\omega}}$ or directly via the torque around the point where the cylinder touches the slope.

	If the cylinder is rolling without slipping, its angular and linear velocity are related as $v=\omega R$.

	The total energy is given by

	$$
	E=\frac{1}{2}Mv^2+\frac{1}{2}I\omega^2+Mgh
	$$

	The moment of inertia of a massive cylinder was $I=\frac{1}{2}MR^2$. If the cylinder moves down over a distance $x$ the height is lowered by $x \sin\theta$.
	
	$$
	E = \frac{1}{2}Mv^2+\frac{1}{2}\frac{1}{2}MR^2 \left ( \frac{v}{R} \right )^2 -Mgx\sin\theta
	$$

	Now invoking conservation of energy we take the time derivative of this expression to obtain
	
	$$
	Mv\dot{v}+\frac{1}{2}Mv\dot{v}-Mgv\sin\theta = 0
	$$

	Here we can divide out the mass $M$ and the velocity $v$ as it is non-zero, to obtain
	
	$$
	a+\frac{1}{2}a-g\sin\theta=0 \Rightarrow a = \frac{2}{3}g\sin\theta
	$$

	??? "Is this acceleration larger or smaller than for a point mass?"
		
		The acceleration is smaller than for a point mass. You can reason this as some energy needs to be transferred to rotating the cylinder, but that is not needed for a point mass. For the latter the acceleration is simply $g\sin\theta$. NB: You are faster down a hill if you slip e.g. on skis than you are with your bike (without pushing the pedals).
		
1. Two masses sliding on a rotating stick	
	We have the following situation. A stick with two masses $m$, located at the end of a stick with length $2R$, is rotating. In the center of the stick are two smaller masses of $m/2$. Initially they are in the center (situation 1 to the left), but as the stick is rotating with $\omega_1$ they move to the outside and join the other masses (situation 2 on the right).
	
	![situation 1](images/rot_mass1.png){ width="250"} ![situation 1](images/rot_mass2.png){ width="230"}
		
	What is the rotation speed $\omega_2$ in situation 2 after the masses of $m/2$ moved to the side?
	
	Because there are only internal angular momenta and no external, the total angular momentum is conserved.
	
	$$
	\begin{array}{rcl}
	\vec{L}_1 &=& \vec{L}_2 = \vec{r}\times \vec{p}\\
	2\cdot R\, m \omega_1 R &=& 2\cdot \frac{3}{2} R\, m \omega_2 R
	\end{array}
	$$
	
	Thus we find $\omega_2 = \frac{2}{3}\omega_1 < \omega_1$.
	
	??? "Is $E_{kin}$ conserved here?"
	
		No. The masses $m/2$ do not magically move to the side. It may look as the stick is rotating, but if you place yourself into the frame of the stick, you see directly that work needs to be done to move the small masses to the side. Thus the kinetic energy in situation 2 is *smaller* than in situation 1. Therefore solving for the rotation speed via $\frac{1}{2}I_1\omega_1^2 = \frac{1}{2}I_2\omega_2^2$ is not correct.
		
1. "Atwood machine"

	An [Atwood machine](https://en.wikipedia.org/wiki/Atwood_machine){ target="_blank"} consists out of a pulley, sometimes considered massless, over which a string is connected to two masses. Here we consider the pulley as a solid disk with mass $M$ and radius $R$, having a moment of inertia $I=\frac{1}{2}MR^2$ (Check that you can compute this simple moment of inertia easily by now). 

	![Atwood machine](images/atwood.png){ width="250" align="right"}
	
	Task: Compute the acceleration on the two masses.

	First we need to recognize that the positions, velocities and accelerations of the two masses are connect to each other, but also to the pulley via the string. If mass 2 goes down with $\vec{a}$ then mass 1 goes up with $-\vec{a}$. The angular acceleration of the pulley is $\alpha = a/R$ and also connected to the motion of the masses, if the string is not slipping.

	We solve for the acceleration by considering the forces in the string $T_1,T_2$ and the torque $\Gamma$ on the pulley. 

	We chose the positive coordinate axis down as $m_2>m_1$. Then the angular acceleration is also positive as is the torque. The forces acting on the masses and the torque on the pulley are

	$$
	\begin{array}{rcl}
	m_1g-T_1 &=& -m_1a\\
	m_2g-T_2 &=& m_2a\\
	\Gamma = (T_2-T_1)R &=& I \alpha =\frac{1}{2}MR^2\frac{a}{R}
	\end{array}
	$$

	Solving the first two equations for $T_1$ and $T_2$ and substituting in the third, we obtain

	$$
	m_2(g-a)-m_1(g+a) = \frac{1}{2}Ma
	$$

	and after solving for the acceleration $a$

	$$
	a= g\frac{m_2-m_1}{m_2+m_1+\frac{1}{2}M}
	$$

	We can verify that for $m_1,M\to 0 \Rightarrow a=g$ which makes sense and also $0<a<g$. Also $m_1=m_2 \Rightarrow a=0$. Check.

	NB: For the case of a massless pulley you can directly solve with $T_1=T_2$.

	*Via the conservation of energy*:
	
	Of course you can obtain the same result via the total energy:
	
	$$
	E= \frac{1}{2}m_1v_1^2 + \frac{1}{2}m_2v_2^2 + \frac{1}{2}I\omega^2 +m_1g(H-x) + m_2 g (H+x)
	$$
	
	Now we use $v_1 = -v_2$ and rolling without slipping condition $\omega=v/R$ together with $I=\frac{1}{2}MR^2$ which gives the same acceleration as above after differentiation with respect to time.

## Demos & Examples

1. [Flywheels](https://en.wikipedia.org/wiki/Flywheel_energy_storage){ target="_blank" } are large, heavy rotating disk that can story very large energies in the rotation and e.g. release them very quickly for the generation of high currents or magnetic fields for nuclear fusion experiments. Putting some numbers to it. For a wheel of $R=2$ m and mass $M=200\cdot 10^3$ kg, rotation at a speed of 1800 rpm (=30 1/s), the kinetic energy stored is $E=\frac{1}{2}I\omega^2=\frac{1}{4}200\cdot 10^3\cdot 2^2\cdot 30^2= 180$ MJ (or 50 kWh) but can be released in seconds.

1. The complicated physics of a rotating bicycle wheel by [Prof. Walter Lewin from MIT](https://www.youtube.com/watch?v=XPUuF_dECVI&t=2432s){ target="_blank" } starting from 14:30.

1. Practice your thinking about (angular) momentum and energy by [shooting a bullet into a piece of wood](https://www.youtube.com/watch?v=vWVZ6APXM4w){ target="_blank" }.

1. Watch a [gyroscope in space](https://www.youtube.com/watch?v=xQb-N486mA4){ target="_blank" }.

## Exercises

1. You have a solid cylinder of mass $M$ and a thin walled cylinder of the same mass $M$ both rolling down an inclined slope of angle $\alpha$. Which one will be down quicker? Compute their arrival times. If you need help, [look here](https://www.youtube.com/watch?v=cPMSMDSY-rc){ target="_blank" }. For a good instruction video [wacht here](https://www.youtube.com/watch?v=XPUuF_dECVI&t=2432s){ target="_blank" }, starting at 11:20.
