# Pendulum

!!! note "Textbook & Collegerama"
    - Chapter 4: section 4.2
    - Tn1612TU_12

!!! summary "Learning goals"
    - Derive the equation of motion for a pendulum
    - Approximate the motion around the equilibrium 

![pendulum](images/Pendulum.jpg){ align="right" width="250" }

A well-known example of an oscillation is the pendulum. In essence, a (simple) pendulum is a massless, stif rod of length $L$ suspended from a ceiling with at the other end a point mass $m$. The pendulum can move frictionless both in the air and with respect to its pivot point at the ceiling. The equilibrium position is, obviously, when the pendulum is hanging vertically. Assume we give the pendulum a displacement out of its equilibrium position, as sketched in the figure. What kind of motion will the mass exhibit?

First, let's realize that although the motion is in a plane (2D) it is in fact a 1D problem. How to understand this? Well, there is a strong constraint: the mass will always be at a distance $L$ from the pivotal point. Hence, if we know the angle the rod makes with the vertical, we know where the mass is. The motion of the mass only has one degree of freedom. It is not a 3D problem if we let the mass act only on gravity, if we give the motion an initial arbitrary velocity out of the plane then things change, but let's keep things simple.

## Equation of Motion

In order to set up the equation of motion, we look at the gravitational force in the radial and tangential direction.

* In the radial direction $\hat{r}$, the component of gravity $mg \cos \phi$ is balanced by the tension force $F_t$ in the rod. This force is such that there is no motion in the $\hat{r}$ direction: $m\ddot{r} = mg \cos \phi + F_t = 0$.

* In the tangential direction $\hat{\phi}$ the component of gravity is: $-mg \sin \phi$.

    Thus, the equation of motion in the tangential direction reads as: $m \dot{v}_\phi = mL \ddot{\phi} = -mg \sin \phi$.

Thus, N2 for a pendulum is

$$
mL \ddot{\phi} + mg \sin \phi = 0
$$

with initial conditions: at $t=0$ the pendulum will be at some position $\phi_0$ and has some velocity $\dot{\phi}_0$.

Now that this looks similar to a harmonic oscillator, but it is not. The sine is in the way. However, for small $\phi$, we can use the Taylor expansion for the sine function around $x=0$ up to first order only:
$\sin (x) =  x + {\cal{O}}(x^3)$

$$
L\ddot{\phi}+g\sin \phi=0 \text{ :small }\phi\Rightarrow L\ddot{\phi}+g\phi=0
$$

Now this exactly is the [undamped harmonic oscillator](harmonic_oscillator.md#undamped-harmonic-oscillator)! The frequency of oscillation $\omega^2=g/L$ is of course independent of the mass $m$ (if you do not see directly why think about it!). The period is $T=2\pi \sqrt{L/g}$. We could already have known that the pendulum would be described by the harmonic oscillator for small pertubations from the equilibrium because *any* system does this as we [have seen already](https://qiweb.tudelft.nl/mecharela/potential_energy/#taylor-series-expansion-of-the-potential).

### Using the energy equation

We can write the kinetic energy in polar coordinates then $E_{kin} = \frac{1}{2}mv_\phi^2$ with [velocity in polar coordinates](central_forces.md#velocity-and-acceleration) $v_\phi =L\dot{\phi}$. We define a potential $V(\phi )= mgh = mg(L-L\cos\phi)$, with $V(\phi=0)=0$ and $V(\phi=\pi/2)=mgL$ which makes sense.

Substituting this into the energy equation

$$
\frac{1}{2}mv_\phi^2 + V(\phi) = E
$$

we arrive at 

$$
\frac{1}{2}m L^2 \dot{\phi}^2+mgL(1-\cos\phi)=E
$$

Now we take the derivative with respect to time (and remember $\phi=\phi(t)$) and obtain the equation of motion

$$
mL^2\dot{\phi}\ddot{\phi} + mgL \sin\phi\, \dot{\phi} =0
$$

The approach via the energy might not look as easy as via the forces, but in fact that is how you will approach any problem the rest of your physics life after you have followed the second year class on *Classical Mechanics*. The main advantage is that we could directly reduce the problem to 1D. We did not even need to think about the tension force.

## Example

The solution of the equation of motion for the pendulum is not easy for larger angles $\phi$. It is not a simple addition of sine and cosine. Only for a small angles it approaches the harmonic oscillator.

In the widget below, you can see how much the difference is. The widget shows a harmonic oscillator at small angles (the green dot). You can set a harmonic oscillator at an initial angle (the blue dot) and a pendulum at the same angle (red dot).
Note that both harmonic oscillators have the appearance of a pendulum with $\frac{k}{m} = \frac{g}{L}$.

Click image to start the widget. Watch the time traces on the right!

[![pendulum widget](images/PendulumWidget.jpg){ width="500" }](Widgets/PendulumWidget.html){ target="_blank" }

## Jupyter labs

1. Pendulum with a spring 	[Exercise5.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex5.ipynb)
