# Galilean Transformation

!!! note "Textbook & Collegerama"
    - Chapter 1: sections 1.1 and 1.6 - 1.8
    - TN1612TU_10

!!! summary "Learning goals"
    - Know the concept *inertial frame*
    - Apply a Galilean transformation between two frames

In order to describe physical phenomena qualitatively, we need a reference system which assigns a coordinate frame and origin. A basic example: suppose you tell me that you see an object moving at velocity $\vec{v}=1m/s$. I could measure a totally different value for the speed of this object. Your number only makes sense to me if I know what your reference system is compared to mine. In my system, the object could be standing still or moving in the opposite direction. In other words, we use a frame of reference for calculations.

## Inertial frame of reference

Newton's laws hold in their most basic form (as we described them [earlier](newtons_laws.md)) in what is called an *inertial frames of reference*. Loosely speaking, this is a frame that moves at a constant velocity (which may be zero) with respect to some fixed frame; in Newton's time the distant stars served this purpose. Nowadays, *inertial frames* are used as mathematical idealizations only. Newton's laws take the same form in all inertial reference frames, although depending on the case, some are easier to use than others.

To make the above loose statement more precise. We have two coordinates systems CS and CS'. The transformation between both is given by a translation of the origin $\vec{R}$ and a rotation given by the rotation matrix $D$ (in 2D or 3D) 
in the form

$$
\vec{x}' = D\, (\vec{x}-\vec{R})
$$

Now the statement: Newton's equation of motion $\vec{F}=m\vec{a}$ have the same form in CS and CS' can be formulated as the requirement on the transformation $\dot{D} =0$ and $\ddot{\vec{R}}=0$, i.e. the rotation is time independent and the origin offset must not accelerate.

In the case $\dot{D}\neq 0$ or $\ddot{\vec{R}}\neq 0$ Newton's equation of motion obtain additional terms (called *fictitious* or *inertial forces*). We will treat these cases [later](noninertialframes.md).

We assume that a CS without inertial forces exist, and such a system we call *inertial system*. I.e. we can write down Newton's equation in the form of $\vec{F}=m\vec{a}$. If one CS is an inertial system, then CS' is also a inertial system if they are linked by a coordinate transformation $\vec{x}' = D\, (\vec{x}-\vec{R})$ with $\dot{D}=0$ and $\ddot{\vec{R}}=0$. The transformation that fulfils such a change is called the *Galilean Transformation*.

## Galilean Transformation

The transformation between inertial frames may only contain a constant velocity (by $\ddot{\vec{R}}=0$). In addition the frames must not rotate with respect to each other as a function of time ($\dot{D}=0$). As a consequence, velocities, as observed by two different inertial observers, differ by a fixed, constant velocity, whereas accelerations are the same for both observers. This can easily be seen by the basic *Galilean Transformation* between two inertial frames, $S$ and $S'$. According to observer $S$, $S'$ is moving at a constant velocity $V$. Both observers have chosen their coordinate system such that $x$ and $x'$ are parallel (we ignore the possibility of the rotational shift for simplicity in all the discussion and assume $D$ as the identity matrix). Moreover, at $t=t'=0$, the origins $O$ and $O'$ coincide. The picture below illustrates this.

![GalileiTransformation](images/GalileiTransformation.jpg){ align="right" width="300" }

Consider for simplicity a 2D point $P$ with coordinates $(x',y')$ and time $t'$ for  $S'$. What are the coordinates according to $S$? First of all: in classical mechanics, there is only one time, that is: $t=t'$. Until the days of Einstein this seemed self evident; we know that nature is more complex now.

For the spatial coordinates, we see immediately: $y=y'$. And for the $x$-coordinate $S$ can do the following. To go to the $x$-coordinate of $P$, first $S$ goes to the origin $O'$ of $S'$. $O'$ is a distance $Vt$ from $O$. Thus, the distance to $P$ along the $x$-axis is $Vt+x'$. If we sum the above up, we can formulate the relation between the coordinate system of the two observers. This transformation is known as *Galilean Transformation*.

$$
\begin{eqnarray} x' &= &x - Vt \\
              y' &= &y \\
              t' &= &t
              \end{eqnarray}
$$

### Velocity is relative; acceleration is absolute

A direct consequence of the Galilean transformation (GT for short) is that velocity is observer-dependent (not surprising), but for observers in inertial frames, observed velocities differ by a constant velocity vector.

$$
\begin{eqnarray} v'_{x'} &\equiv &\frac{dx'}{dt'} \Rightarrow  v'_{x'} = \frac{d(x-Vt)}{dt} = v_x - V\\
               v'_{y'} &\equiv &\frac{dy'}{dt'} \Rightarrow  v'_{y'} = \frac{dy}{dt} = v_y 
              \end{eqnarray}
$$

$$
\begin{eqnarray} a'_{x'} &\equiv &\frac{dv'_{x'}}{dt'} \Rightarrow  a'_{x'} = \frac{d(v_x-V)}{dt} = a_x\\
               a'_{y'} &\equiv &\frac{dv'_{y'}}{dt'} \Rightarrow  a'_{y'} = \frac{dv_y}{dt} = a_y 
              \end{eqnarray}
$$

Consequently, N2 holds in both inertial systems if we postulate that $m' = m$. In other words: mass is an object property that does not depend on the observer.

Thus, two observers, each with its own inertial frame of reference, will both *see the same forces*: $F = ma = m'a' = F'$.

This finding is stated as: Newton's second law is *invariant* under Galilean Transformation. Invariant means that the form of the equation does not change if you apply the Galilean coordinate transformation. Later we will expand this to [Lorentz invariant](lorentz.md) transformation in the context of special relativity. The concepts of invariance is very important in physics as hereby we can formulate laws that are the same for everybody (loosely speaking).

## Navier-Stokes and Galileo invariance

In hydrodynamics the conservation of mass in a fluid or gas is described in 1D by

$$
\frac{\partial \rho }{\partial t} + \frac{\partial (\rho  v)}{\partial x}=0
$$

with $\rho(x,t)$ the mass density (unit kg/m$^{3}$) and $v(x,t)$ the flow velocity field. This continuity equation was introduced by [Navier](https://en.wikipedia.org/wiki/Claude-Louis_Navier){ target="_blank"} and [Stokes](https://en.wikipedia.org/wiki/Sir_George_Stokes,_1st_Baronet){ target="_blank"} around 1840 and is Galileo invariant. 

You can visualize the statement of the equation and derivation as follows. In a liquid (but also for gasses) the mass that flows into a volume element and out of it is conserved. Through an area $A$ perpendicular to the flow direction the inflow of mass into a small element of length $dx$ is $\rho A vdt|_{x}$ and the out flow is $\rho Avdt|_{x+dx}$. As the mass is conserved we have $M(t+dt)-M(t) = \rho A v|_{x}dt - \rho Av|_{x+dx}dt$. Now we replace $M(t)=\rho Adx$ and can rearrange to find the continuity Navier-Stokes equation.

??? "Continuity Equations"

	You will find these type of "conservation equations" for different kind of phenomena later in your study, they are called *continuity equations*. A certain property, here mass, is continuous and its change in time must be compensated by in or out flow of the same quantity. The general form of the equation is 
	
	$$
	\frac{\partial \rho}{\partial t}+div\ \vec{j} = 0
	$$
	
	Where the divergence is the inner product of the nabla operator with the vector field $\nabla \cdot \vec{j}(\vec{x},t)$ and $\vec{j}$ represents the flux of the quantity. You will see continuity equations again as conservation of charge in Electromagnetism, as heat transfer in Thermodynamics, in semi-conductors (for electron and holes) in Solid State and even in Computer Vision as [Optic Flow](https://en.wikipedia.org/wiki/Optical_flow){ target="_blank"}.


We will now show that the continuity equation is GT invariant. We use the formalism of transformation again later for Maxwell's equations when going to [special theory of relativity](special.md).

For an observer $S'$ with coordinates $x'=x-Vt, t'=t$ the equation needs to be transformed, i.e. the partial derivatives. The density $\rho( x(x',t'), t(x',t'))$ is a function of two variables $x',t'$. The chain rule gives us in general $\frac{\partial f(x(x',t'))}{\partial x} =  \frac{\partial f}{\partial x'}\frac{\partial x'}{\partial x} + \frac{\partial f}{\partial t'}\frac{\partial t'}{\partial t}$ and thus for the space derivative 

$$
\frac{\partial}{\partial x}=
\frac{\partial}{\partial x'}\frac{\partial x'}{\partial x} + 
\frac{\partial}{\partial t'}\frac{\partial t'}{\partial x}
$$

Luckily the GT is simple and the derivatives simply $\frac{\partial x'}{\partial x}=1$ and $\frac{\partial t'}{\partial x}=0$. Therefore we have

$$
\frac{\partial}{\partial x}=\frac{\partial}{\partial x'}
$$

For the time derivate we find

$$
\frac{\partial}{\partial t}=
\frac{\partial}{\partial t'}\frac{\partial t'}{\partial t} + 
\frac{\partial}{\partial x'}\frac{\partial x'}{\partial t}
$$

with $\frac{\partial t'}{\partial t}=1$ and $\frac{\partial x'}{\partial t}=-V$ we have

$$
\frac{\partial}{\partial t}=
\frac{\partial}{\partial t'}-V \frac{\partial}{\partial x'}
$$

Combining the above for the transformed equation in $x',t'$

$$
\frac{\partial \rho }{\partial t'} -V\frac{\partial \rho }{\partial x'} + \frac{\partial (\rho v)}{\partial x'} = 0
$$

If we now define $v'\equiv v-V$, and recall $V=const.$, then the equation reads 

$$
\frac{\partial \rho }{\partial t'} + \frac{\partial (\rho v')}{\partial x'}=0
$$

therefore also an observer in $S'$ with $\rho' (x',t')=\rho (x,t)$ can write the Navier-Stokes equation as

$$
\frac{\partial \rho' }{\partial t'} + \frac{\partial (\rho' v')}{\partial x'}=0
$$

in his own coordinates such that the form of the equation is preserved between $S$ and $S'$.

If we cannot find a $\rho'$ and $v'$ such that the equations have the same form, the Navier-Stokes equations would not have been Galilei invariant (which is bad of course as all relevant physical laws must be invariant under this coordinate transformation).

## Worked Example

In class you have seen the *Superballs* example. You can watch it [here](https://www.youtube.com/watch?v=2UHS883_P60
){ target="_blank"} again. The explanation is not really correct there, we will reason the observations here.

If you let a <span style="color:green">smaller</span> and a <span style="color:blue">larger</span> ball drop together, stacked on top of each other, the smaller ball will bounce back much stronger (higher) than if you let the small ball fall without stacking it on the lager ball. How can that happen?

To explain this we use the Galilei transformation (GT).

* Situation 1). Both balls are falling with velocity $\vec{v}$ towards the ground.
* Situation 2a). The larger ball just hit the ground. As the mass of the ground is much larger than the large ball, it is (elastically) reflected, i.e. the direction of the velocity is reversed but the magnitude stays the same (no momentum is transferred to the ground). The small ball is still moving downwards with $\vec{v}$.
* Situation 2b). We apply a GT of the observer (yellow star) from the ground to an observer with the larger ball. The observer with the larger ball is at rest for himself, therefore the smaller ball will now move with $2\vec{v}$ towards it.
* Situation 3a). The smaller ball hits the larger ball and is reflected due to its smaller mass. In the frame of the observer on the larger ball, the smaller ball now moves with $2\vec{v}$ away from it.
* Situation 3b).  We apply a GT of the observer (yellow star) from the larger ball  back to an observer on the ground. For the observer on the ground the larger ball has velocity $\vec{v}$ upwards from 2a), therefore the smaller ball has velocity $3\vec{v}$ upwards.

![Superballs](images/superballs.png){ width="500"}

The smaller ball has now velocity $3\vec{v}$ instead of $\vec{v}$ if you drop it without the larger ball. NB: If you would use three balls instead of two, the third ball would have a velocity of $7\vec{v}$ using the same reasoning as above. How much higher does the smaller ball fly with velocity $3\vec{v}$ compared to $\vec{v}$?

??? "Answer"
	
	We equate the kinetic energy when the ball is just reflected with the potential energy when the ball reached it maximal height before falling back.
	
	$$
	\frac{1}{2}mv^2 = mgh \Rightarrow h = \frac{v^2}{2g}
	$$
	
	Therefore the ball with $3v$ flies 9 times higher.

??? "What is very fishy about this whole outcome?"

	In situation 1) the kinetic energy is $\frac{1}{2}m_s v^2 + \frac{1}{2}m_\ell v^2$, but in situation 3b) it is $\frac{1}{2}m_s (3v)^2 + \frac{1}{2}m_\ell v^2$ while the potential energy is zero in both cases. This clearly does not add up! But energy must be conserved under all circumstances!
	
	The conclusion is, that we always did make an approximation and did not solve the energy and impulse conservation equations for elastic collisions. Even for the case $M\gg m$ there is some momentum transfer. If you solve for the velocity of $m$ after the collision with $M$, you obtain 
	
	$$
	v' = \frac{\frac{m}{M}-1}{\frac{m}{M}+1} v
	$$
	
	For $M\gg m$ you indeed see $v'=-v$. Thus the smaller ball will have a smaller velocity than reasoned above *and* the larger ball with also have a smaller velocity (in the experiment you can clearly notice that it does not fly as high as when it drops without the small ball on top). In real life, the balls also deform which makes the collision inelastic.

## Example

1. 	Consider yourself biking at a constant velocity on an unlikely day with zero wind. Still, you experience a frictional force from the air, with the following observation: the faster you bike, the larger this force. An experimentalist is trying to measure the friction force of the air and relate it to your velocity. She finds that, by and large, these forces turn out to scale with the square of your velocity $v_b$
	
	$$
	F_f \propto v_b^2
	$$
	
	![Fietser](images/Fietser.jpg){ align="right" width="250" }
	
	Understanding the Galilean transformation, you immediately see that this can't be correct. In your frame of reference, your velocity is zero. And thus, the friction force would be zero. But that cannot be true: both observers should see the same forces. What you see is that the air is blowing at a speed $v_{air}−v_b$ past you. And indeed, the faster you bike, according to the experimentalist, the faster you see the air moving past you: velocity is relative.
	
	You quickly realize that a proper description of the air friction must depend on the relative velocity between you and the air. *Relative* velocities are invariant under Galilean transformation:
	
	$$
	F_f \propto (v_b - v_{air} )^2
	$$

1. Riding a bike while it rains. You have done this 100s of times. Your front gets soaked, while the backside of your coat stays dry. Or if you have a passenger on your carrier he/she will not get wet, while you take all the water. From a GT to the reference frame of the biker it is obvious  why this is the case. The rain is not falling straight from the sky, but at an angle towards him. 

![Riding a bike in the rain](images/RainBike.png){ width="400"}

NB: For Dutch bikers you have had this experiences with head wind and rain all your life.

## Demo

Bouncing against a wall: what would Galilei say? Click image to start the widget.

[![bouncing ball](images/BouncingBall.jpg){ width="500" }](Widgets/BouncingBall.html){ target="_blank" }

## Exercise

1. A train is passing a station at a constant velocity $V$. At the platform that the train passes, an observer $S$ sees that in the middle of the train (train length $2L$), at $t=0$ an object is released with a constant velocity $u$. The object moves towards the back of the train and, at some point in time, will hit the back.

	Inside the train, observer $S'$ sees the same phenomenon. Show that both find the same time for the object hitting the back of the train.

1. Show that the equation of motion $\dot{\vec{p}}=\vec{F}$ is invariant under rotation of the coordinate system, i.e. the the orientation of the axes in the inertial frame do not matter.
