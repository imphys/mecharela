# Beyond Newton's Mechanics

!!! note "Textbook & Collegerama"
    - Chapter 15 on General relativity
    - TN1612TU_31
    
!!! summary "Learning goals"
	- Remain curious & have fun with physics

Newton is dead already a long time (300 years), has nothing new happened in mechanics since then? The good news, no and yes. No, Newton's three fundamental laws stand, therefore the physics remains the same. Yes, Newton's law of gravity is replaced by Einstein's theory of general relativity ([see below](beyondNewton.md#general-relativity)). Yes, the way how to *think* and *solve* a problem has been greatly improved by [Lagrange](https://en.wikipedia.org/wiki/Lagrangian_mechanics){ target="_blank"} and [Hamilton](https://en.wikipedia.org/wiki/Hamiltonian_mechanics){ target="_blank"}. Their way of formulating mechanics, is now the standard way in physics. You will learn their approach in the second year class *Classical Mechanics* and use it later in all other courses.  

## Inertia tensor

When we introduced the [moment of inertia](rigid_body.md#moment-of-inertia) that was always with respect to a given axis. That is not needed if you compute the moment of inertia as a second order tensor (matrix) $i,j=1,2,3$

$$
I_{ij} \equiv \int_M  (r^2\delta_{ij}-r_i r_j)\, dm
$$

which allows also to write the kinetic energy in a more general form without [the requirement](rigid_body.md#kinetic-energy-of-rotation) $\vec{\omega} \perp \vec{r}$. 

$$
E_{rot} = \frac{1}{2} \vec{\omega} I \vec{\omega}
$$

The product $\vec{\omega} I \vec{\omega}$ projects the inertia tensor onto the rotation axis, we automatically get the rotation energy for the given axis - nice.

As the inertia tensor can be diagonalized, we see that there are 3 principal axis of rotations.

Dealing with rotations is mathematically difficult, therefore we only treated simpler cases here.


## Calculus of variations

Up to now our strategy was to obtain the equation of motion via  N2 and then solved for $x(t)$. Later you will do this by something that seems very complicated at first. You will try to find the function or path $x(t)$ that minimizes the integral (actually to make it [*stationary*](https://en.wikipedia.org/wiki/Stationary_point){ target="_blank"})

$$
S = \int_{t_1}^{t_2} f(x(t),\dot{x}(t),t)\, dt
$$

where $x(t)$ is the unknown path between $(t_1,x_1)$ and $(t_2,x_2)$ such that $x(t_1)=x_1$ and $x(t_2)=x_2$. The form of the function $f(x,\dot{x},t)$ depends on the problem at hand, but that turns out not to be hard.

Now the cool part. You can find the path $x(t)$ by the Euler-Lagrange equations. 


### Euler-Lagrange equations

The path $x(t)$ for which this integral is stationary is given by the *Euler-Lagrange* equation (omitting the proof)

$$
\frac{\partial f}{\partial x}-\frac{d}{dt}\frac{\partial f}{\partial \dot{x}}=0
$$

NB: this is easily extended to $n$ dependent variables. Here our independent variable is time $t$ and dependent is the position $x(t)$ and the velocity $\dot{x}(t)$.

??? "Shortest time on a roller coaster?"

	You need to design the shape of a frictionless children slide such that the kids slide the quickest from point $x_1$ to $x_2$ purely under the influence of gravity, with height $y_1 > y_2$. This classical problem of calculus of variations is called [*Brachistochrone*](https://en.wikipedia.org/wiki/Brachistochrone_curve){ target="_blank"}.
	
	The travel time $t_{1\to 2}$ from $x_1$ to $x_2$ is
	
	$$
	t_{1\to 2} =\int_{x_1}^{x_2} \frac{1}{v}\, ds\quad (*)
	$$
	
	where the velocity is $v=\sqrt{2gy}$ at height $y$ at any time by conservation of energy ([you know this](work.md#exercises)). We parameterize the unknown path as $x(y)$ and take $y$ as our independent variable. The arc length interval $ds$ is given by $ds^2 = dx^2+dy^2 = \left [\left (\frac{dx(y)}{dy}\right )^2 +1\right ] dy^2$.
	
	Filling these findings into equation $(*)$ we have
	
	$$
	t_{1\to 2} = \frac{1}{\sqrt{2g}} \int_{0}^{y_2} dy\, \frac{\sqrt{x'(y)^2+1}}{\sqrt{y}}
	$$
	
	We need to find the minimum of this integral by finding the right path $x(y)$ with the function $f(x,x',y)=\sqrt{\frac{x'(y)^2+1}{y}}$, which we subject to the Euler-Lagrange equation (with the correct variables)
	
	$$
	\frac{\partial f}{\partial  x} - \frac{d}{dy} \frac{\partial f}{\partial x'}=0 
	$$
	
	The derivative $\frac{\partial f}{\partial  x}=0$ and therefore $\frac{\partial f}{\partial x'}=C=const$. We arrive at a differential equation for $x(y)$ which we can solve (not done here)
	
	$$
	\frac{dx}{dy} = \sqrt{\frac{y}{1/C-y}}
	$$
	
	Note: we have been very smooth with the variables, e.g. $t\to y$ compared to the introduction of the Euler-Lagrange equation. That this is possible, is a big strength.
	
	Note: You can solve the problem differently, already Newton or Bernoulli did that, but that was hard work on their side.

## Generalized Coordinates

If we wanted to describe the motion of a particle on the surface of a sphere with radius $R$ it is a 2D problem as it only moves on the surface, but how to treat it? Instead of writing down the problem in $x,y,z$ coordinates which are dependent on each other by the constraint $x^2+y^2+z^2=R^2$ we better find independent coordinates! In this case, you could use spherical coordinates $q_1=\theta, q_2=\phi$, but how to do this for a complicated constraint?

You will learn to replace the $3n$ space coordinates for $n$ particles by $f=3n-\Lambda$ *generalized coordinates*, where $\Lambda$ is the number of constraints the system has $\{\vec{r}_1,\vec{r}_2,\dots \vec{r}_n \}\to \{ q_1, q_2,\dots q_f\}$. These generalized coordinates do no even need to have the unit of meters! We have done this sort of constraint business a few times implicitly for simple cases already, for example, for the equation of motion of the [pendulum](pendulum.md).


## Lagrange formalism

With the generalized coordinates and the calculus of variations you are ready for the real deal. Lagrange's way to tackle a problem is easier than Newton's in the sense that it allows to eliminate constraints (imposed by force or geometry) at no cost. For example, the [tension force](noninertialframes.md#foucaults-pendulum) to keep the pendulum at a certain distance on a rod is not needed here. In addition Lagrange equations take the same form in any CS.

[Lagrange](https://en.wikipedia.org/wiki/Joseph-Louis_Lagrange){ target="_blank"} defined the quantity 

$$
{\cal{L}}(x,\dot{x},t) \equiv \frac{1}{2}m\dot{x}^2-V(\vec{x})
$$

as the difference of kinetic and potential energy, which is nowadays called the *Lagrangian* (often you see ${\cal{L}} \equiv T-U$ with $T\equiv E_{kin}$). This quantity has the following properties

$$
\frac{\partial {\cal{L}}}{\partial x} = - \frac{\partial V}{\partial  x} = F_x,\quad \frac{\partial {\cal{L}}}{\partial \dot{x}} = m\dot{x} = p_x
$$

Using N2 in the form $F=\dot{p}$ we can combine these two equations (in 1D) to

$$
\frac{\partial {\cal{L}}}{\partial x} = \frac{d}{dt}\frac{\partial {\cal{L}}}{\partial \dot{x}}
$$

which exactly corresponds to the Euler-Lagrange equations above. In 3D for $x,y,z$ you just get 3 equations, not 1.

We can conclude that there are 3 equivalent ways to tackle a problem to find the path $x(t)$

1. Solving the equation of motion given by N2
2. The path is determined by the Lagrange equations
3. The *action integral* $S=\int {\cal{L}}\, dt$ is stationary 

### Including constraints

It turns out that we can replace the coordinate $x_1,y_1,z_1,x_2,\dots$ with the generalized coordinates $q_i$ and the Euler-Lagrange equations remain of the same form for $i=1,\dots f$. With the Lagrangian ${\cal{L}}(q,\dot{q},t)$

$$
\frac{\partial {\cal{L}}}{\partial q_i} = \frac{d}{dt}\frac{\partial {\cal{L}}}{\partial \dot{q_i}}
$$

For example in the case of the [simple pendulum](pendulum.md#using-the-energy-equation) we had for the kinetic energy in polar form $T=\frac{1}{2}mL\dot{\phi}^2$ and for the potential $U=mgL(1-\cos\phi)$ leading to the Lagrangian ${\cal{L}}=\frac{1}{2}mL\dot{\phi}^2-mgL(1-\cos\phi)$. This only has 1 degree of freedom and therefore here we have $q_1=\phi$

$$
\frac{\partial {\cal{L}}}{\partial \phi} = \frac{d}{dt}\frac{\partial {\cal{L}}}{\partial \dot{\phi}}
$$

Evaluating the derivate, leads directly to the known equation of motion. This approach apparently requires very little thinking as we did not worry about the tension force that keeps the pendulum at constant distance $L$ from the ceiling. Working with energies rules! Once we have written down the Lagrangian, our work is done. And it is easier than writing down the relevant forces for N2.

## Hamilton formalism

The Lagrangian does not have a nice physical meaning and is not unique, i.e. we can apply a gauge transformation (as for electric potentials) and obtain the same solution for the path.

[Hamilton](https://en.wikipedia.org/wiki/William_Rowan_Hamilton){ target+"_blank"} defined the quantity

$$
{\cal{H}} \equiv p \dot{x} - {\cal{L}}
$$

This is the *sum* of kinetic and potential energy $T+U$, which is nowadays called the *Hamiltonian*. This allows for much easier interpretation of the quantity ${\cal{H}}$ than ${\cal{L}}$ as the total energy. In addition, it is conserved as long as the Lagrangian does not explicitly depend on time.

??? "Energy is conserved if the Lagrangian does not explicitly depend on time."

	The Lagrangian is a function of ${\cal{L}}(q,\dot{q},t)$ taking the total time derivative, assuming that it does not explicitly depend on time ($\frac{\partial {\cal{L}}}{\partial t}=0$), using the chain rule give us
	
	$$
	\begin{array}{rcl}
		\displaystyle{\frac{d}{dt}{\cal{L}}} &=& \displaystyle{\sum_i \dot{q}_i  \frac{\partial {\cal{L}}}{\partial q_i}  + \ddot{q}_i} \frac{\partial {\cal{L}}}{\partial \dot{q}_i} \\
	 &=& \displaystyle{\sum_i \dot{q}_i \frac{d}{dt} \frac{\partial {\cal{L}}}{\partial \dot{q}_i}  +  \ddot{q}_i \frac{\partial {\cal{L}}}{\partial \dot{q}_i}}\\
	&=& \displaystyle{\sum_i \frac{d}{dt} \left ( \dot{q}_i \frac{\partial {\cal{L}}}{\partial \dot{q}_i} \right ) }
	\end{array}
	$$
	
	In the second step we replaced the derivative with respect to $q_i$ using the Lagrange equation and the in the third step we used the product rule.
	
	From this we find
	
	$$
	\frac{d}{dt} \left (\sum_i \dot{q}_i \frac{\partial {\cal{L}}}{\partial \dot{q}_i}-{\cal{L}} \right )=0
	$$

	The term in the bracket is the total energy or the Hamiltonian of the system and is conserved.

He formulated the problem not as a function of the (generalized) coordinates and velocities 

$$
(q_1, \dots, q_f, \dot{q}_1, \dots, \dot{q}_f) \quad - \quad state space
$$

but in terms of coordinates and momentum 

$$
(q_1, \dots, q_f, {p}_1, \dots, {p}_f) \quad - \quad phase space
$$

This ([Legendre](https://en.wikipedia.org/wiki/Legendre_transformation){ target="_blank"}) coordinate transformation ${\cal{H}} (q,\dot{q}, t)\to {\cal{H}}(q, p, t)$ is typically allowed.

The equation of motion for the Hamiltonian are found by evaluating the partial derivatives of ${\cal{H}} (q, p, t)$ with respect to $q_i$ and $p_i$

$$
\frac{\partial {\cal{H}}}{\partial q_i} = -\dot{p_i}, \quad 
\frac{\partial {\cal{H}}}{\partial p_i} = \dot{q_i}
$$

Here we have 2 first-order differential equations describing the equation of motion, while in the Lagrange formalism we had 1 second-order equation. While this is no improvement in itself, the specific form of the Hamilton equations makes them easier to solve (as it turns out).


## Noether's theorem

[Emmy Noether](https://en.wikipedia.org/wiki/Noether%27s_theorem){ target="_blank"} made the observation that if the Hamiltonian ${\cal{H}}(q,p,t)$ does not explicitly depend on a coordinate $q_i$, then $\frac{\partial {\cal{H}}}{\partial q_i} = 0$ which directly implies that $\dot{p}_i = 0$ or $p_i=const.$

In other words if the problem does not depend on a certain coordinate $q_i$, the corresponding momentum $p_i$ is conserved. Here symmetry of the problem implies existence of a conserved quantity. 

### Examples

1. In this abstract example you see how symmetry simplifies the thinking. Consider a Hamiltonian ${\cal{H}}(q_1,p_1,p_2)$, it is independent of $q_2$, thus we directly know $p_2=C=const.$ and the Hamiltonian is ${\cal{H}}(q_1,p_1,C)$ which reduces the problem to a 1D problem. One symmetry directly reduces the degrees of freedom by one.

2. Think of a mass $m$ falling in the gravitational field along the $z$-axis. We have ${\cal{H}}(z,p_z)= \frac{p_z^2}{2m}+mgz$. As the Hamiltonian does not depend on $x,y$ the momenta $p_x,p_y$ are conserved. Notice, we did not write the kinetic energy in the $\frac{1}{2}mv^2$ form we used, but $\frac{p^2}{2m}$ as we need an explicit form depending on the momentum $p$ for the Hamiltonian.

3. Mass $m$ in a central conservative force field. We know that the [angular momentum is conserved](central.md), we can find this in a different way now. The kinetic energy in [polar coordinates](central_forces/#velocity-and-acceleration) $(r,\phi)$ is $T=\frac{1}{2}m(\dot{r}^2+r^2\dot{\phi}^2)$. From this we have for the momenta $p_r = \frac{\partial T}{\partial \dot{r}} = m\dot{r}$ and $p_\phi = \frac{\partial T}{\partial \dot{\phi}} = mr^2\dot{\phi}$, thus for the Hamiltonian we find
	
	$$
	{\cal{H}}(r,p_r,\phi,p_\phi) = \frac{1}{2m}(p_r^2+\frac{1}{r^2}p_\phi^2)+V(r)
	$$
	
	This Hamiltonian does not depend on $\phi \Rightarrow \frac{\partial {\cal{H}}}{\partial \phi} = 0 \Rightarrow p_\phi=const.$ Angular momentum is conserved. Check.

	We can generalize the conservation of angular momentum now. It is apparently only required that the Hamiltonian does not explicitly depend on $\phi$ to conserve angular momentum, no need to have a central force. Independence of the angle $\phi$ means that the space is *isotropic*, i.e. the mechanical properties of a closed system do no vary when it is rotated as a whole. 

---

Now you wonder can we not apply some other transformation to the coordinates $(q_i,p_i)$ such that we obtain more symmetries and in turn reduce the dimensionality. That is the case sometimes: *canonical transformations*.

## Liouville's theorem

[Liouville](https://en.wikipedia.org/wiki/Joseph_Liouville){ target="_blank"} found a conservation law that has applications even outside classical mechanics also in statistical, quantum and fluid mechanics.

We place ourselves in *phase space* $(q_1, \dots, q_f, \dot{p}_1, \dots, \dot{p}_f)$. There each point corresponds to one time point of the system (given by the Hamiltonian) for a given initial condition. As time evolves a system describes a path through this phase space. Liouville found an equation describing the time evolution through phase space, not for one specific initial condition of a problem at hand, but for the density of a set of different initial conditions. This density is conserved in phase space. You can also say that the volume that close-by paths enclose is conserved ("conservation of phase space volume").

In quantum mechanics this time evolution of the density (matrix) in phase space is known as the [von Neumann equation](https://en.wikipedia.org/wiki/Density_matrix#The_Von_Neumann_equation_for_time_evolution){ target="_blank"}, where it allows to describe mixed states opposed to pure states only as in the Schr&ouml;dinger equation.

### Examples

1. *Etendue*: Is a conserved property in geometrical optics. It measures the spatial extend of a beam of light times its solid angle. A lens can focus a light beam (reduce its spatial extend), but that is only possible at the cost of increased transverse momentum (beam divergence). The phase space volume is conserved, i.e. the product of spatial extend and momentum.
2. For an ideal gas the quantity $VT^{3/2}$ is conserved  (under adiabatic compression/expansion). The temperature depends on the momentum as $T \propto p^2 \Rightarrow p \propto T^{1/2}$. The volume $V$ times $T^{3/2}$ is therefore conserved as the ideal gas has 3 degrees of freedom. 

You will use the book *Classical Mechanics* by John Taylor. This is a very nice book and light on the formalism. You could use it already for this course, except for a few times where we lack the math, or in case of rigid body motion.

## General relativity

Special relativity is based on the conceptual assumption that the speed of light is constant for all inertial observers where the [Michelson-Morely experiment](special.md#the-michelson-morley-experiment) found $c_\perp = c_{||}$.

For the mass we have seen that [experimentally](forces.md#inertia) inertial $m_I$ and heavy/gravitational mass $m_g$ are the same. Again Einstein conceptualized this $m_g\equiv m_I$ as discussed before with [fictitious forces](noninertialframes.md#gravity-as-a-fictitious-force). The equivalence principle eventually leads to the *general theory of relativity*. Einstein published his final findings on it in 1915. His theory replaces Newtons theory of gravity and is currently still the accepted theory.

### Photons feel gravity

Although photons have [no mass](4impuls.md#photons), they are affected by gravity. This is a consequence of the equivalence principle, i.e. there is no difference between an accelerated frame of reference and a frame at rest with a constant gravitational field. 

![Light in Elevator](images/lightelevator.png){ align="right" width="300"}

If you consider a light source in an elevator that is accelerated with $a$, then it feels normal that the light bends downwards over a distance $L$ as  $\Delta s = \frac{1}{2}a\Delta t^2 \approx \frac{1}{2}a\left ( \frac{L}{c}\right )^2$. But as this is the same for an observer at rest with a constant gravitational field with $g=a$, we note that gravity acts on massless particles! If you fill in numbers, you see that the effect is (very) small as long as $L$ is not very large $(L/\Delta t \sim c)$.

### Mass bends space

Clearly, we need a different interpretation of gravity than attraction based on mass. Einstein provides this by postulating that mass deforms space time itself. This is typically visualized by a 2D surface deformed by a pebble as we cannot visualize a 3D surface. Our 3D space is not a uniformly flat 3D surface but locally curved under the influence of mass/energy (recall that they are the same).

![Space time](images/pebble.jpg){ align="right" width="150"}

Light travels over this deformed surface via the shortest route (a so-called [geodesic](https://en.wikipedia.org/wiki/Geodesic){ target="_blank"}). From this interpretation, it is clear that general relativity is a "geometric" theory. The mathematical toolset of [differential geometry](){ target="_blank"} is needed to do write down and solve equations of motions on 3D non-Euclidean surfaces. Differential geometry was introduced by Gauss and Riemann in the 19th century, later Poincare, Weyl, Cartan, Lie and other contributed. In the curriculum of *Technische Natuurkunde* this topic is not covered in the mathematics courses, making the treatment of general relativity impossible (and thus no such course is offered). For the double degree students *Differential Geometry* is an elective in the third year.

In 1919 the British astronomer [Eddington](https://en.wikipedia.org/wiki/Eddington_experiment){ target="_blank"} together with a team of the Royal Society measured the deflection of light emitted by stars behind the sun during a total solar eclipse. Indeed they found the predicted effect by Einstein. This is seen as the earliest experimental evidence in favor for his new theory of gravity. Actually, the success of the experiment was world news and Einstein instantly became the most important youtuber of his time.

![Gravitational lensing](images/GravitationalLensingHubble.jpg){ align="left" width="200"}

Nowadays this effect, called [*gravitational lensing*](https://en.wikipedia.org/wiki/Gravitational_lens){ target="_blank"}, is observed on much larger scales. The light of a far-away galaxy is lensed by another closer-by galaxy and observed by us. If a far-away star/galaxy is directly behind one in our line of sight, we will observe the far-away "ring"

![Gravitational lensing](images/GravitationalLensing2.png){ align="right" width="200"}

Other early experimental evidence supporting general relativity include the gravitational red-shift ([see below](beyondNewton.md#gravitational-redshift)) and the perihelion precession of Mercury (the closest point of Mercury's orbit is not stable but rotates around the sun).

### Mass slows time

If we consider a light source emitting on the bottom of our elevator (with height $H$) in the same direction as it is accelerated with acceleration $a$, then a light pulse emitted at $t=0$ with frequency $f_0$ will arrive on the ceiling at $T=H/c$. At that time the elevator has the velocity $v=aT=a\frac{H}{c}$. If we substitute this into the relativistic [Doppler formula](doppler.md#doppler-effect) we find a frequency shift of the emitted light at the ceiling of the elevator $f_{ceil} \approx \left ( 1-a\frac{H}{c^2} \right ) f_0$. Of course that must also apply in a gravitational field based on the equivalence principle. 

$$
\frac{\Delta \lambda}{\lambda_0} \sim \frac{v}{c} = \frac{gH}{c^2}
$$

Here we can identify $gH$ as the gravitation potential. The frequency shift is larger for stronger gravitational fields. 

If we use this elevator setup as a clock (we did a similar thing [before](timedilation.md#time-dilation)) with $T=1/f$ we find that gravity slows time. Time passes more slowly the closer you are to the center of gravity (the stronger the field). 

This effect needs to be accounted for in the [GPS system](https://en.wikipedia.org/wiki/Global_Positioning_System){ target="_blank"} used by many of us everyday. This effect is next to the special relativistic effect due to the velocity of the satellites. As the satellites are farer away from the earth (about 20,000 km) than your device and your position is calculated by time difference triangulation from a few satellites, precise timing is of the essence. Although a small effect initially, time difference accumulated and within minutes the system would be off by more than 100 m, making it useless in practice.

#### Gravitational redshift

We consider light moving away from a black hole. If a photon starts with energy $E=hf_0$ and it is moving away, it gains potential energy in the gravitational field of the black hole, thus it must loose kinetic energy. As the speed of light is constant, that is only possible by decreasing the frequency $f<f_0$. Light when moving away from a strong gravitational field is red shifted, e.g. for a small black hole this shifts visible light close to the event horizon to radio frequency far away.

### Black holes

A *black hole* is an object with such a high concentration of mass that nothing, not even light can escape (thus the wording). In terms of space-time, a black hole is a singularity in the field equations, a region of infinite curvature. The boundary around the black hole where escape is just possible is called *event horizon*. If the black hole does not rotate, this coincidences with the *Schwarzschild radius* (unbelievable but true: the naming is after a person, not the property, [Karl Schwarzschild](https://en.wikipedia.org/wiki/Karl_Schwarzschild){ target="_blank"} who first solved Einstein's field equations). 

Classically we can find the Schwarzschild radius by equating the escape velocity to $c$. Equating the kinetic energy and the potential energy in a gravitational field

$$
\frac{1}{2}mv^2 = G \frac{mM}{r}
$$

and setting $v=c$ we find $r_c=\frac{2GM}{c^2}$. For example for the sun, this would correspond to 3 km where the current radius of the sun is about 700,000 km. You see that the mass density must be very high indeed.

A black hole forms (mostly) by collapse of a massive star at the end of its lifetime. Once the internal pressure is smaller than its own gravity, it collapses. Typically a star generates internal pressure by temperature and radiation from nuclear fusion of hydrogen to helium. Once all hydrogen is burned, helium can be fused into heaver elements to produce energy. This burning runs through the periodic table until iron, which is the last nucleus that can produce energy from fusion. Formation of all heavier nuclei consume energy in fusion. A the end of this process, depending on the mass of the star, a (super)nova explosion can happen if the core of the star suddenly collapses into a [white dwarf](https://en.wikipedia.org/wiki/White_dwarf){ target="_blank"}, [neutron star](https://en.wikipedia.org/wiki/Neutron_star){ target="_blank"} or black hole. All elements heavier than iron have been produced once in such an explosion.

#### Hawking radiation

Can *nothing* escape a black hole, *nothing*? (Toute la Gaule est occupée par les romains... Toute? [Non!](https://fr.wikipedia.org/wiki/Ast%C3%A9rix){ target="_blank"})

Indeed black holes are not totally black. That follows from conservation of information/entropy. The process is a quantum and statistical mechanics effect, named after [Steven Hawking](https://en.wikipedia.org/wiki/Stephen_Hawking){ target="_blank"}, although others contributed significantly. It has nothing to do with general relativity but it is based on the finding that black holes have entropy and therefore temperature. In quantum mechanics it turns out that an accelerated observer will observe a thermal bath (temperature), even if the inertial observer does not ([Unruh effect](https://en.wikipedia.org/wiki/Unruh_effect){ target="_blank"}). Due to the temperature a black hole radiates just like a black body.

Hawking often used an explanation via vacuum fluctuations and virtual particle anti-particle pairs which is not 100% right, but intuitive. Vacuum is not nothing, by Heisenberg's uncertainty principle the energy conservation can be violated for a short time, $\Delta E \Delta t \sim h$. If the uncertainty in the energy is large, by creation of a particle anti-particle pair, e.g. electron and positron, this is OK, as long as the time this happens is very short. If this happens close to the event horizon, one partner can cross the event horizon in $\Delta t$, falling into the black hole, while the other goes away. The partner that escapes is created "out of nothing", that is only possible if the black hole losses energy - it radiates.

### Worm holes

Worm holes are speculative tunnels or bridges in space-time which are actually solutions of Einstein's field equations. Do they exist? Up to now the only evidence is in science fiction.

### Einstein's field equations

The exact math is complicated, here it comes.

$$
R_{\mu \nu}-\frac{1}{2}R g_{\mu\nu} = \frac{8\pi G}{c^4} T_{\mu \nu}
$$

On the right-hand side we have $T_{\mu\nu}$ the energy-momentum tensor ($4\times 4$ symmetric matrix with certain transformation properties, $\mu,\nu=0,1,2,3$ with unit N/m$^2$) describing the density of energy/mass and momentum in space time (including electro magnetic energy). This is the given quantity, like the forces and mass for N2. The metric tensor $g_{\mu \nu}$ describes how space-time is locally curved, and $R$ and $R_{\mu \nu}$ are functions it. They describe how the metric changes as a function of position in a complicated way essentially. The constant on the right hand side $\frac{8\pi G}{c^4}\sim 10^{-43} \frac{1}{\text{N}}$ is very small and you see that you need very large energies to bend space time. In the case $T_{\mu \nu}=0$, the absence of matter, space time is flat. The number of independent equations is 4 but they are non-linear.

The way of operation is to solve the field equations for the metric $g_{\mu\nu}$ given the energy distribution. Once you have the metric, you can start solving the equation of motion. In other words, the energy distribution (on the right) dictates how space-time is curved (on the left).

For a free falling particle (no forces act on it) we have for the equation of motion (world line)

$$
\frac{d^2 x^\mu}{d\tau^2}+ \Gamma^\mu_{\alpha\beta} \frac{dx^\alpha}{d\tau}\frac{dx^\beta}{d\tau}=0
$$

with $\tau$ the proper time and summation over $\alpha,\beta=0,1,2,3$ (implicit summation over equal indices and omitting the summation sign $\sum_{\alpha,\beta=0}^3$ is called *Einstein convention*) and $\Gamma^\mu_{\alpha\beta}$ the [Christoffel symbols](https://en.wikipedia.org/wiki/Christoffel_symbols){ target="_blank"}, which tell us how the metric changes as a function of space time in a complicated way.

??? "The metric tensor"

	Why is differential geometry so involved? Because it does not have an embedding space like $\mathbb{R}^4$ but only uses local coordinates on the surface. The metric tensor tells us how to measure distances (locally) on a surface. If we choose a parameterization of the surface, the metric tells us how the different coordinates relate. 
	
	In general the inner product defines distances and angles between two $N$-dimensional vectors $\vec{a},\vec{b}$ and it is given with the metric by
	
	$$
	\vec{a}\cdot \vec{b} \equiv g_{ij}a^i b^j
	$$	
	
	where we sum over the indices $i,j=1,\dots ,N$. The metric $g_{ij}$ is of size $N\times N$, but symmetric. In Euclidean space with parameterization $\vec{x}=(x,y,z)$ the inner product between two vectors is given by the metric
	
	$$
	g_{ij} = \left ( 
	\begin{array}{ccc}
	1 & 0 & 0\\
	0 & 1 & 0\\
	0 & 0 & 1
	\end{array}
	\right )
	$$
	
	Resulting in the well-known $\vec{a}\cdot\vec{b} = a_1 b_1 + a_2 b_2 + a_3 b_3$. For the distance element $ds^2$ we have
	
	$$
	ds^2 = dx^2+dy^2+dz^2
	$$

	The quantity $ds^2$ is invariant under coordinate transformation (we have used that [before](4vector.md#inner-product-conventions) in special relativity).
	
	For special relativity, with the Minkowski metric, we have 
			
	$$
	g_{\mu\nu} = \left ( 
	\begin{array}{cccc}
	1 & 0 & 0 & 0\\
	0 & -1 & 0 & 0\\
	0 & 0 & -1 & 0\\
	0 & 0 & 0 &-1 
	\end{array}
	\right )
	$$
	
	and the distance element is 
	
	$$
	ds^2 = c^2dt^2 - dx^2-dy^2-dz^2
	$$
	
	These are called (pseudo)-Euclidean metrics as the values on the diagonal are $\pm 1$ and all other elements are zero. A Euclidean metric is said to be *flat* (i.e. angles in a triangle add up to 180 degrees).
	
	If you want to do geometry on a 2D surface on a sphere parameterized in spherical coordinates $\vec{x}(r,\phi,\theta) = (r \cos\phi \sin\theta, r \sin\phi \sin\theta, r \cos\theta)$ the metric tensor is 
	
	$$
	g_{ij} = \left ( 
	\begin{array}{ccc}
	1 & 0 & 0 \\
	0 & r^2 \sin\theta & 0 \\
	0 & 0 & r^2 
	\end{array}
	\right )
	$$
	
	and the distance element
	
	$$
	ds^2 = dr^2 + r^2\sin^2\theta \,d\phi^2 + r^2 d\theta^2
	$$
	
	This metric is not flat and angles in a triangle do not add up to 180 deg. The metric tensor does not depend on the coordinate system only on space, if you choose a coordinate system, you obtain the components of the metric.
	
	You can compute the metric from the parameterization by taking the partial derivative with respect to all parameters and then compute the different inner products.
	
	$$
	g_{ij} = \partial_i \vec{x} \cdot \partial_j \vec{x}
	$$
	
	For example for a circle parameterized by $r,\phi:\ \vec{x}(r,\phi)=(r \cos\phi, r\sin\phi)$ we compute
	
	$$
	\begin{array}{rcl}
	\partial_r \vec{x}(r,\phi) &=& (\cos\phi, \sin\phi) \\
	\partial_\phi \vec{x}(r,\phi) &=& (-r\sin\phi, r\cos\phi)\\
	\partial_r \vec{x} \cdot \partial_r \vec{x} &=& \cos^2\phi + \sin^2\phi=1\\
	\partial_\phi \vec{x} \cdot \partial_\phi \vec{x} &=& r^2\sin^2\phi + r^2\cos^2\phi=r^2\\
	\partial_r \vec{x} \cdot \partial_\phi \vec{x} &=& -r\cos\phi\sin\phi + r\cos\phi\sin\phi=0
	\end{array}
	$$
	
	Therefore the metric is
	
	$$
	g_{ij} = \left ( 
	\begin{array}{cc}
	1 & 0 \\
	0 & r^2
	\end{array}
	\right )
	$$
	
	with distance element
	
	$$
	ds^2 = dr^2 + r^2\,d\phi^2
	$$

## Reading suggestions

- *Gravitation* by Charles Misner, Kip Thorne, John Wheeler. The landmark graduate textbook on general relativity. The big black book (1200+ pages) with an apple on the cover.
- *The black hole wars* by Leonhard Susskind. Popular scientific book on his battle to make the world a safer place for quantum mechanics.
- *A brief history of time* by Stephan Hawking. Popular reading on big bang and black holes.
- *The first three minutes* by Steven Weinberg. Popular reading on the early universe.
- *The making of the atomic bomb* by Richard Rhodes. Popular reading on the scientific discovery of fission (in part I) and the organization of the Manhattan project (in part 2) towards the atomic bomb.
- *Quantum: Einstein, Bohr and the great debate about the nature of reality* by Manjit Kumar. Popular reading on the early steps of quantum mechanics.
- *Thirty years that shook physics* by George Gamow. Semi-popular book on history of quantum mechanics.
- *The Feynman lecture* by Richard Feynman. Feynman is a great teacher, and this lectures on mechanics, electro dynamics etc. have inspired many.
- *Theoretical Minimum: What you need to know start doing physics* by Leonard Susskind and George Hrabovsky. Give this to your boy/girl friend who is not studying physics, to learn thinking like a physicist.
- *The physics of Star Trek* by Lawrence Krauss.
- *The hitchhiker's guide to the galaxy* by Douglas Adams.

[We are all made of stars](https://www.youtube.com/watch?v=xAh6fk0KD1c){ target="_blank"}, Moby
