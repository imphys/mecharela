# Forces & Inertia

!!! note "Textbook. & Collegerama"
    - Chapter 1: sections 1.3 (introduction + 1.3.1) and 1.4
    - TN1612TU_02 & _03

!!! summary "Learning goals"
    - Understand the vector quality of force
    - Understand the role of mass in Newton 2
    - Know the meaning of inertial and gravitational mass

## Force

Newton's laws introduce the concept of force. Forces have distinct features:

* Forces are vectors - they have magnitude and direction;
* Forces change the motion of an object:
    * they change the velocity, i.e., they accelerate an object

    $$
    \vec{a} = \frac{\vec{F}}{m}\ \Leftrightarrow\ d\vec{v} = \vec{a}\, dt = \frac{\vec{F}}{m}dt
    $$

    * or, equally true, they change the momentum of an object

    $$
    \frac{d\vec{p}}{dt} = \vec{F}\ \Leftrightarrow\ d\vec{p} = \vec{F}dt
    $$

Many physicists like the second version: forces change the momentum ($d\vec{p}$) of an object, but they need time to act ($dt$).

Momentum is a more fundamental concept in physics than acceleration. That is another reason why physicists prefer the second way of looking at forces as the second expression remains true when considering velocities closer to the speed of light as in Special Relativity.

## Inertia

Inertia is denoted by the letter $m$ for mass. Mass is a property of an object that characterizes its resistance to acceleration or equally to changing its velocity. From N2: $\vec{F}=m\vec{a}$ we see what we know from daily life, heavy objects require more force to move. Think about the force you need to apply to an airplane or oil tanker before they change direction or come to a halt once in motion! Actually, we should have written something like $m_I$, with subscript $I$ denoting inertia.

Why? There is another property of objects, also called mass, which is part of Newton's law of gravitation. Two bodies of mass $m_1$ and $m_2$ attract each other via the so-called gravitational force:

$$
\vec{F}_{12} = - G \frac{m_1 m_2}{r^2_{12}}\hat{r}_{12}
$$

Here, we should have used a different symbol rather than $m$. Something like $m_g$, as it is by no means obvious that the two property 'mass' $m_I$ (from $\vec{F}=m\vec{a}$) and $m_g$ from gravity refer to the same thing at all. If you find that confusing, think about inertia and electric forces: you denote the property associated with electric forces by $q$ and call it charge.

As far as we can tell (via experiments) $m_I$ and $m_g$ have the same value (to precision better than  $10^{-6}$). Actually, it was Einstein who postulated that the two must be conceptually the same: there is no difference, $m_I \equiv m_g$. We come back to this [later](noninertialframes.md#gravity-as-a-fictitious-force).

## Examples

1. Why does a bike move when you ride it? Which force is responsible that you move? Let us ignore any air friction.

	Make a sketch of the action forces and think a bit before peaking at the solution.
	
	??? Answer
	
		Consider you have already a velocity $v_0>0$. 
		
		* If you do not push the pedals you will eventually halt, due to rolling friction of the tires with the street. There is apparently a friction force in opposite direction to the motion $F_{roll}$.
		
		* If you push the pedal hard enough, you know that you will maintain the initial velocity $v_0$ or even accelerate to $v>v_0$. A force for this change in velocity must be located at the tire/street interface in the forward direction. Often it is insightful to consider extremes: Imaging there is no friction between the tires and the street. In this case you will not move! The same goes for biking on ice, you push the pedals, but the wheel is slipping and you do not move forward. Apparently, friction is pushing you forward! The friction force $F_{push}$ is directed in the direction of motion! This is counter intuitive, friction is not always opposite to the direction of motion.

		![fiets](images/fiets.png){ width="400" }

2. Pulling tablecloth without that the glasses fall
	
	As demonstrated during the lectures, it is possible to draw a table cloth from under a set dinner table. Experimentally we found that it is needed to pull the cloth really fast and in the right direction, i.e. parallel to the table surface.
	
	We can reason this with the help of N2. If the glass and plates should not be drawn with the cloth, the resulting force on the glass and plates must be very small in the direction parallel to the table. If we look at N2 in the form $F\, dt=dp$ we see that the change in momentum depends on how long the force is acting. The shorter the time the smaller the change in momentum. If we integrate the equation we obtain
	
	$$
	\int_{t_0}^{t_0+\Delta t} F\, dt = p_{end} - p_{start}
	$$

	Now it is clear, that pulling the cloth quickly, results in a much smaller change in momentum (which could topple the glasses). NB: The integral $\int F\, dt$ is called *impuls* or in Dutch *(kracht)stoot*. Be careful with the terms as *momentum* (GB) = *impuls* (NL).

## Demos

1. Who is strongest?

	[![who is strongest](images/WhoIsStrongest.jpg){ width="500" }](Widgets/WhoIsStrongest.html){ target="_blank" }

2. A game: mass has inertia. Hit the red ball by applying force on a blue one. Click on the image to start.

	[![mass has inertia](images/MassHasInertia.jpg){ width="500" }](Widgets/MassHasInertia.html){ target="_blank" }

3. Two point particles slide down a slope: one feels friction the other doesn't. Can you analyze the situation and understand the graphs? Click on the image to start.

	[![point mass on slope](images/PointMassOnSlope.jpg){ width="500" }](Widgets/PointMassOnSlope.html){ target="_blank" }

4. What kind of force? Click on the image to start.

	[![force acceleration 1](images/ForceAcceleration1.jpg){ width="500" }](Widgets/ForceAndAcceleration1.html){ target="_blank" }

5. A particle pulled one by another. Click on the image to start.

	[![force acceleration 2](images/ForceAcceleration2.jpg){ width="500" }](Widgets/ForceAndAcceleration2.html){ target="_blank" }

6. A particle pulled one by another; explore the influence of friction. Click on the image to start.

	[![force acceleration 3](images/ForceAcceleration3.jpg){ width="500" }](Widgets/ForceAndAcceleration3.html){ target="_blank" }

7. The tablecloth trick

	Pulling a table cloth from a full set table is a cool trick at every dinner party as is demonstrated during the lectures. See [here](https://sciencedemonstrations.fas.harvard.edu/presentations/dinner-table){ target="_blank" } how to set it up yourself.

## Exercises

Here are some exercises that deal with forces. Make sure you practice IDEA.

1. You dropped a stone from a height of 50 m (tower of a church). Calculate the velocity of the stone when it hits the ground (ignore friction). The left side of the video below shows a quick and dirty solution, NOT using IDEA. The right hand side uses IDEA and Newton's 2^nd^ law.

    <video id="myVideo" controls>
 	  	<source src="../movies/DroppingAStone.m4v" type="video/mp4">
 		Your user agent does not support the HTML5 Video element.
	</video>

1. A point particle (mass $m$) is shot from position $z=0$ with a velocity $v_0$ straight upwards into the air. On this particle acts only gravity, i.e. friction with the air can be ignored. The acceleration of gravity $g$ may be taken as a constant.

    Answer the following questions:

    * What is the maximum height that the particle reaches?
    * How long does it take to reach that highest point?

        Solve this exercise using IDEA.

    Sketch the situation and draw the relevant quantities.

    * Reason why this exercise can be solved using $F=ma$ (or $\dot p = F$). Write down the equation of motion (N2) for $m$. Classify what kind of mathematical equation this is and provide initial (boundary) conditions that are needed to solve the equation.
    * Solve the equation of motion and answer the two questions. Check your math and the result for dimensional correctness. Inspect the limit: $F_{g}\to0$.

        <!--First try it yourself (and try seriously) before peeking at the [solution](https://qiweb.tudelft.nl/mecharela/Solutions/KM21-01-Opg01.pdf).-->

---

It is a good habit to make your mathematical steps small: one-by-one. Don't make big jumps or multiple steps in one. If you make a mistake, it will be very hard to trace it back (giving little points in the exam).

Next: always check the dimensional correctness of your equations: that is easy to do, and you will find most of your mistakes. Finally, use letters to denote quantities, including $\pi$. The reason for this is:

* letters have meaning and you can easily put dimensions to them;
* letters are more compact;
* your expressions usually become more compact until the end, then you fill in the numbers if needed.

## Do it yourself

![drop from old church](images/DropFromOldChurch.jpg){ align="right" }

Acceleration of gravity

* Find an object that you can safely drop from some height.
* Drop the object from any (or several heights) and measure using a stop watch or you mobile phone the time from dropping to hitting the ground.
* Measure the dropping height.

Find from these data the value of gravity's acceleration constant $g$.

Don't forget first to make an analysis of this experiment in terms of a physical model and make clear what your assumptions are.

Hint: think about the effect of air resistance: is dropping from a small, medium, or a high height best? Any arguments?

If you want to learn also how to use numerical methods, try using an air drag force: $F_{drag} = -A_\perp C_D \frac{1}{2} \rho_{air} v^2$. With $A_\perp$ the cross-sectional area of your object perpendicular to the velocity vector and $C_D \approx 1$ the [drag coefficient](https://en.wikipedia.org/wiki/Drag_coefficient){ target="_blank"} (that is actually a function of the velocity). $\rho_{air}$ is the density of air which is about $1.2 \text{ kg/m}^3$. Write a computer program (e.g. in Python) that calculates the motion of your object.

## Jupyter labs

You can get some experience with solving the equation of motion with a computer. Analytically, this is often only possible for a very small range of (not realistic) forces. With the computer we can tackle a wider (more realistic) range of forces. Later you will always solve differential equations with the computer.

1. Introduction [introduction.ipynb](resources/ClassicalMechanicsSpecialRelativity-intro.ipynb)
2. Falling with friction part 1
		[Exercise1.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex1.ipynb)
3. Falling with friction part 2
	[Exercise2.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex2.ipynb)
4. Trajectory of a ball at an angle
	[Exercise3.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex3.ipynb)
