# Harmonic Oscillator

!!! note "Textbook & Collegerama"
    - Chapter 4: sections 4.1 - 4.3
    - TN1612TU_12 & _13

!!! summary "Learning goals"
    - List and describe types of oscillators
    - Derive equations of motion for different types of harmonic oscillators
    - Solve problems with mass-spring systems  

The harmonic oscillator is probably the most important thing to remember from this class. That is because for small deviations from a stable equilibrium point *all* systems, irrespectively of their exact nature and what type of pertubation, behave as a harmonic oscillator (see [Potential Energy](potential_energy.md#taylor-series-expansion-of-the-potential)).

In many situations, a particle or object is in some form of oscillatory motion. The physics of these motions usually leads to identifying a restoring force, i.e., a force that tries to drive the particle back to some equilibrium state and due to some form of inertia the motion that is currently present has the tendency to continue.

Why this somewhat abstract text? The reason is that outside classical mechanics, oscillating systems are also found. For example, the disturbance of an electromagnetic field to the electrons in a molecule, give rise to (driven) oscillations, the harmonic response of the electrons is called [Raman scattering](harmonic_oscillator.md#Examamples). It is convenient if you then can make an analogy with a classical mechanics system. It is usually easier to visualize, and its concepts are closer to daily life.

First, we will deal with the archetype of oscillations: the harmonic oscillator. The harmonic oscillator is a system that, when brought out of equilibrium, experiences a force that drives it back to equilibrium, with force proportional to the distance out of equilibrium.

## Undamped harmonic oscillator

A concrete oscillator: Mass-Spring system. Let's look at a particle of mass $m$ suspended on a spring, with spring constant $k$. There is no other force action on $m$ than that of the spring. We will stick to a one-dimensional situation.

![MassSpring](images/MassSpring.jpg){ align="right" width="250" }

The figure shows the archetype: a mass on a spring is brought out of equilibrium by displacing it by $u$. As a result of this displacement, the spring exerts a force $−ku$ that drives the mass back. However, due to its inertia, the mass will 'overshoot' and not stop at the equilibrium position.

We set up the equation of motion for the particle. One force acts on it with  the $x$-coordinate as in the sketch, and we choose $x=0$ when the spring is at its rest length $l_0$. In that case, it doesn't exert any force. Once we make it longer and stretch it to $l>l_0$, it will exert a force $F=-ku=−k(l−l_0)=-kx$, with our choice for $x=0$. Thus N2 takes the form:

$$
m\ddot{x} = F = -k ( l - l_0 ) = -kx
$$

with initial (or boundary) conditions: at $t=0$ the particle will have some position $x_0$ and some velocity $v_0$.

$$
  m\ddot{x} + kx =0
$$

The solution of the harmonic oscillator equation of motion is a combination of sines and cosines (complex exponentials in general):

$$
x(t) = A \sin (\omega_0 t) + B \cos (\omega_0 t)
$$

with $A,B$ constants depending on the boundary conditions and the *natural* oscillation frequency

$$
\omega^2_0 \equiv \frac{k}{m} \hspace{5mm}
$$

Instead of $\omega_0$ we can also specify the period $T = \frac{2\pi}{\omega_0} = 2 \pi \sqrt{\frac{m}{k}}$.

Note: the dimensions of the constants $A,B$ must be [m], the unit of $\omega_0$ is [1/s]. We can find the constants $A,B$ from the initial conditions $x(t=0)=x_0$ and $\dot{x}(t=0)=v_0$.

$$
\begin{array}{rcl}
x(t=0)&=&x_0 = B\\
\dot{x}(t=0) &=& v_0 = A\omega_0
\end{array}
$$

The equation of motion is a second order differential equation with constant coefficients. It is linear because all derivatives only appear linear, e.g. not $x^2$ or $\dot{x}^3$ and the coefficients $m,k$ are time independent and therefore constants.  To solve for $x(t)$ uniquely we need two boundary/initial conditions for a second order differential equation, for us that is the initial speed and position. In the second year you will learn to treat these type of equations more rigorously in the classes *Systems and Signals* and *Differential Equations*, in particular for the case of damping and with a driving force.

![HarmonicSinCos](images/HarmonicSinCos.jpg){ align="center" width="250" }

Note: a heavy particle (large $m$) makes the oscillation slow (large period $T$); a strong spring (large $k$) will cause the system to respond quickly (small period, large frequency of oscillation). We already know this from experience in daily life.

Notice that the oscillation amplitude does not change over time (the oscillation is undamped, see the figure)! In our daily lives, we do not see this much, if at all. Most oscillating systems sooner or later come to rest unless we provide some driving force (like the pendulum of a clock: if there is no energy source, the pendulum will come to rest). The reason is: that we have not included any damping effect ("friction").


### Solution via complex exponentials

Later you will see that a combination of complex exponentials $x(t)\propto e^{\lambda t}$ with $\lambda \in \mathbb{C}$ are the solutions to all linear differential equations. Here we show it for the undamped harmonic oscillator

$$
\ddot{x}+\frac{k}{m}x=0
$$

$$
\begin{array}{rcl}
x(t) &=& e^{\lambda t}\\
\dot{x}(t) &=& \lambda e^{\lambda t}\\
\ddot{x}(t) &=& \lambda^2 e^{\lambda t}
\end{array}
$$

Substituting into the equation of motion

$$
\begin{array}{rcl}
\lambda^2 e^{\lambda t} + \frac{k}{m} e^{\lambda t} &=&0\\
\Rightarrow \lambda^2 &=& -\frac{k}{m}
\end{array}
$$

But as both $k,m>0$, $\lambda$ must be pure imaginary.

$$
\lambda = \pm i \sqrt{\frac{k}{m}} \equiv \pm i \omega_0
$$

Now we have the homogenous solution to the differential equation. To find the particular solution (i.e. a solution for specific boundary conditions) we need to include constants $A,B$ for the plus and minus solution $x(t)=e^{\pm i\omega_0 t}$ just as we did before $x(t)=Ae^{ i\omega_0 t}+Be^{ -i\omega_0 t}$. 

$$
\begin{array}{rcccl}
x(t=0)&=&x_0 &=& A+B\\
\dot{x}(t=0) &=& v_0 &=& i\omega_0 A-i\omega_0 B
\end{array}
$$

This gives us for the two constants 

$$
\begin{array}{rcl}
A &=&\frac{1}{2}(x_0+\frac{v_0}{i\omega_0})\\
B &=&\frac{1}{2}(x_0-\frac{v_0}{i\omega_0})
\end{array}
$$

We see that the constants are complex conjugate of each other $A=B^*$. With Euler's formula $e^{iz}=\cos z + i \sin z$ we can now find the solution by substituting the constants into $x(t)=Ae^{ i\omega_0 t}+Be^{ -i\omega_0 t}$

$$
\begin{array}{rcl}
x(t)&=& \frac{1}{2}(x_0+\frac{v_0}{i\omega_0}) (\cos \omega_0 t + i \sin \omega_0 t) + 
\frac{1}{2}(x_0-\frac{v_0}{i\omega_0}) (\cos \omega_0 t - i \sin \omega_0 t)\\
&=&x_0 \cos \omega_0 t + \frac{v_0}{\omega_0} \sin \omega_0 t
\end{array}
$$

Of course this is the same solution as above, however this is the general solution strategy for linear differential equations with constant coefficients. NB: The solution is pure real, although we started with a general complex ansatz. This makes sense as the oscillation must have a real dimension. NB: remember $\frac{1}{i}=-i$.

### Harmonic Oscillator via the potential

The force driving of the oscillation is given by $F=−kx$. It is a conservative force, and the potential associated with it is the *harmonic potential*:

$$
F = -k x \rightarrow V(x) = -\int_{x_{ref}}^x -kx' dx' \Rightarrow V(x) = \frac{1}{2}k x^2
$$

Where we have taken $x_{ref} = 0$.

Thus the energy equation for the harmonic oscillator is:

$$
\frac{1}{2} m \dot{x}^2 + \frac{1}{2}k x^2 = E_0
$$

Taking the time derivative of the energy equation gives the equation of motion, as we can divide out $\dot{x}\neq 0$ for some time. 

$$
\begin{array}{lcr}
\frac{1}{2}2m\dot{x}\ddot{x}+\frac{1}{2}2x\dot{x} &=& 0\\
m\ddot{x}+kx&=&0
\end{array}
$$

Note that for all potentials that have a stable equilibrium point ($V'(x_{eq}) = 0$ and $V^{"}(x_{eq}) > 0$) we will have that for small deviations from the equilibrium position, a [second-order Taylor expansion](potential_energy.md#taylor-series-expansion-of-the-potential) of the potential energy gives the harmonic oscillator.

## Damped Harmonic Oscillator

In many cases, an additional friction-like force acts on the mass. Consequently, some of the spring's kinetic and/or potential energy is converted into heat, and the oscillator's motion decays over time: the oscillation is damped.

A simple case occurs when the damping force is proportional to the velocity of the mass. The equation of motion includes a friction force, i.e., $F_f = -b\dot{x}$:

$$
m\ddot{x} + b\dot{x} + kx=0
$$

With initial conditions: at $t=0$ the particle will have some position $x_0$ and some velocity $v_0$.

The solution of the damped harmonic oscillator equation of motion is more complicated than that of the harmonic oscillator. We use the ansatz $x(t) \propto e^{-\lambda t}$, with $\lambda\in\mathbb{C}$ a complex constant (Remember Euler's formula $e^{iz}=\cos z + i \sin z$ to see that this can result in oscillations). Substituting $x(t)\propto e^{\lambda t}$ in the equation of motion we obtain the characteristic equation

$$
 m\lambda^2 + b\lambda + k = 0
$$

with solutions (roots)

$$
 \lambda_{1/2} =\frac{-b \pm \sqrt{b^2 - 4mk}}{2m} 
$$

Depending on the respective values of $b,m,k$ the roots $\lambda_{1/2}$ can be real or complex and influence the qualitative behaviour of the oscillation. We need to distinguish 3 cases based on the value of the discriminant: $D = b^2 - 4mk$.

1. Case: $D<0 \Rightarrow \lambda$ are complex
    ![DampedOscillation](images/DampedOscillation.jpg){ align="right" width="200" }

    $$
    x(t) = A e^{-\frac{b}{2m}t} \sin(\omega_d t) + b e^{-\frac{b}{2m}t} \cos (\omega_d t)
    $$

    $$
    \omega_d^2= \frac{k}{m} - \frac{b^2}{4m^2} < \frac{k}{m} = \omega_0^2
    $$

    The solution is an oscillation with decaying amplitude over time. The oscillation frequency is always smaller than the natural frequency $\omega_0$. The characteristic time of the damping/decay of the oscillation is given by the constant in the exponent as $\tau = \frac{2m}{b}$. This expression has the unit [1/s].

1. Case: $D=0 \Rightarrow \lambda = -\frac{b}{2m}$ both roots collapse and there is only one solution. The oscillation part of the solution disappears and only the decay remains.
    ![CriticalDampedOscillation](images/CriticalDampedOscillation.jpg){ align="right" width="200" }

    $$
    x(t) = (A + Bt) e^{-\frac{b}{2m}t}
    $$

    This case is called *critically damped*, because it is the fastest way to go back to equilibrium position. (Note: this has practical value is case you want construct e.g. a door closer. It should close as fast as possible, but without that the door hits the frame before).

1. Case: $D>0 \Rightarrow$ both roots $\lambda_{1/2}$ are real
    ![RealRootOscillation](images/RealRootOscillation.jpg){ align="right" width="200" }

    $$
    x(t) = A e^{ \left ( -\frac{b}{2m} + \sqrt{\frac{b^2}{4m^2} - \frac{k}{m} } \right ) t} +
              B e^{ \left ( -\frac{b}{2m} - \sqrt{\frac{b^2}{4m^2} - \frac{k}{m} } \right ) t}
    $$

    Not really oscillating. The first term dictates the long-term behaviour (the second one decays faster). This case is called *over damped*.
    


## Driven Damped Harmonic Oscillator

Oscillators sometimes experience a driving force that can be periodic in itself.  We will take here the case of a sinusoidal force with frequency $\nu$. Once we understand this, forces consisting of more than one frequency (broader spectrum) can be understood using Fourier analysis (which you will learn about in the second year in the class *Systems and Signals*). There you will also learn to treat this system in more detail analytically. Here we will stick to a simple driving force of the form $F_{ext} = F_0 \sin (\nu t)$.

This gives for the equation of motion:

$$
m\ddot{x}  +b\dot{x} +kx=  F_0 \sin (\nu t)
$$

with initial conditions: at $t= 0$ the particle will have some position $x_0$ and some velocity $v_0$.

The solution of the driven damped harmonic oscillator equation of motion for the case $D = b^2 - 4mk < 0$ is:

$$
x(t) = A e^{- \frac{b}{2m} t} \sin \left ( \sqrt{\frac{k}{m} - \frac{b^2}{4m^2}} t+ \epsilon \right ) +
              x_{max} \sin \left ( \nu t + \alpha \right )
$$

With $A$ and $\epsilon$ determined by the initial conditions.

The two other parameters $x_{max}$ and $\alpha$ are fixed. We will give only the expression for $x_{max}$:

$$
x_{max} = \frac{F_0}{\sqrt{\left ( \omega_0^2 - \nu^2 \right ) ^2 + \frac{b^2}{m^2} \nu^2}}
$$

For $t\to\infty$, the second part, i.e., the term from the driving force $x_{max} \sin \left ( \nu t + \alpha \right )$, survives as the exponential decay will have damped the first term. The oscillation will have frequency $\nu$ of the driving force. As can be seen, the amplitude of the motion is for longer times $x_{max}$. If the driving frequency $\nu\sim\omega_0$, the amplitude increases strongly. Especially for small damping, i.e., small $b$, the amplitude will increase to high values. This phenomenon is called *resonance*:

$$
\text{if  } b \rightarrow 0 \text{ and } \nu \rightarrow \omega_0 \text{  then  } x_{max} \rightarrow \infty \text{   resonance}
$$

### Evolution of the damping

Here we will have a quick look how the damping is evolving, that is we look at the root of the characteristic equation

$$
\lambda_{1/2} =\frac{-b \pm \sqrt{b^2 - 4mk}}{2m} 
$$

and see how it evolves as a function of the damping $b$ in the complex plane.

![Pole Zero Plot](images/polezero.png){ width="400"}

This gives quickly a qualitative view on the different regimes of the damping. The root $\lambda_{1/2}$ is in general complex. We start by looking at the value for roots $\lambda_{1/2}$ as a function of the damping $b$

- No damping: $b=0$. The root is pure imaginary $\lambda_{1/2}=\pm i\sqrt{k/m}$ with two conjugate solutions on the imaginary axis. This gives pure oscillations.
- Some damping $0<b<\sqrt{4mk}$. The root is complex, with real and imaginary part, the oscillation will damp out over time (shown in <span style="color:blue">blue, underdamped regime</span>).
- $b^2=4mk$. The roots collapse into one pure real root $\lambda=-b/2m$  (critically damped), no oscillation.
- Lots of damping $b>\sqrt{4mk}$. The root splits into two real roots, no oscillations (shown in <span style="color:orange">yellow, overdamped regime</span>).

The root walks over the shown graph from $b=0$ on the imaginary axis to $b\to\infty$ over the blue and then yellow part of the graph. The yellow graph does not cross the imaginary axis.

From this plot you can directly see that the system is stable for $b>0$, but unstable for $b=0$ without the need to check the frequency that the system is driven with (for $b=0$ driven with the resonance frequency results in an infinite amplitude - an instable system). How you can see that so quickly you will learn in the second year class *Systems and Signals*.

## Coupled Oscillators

In this course we mostly only consider one oscillator, but of course there could be many that are coupled in one way or another. Already [Christiaan Huygens](https://nl.wikipedia.org/wiki/Christiaan_Huygens){ target=" _blank"} considered them.

![Huygens coupled oscillators](images/coupled_huygens.jpg){ size="300"}

There are 2 pendula suspended from a common connection, which rests on two chairs. If you set the pendula in motion, they will be initially *out of phase*, i.e. the relative position of the pendula is different. But over time their motion synchronises!  What has happend? Apparently the two pendula are connected, *coupled*, via the suspension and act on each other, they are not independent, but influence the motion of the other pendulum.

[![Coupled Oscillators](images/prev_coupled.jpg)](https://www.youtube.com/watch?v=DD7YDyF6dUk){ target=" _blank"}

In this movie, you can see a modern day version of this phenomena. Here the pendula are coupled via the ground. This influence is called *weak coupling*. In this course we cannot treat this coupling mathematically, but in the second year course on *Classical Mechanics* you will learn to study systems like these.

## Examples

1. Example of resonance: sound waves are exciting a glass. By changing the frequency of the sound waves to the resonance frequency, the glass starts oscillating with increasing amplitude until it finally breaks.

    <video id="myVideo" width="400px" controls>
          <source src="../movies/Resonance_GlassDestroyedbySound.mp4" type="video/mp4">
    </video>

1. Driven harmonic oscillator with damping.

    Click on the image to start the physlet.

    [![driven damped oscillator](images/DrivenDampedOscillator.jpg){ width="400" }](Widgets/DrivenOscillator.html){ target="_blank" }

1. 1940: the Tacoma Narrows Bridge in the state Washington on the West coast of the USA is brought into resonance by the wind. The end result: click the movie to see it yourself.

    <video id="myVideo" width="400px" controls>
          <source src="../movies/tacoma_narrows1.mp4" type="video/mp4">
    </video>

1. Breaking a HDD hard disk with a song of Janet Jackson

    [Read here](https://devblogs.microsoft.com/oldnewthing/20220816-00/?p=106994?WT.mc_id=onedevquestion-c9-raymond){ target="_blank" } about this truly amazing piece of applied physics on a blog of Microsoft developer Raimond Chen.
    
1. Raman scattering. 
	
	If electromagnetic radiation interacts with a molecule to zero order, then the molecule does not vibrate or rotate. The radiation with field $E(t)=E_0 \cos (2\pi \nu_0 t)$ then induces a dipole moment in the electron cloud of the molecule what will vibrate with the same frequency as the external field $\nu_0$. This phenomena is called [Rayleigh scattering](https://en.wikipedia.org/wiki/Rayleigh_scattering){ target="_blank"}.
	
	If the electrons in the molecule already oscillate with another frequency $\nu_{vib}$, and the polarizability $\alpha$ of the molecule depends on the distance $R=R_0\cos (2\pi\nu_{vib}t)$ from the nuclei of the molecule, to first order the polarizability can be modeled as $\alpha (R)= \alpha (R_0) + \alpha' (R-R_0)+ {\cal{O}} (R^2)$. The emitted field is therefore
	
	$$
	\begin{array}{rcl}
	p(t) &=& \left [ \alpha(R_0)+\alpha' \cos (2\pi \nu_{vib}t) \right ] E_0 \cos(2\pi\nu_0 t)\\
	&=& \alpha(R_0)E_0 \cos (2\pi\nu_0 t) \\
	&& +\frac{1}{2}\alpha' E_0 \left [ \cos(2\pi (\nu_0+\nu_{vib})t) + \cos(2\pi (\nu_0-\nu_{vib})t)\right ]
	\end{array}
	$$
	
	We see that next to the Rayleigh term $\cos(2\pi\nu_0 t)$ two additional lines with frequency $\nu_0\pm \nu_{vib}$ appear. These lines are called *Raman* lines. They are characteristic for the vibrational bands of the molecule present and are used to probe the presence of certain molecules in practice. NB: The line with frequency $\nu_0+\nu_{vib}$ has higher energy than the incoming light. This line has a much smaller intensity than the $\nu_0-\nu_{vib}$ line. To understand this you need to consider quantum mechanics and that the occupation of states is Boltzmann distributed (but this is [beyond this example](https://en.wikipedia.org/wiki/Raman_scattering){ target="_blank"} and you will learn more about this in the second and third year).
	
	We hope that you see the similarity to the harmonic oscillator where the polarizability $\alpha$ is modelled as a (restoring) force.
	
1. Second-harmonic generation 
	
	Of course the harmonic potential is only a first order approximation around an equilibrium. An example, for a non-linear force or anharmonic potential effect, is the generation of [second-harmonic generation](https://en.wikipedia.org/wiki/Second-harmonic_generation){ target="_blank"}. If you shine high intensity light onto the electrons of a molecule, they are pushed out of equilibrium further and if the governing potential is anharmonic, the electric field response will not only include the incoming frequency $\omega$ but also *higher harmonics* $2\omega, 3\omega,\dots$, but with much lower intensity. That the emitted frequencies are occurring in integer multiple of the incident frequency can be understood either from quantization of light into photons (and the conservation of energy) or from Fourier analysis of the periodic motion of the electron.

1. Erasmus Bridge & singing cables. 

	The bridge in Rotterdam, but also others, suffer from long cables that the wind can put into resonance. Their motion then generates acoustic waves in the audible spectrum. [Listen here](https://singingbridgesmusic.bandcamp.com/track/erasmus-bridge-rotterdam){ target="_blank"} to the sound of the cables starting from 1:00 on the website for singing bridges! 


## Exercises

Here are some exercises that deals with oscillations. Make sure you practice IDEA.

1. A massless spring (spring constant $k$) is suspended from the ceiling. The spring has an unstretched length $l_0$. At the other end is a point particle (mass $m$).

    * Make a sketch of the situation and define your coordinate system.

    * Find the equilibrium position of the mass $m$.

    * Set up the equation of motion for $m$.

    * Solve it for the initial condition that at $t=0$ the mass $m$ is at the equilibrium position and has a velocity $v_0$.

1. Same question, but now two springs are used. Spring 1 has spring constant $k$; spring 2 has $2k$. Both have the same unstretched length $l_0$.

    * The two springs are used in parallel, i.e., both are connected to the ceiling, and $m$ is at the joint other end of the springs.

    * Both springs are in series, i.e., spring 1 is suspended from the ceiling, and the other one is attached to the free. The particle is fixed to the free end of the second spring.

## Do it yourself

![MassSpringDIY](images/MassSpringDIY.jpg){ align="right" width="250"}

Find a rubber band and use nothing but a mass (that you are not allowed to weigh) that you can tie one way or the other to the spring, a ruler, and the stopwatch/clock on your mobile.

Set up an experiment to find the mass $m$, the spring constant $k$, and the damping coefficient $b$.

Don't forget to make a physics analysis first, a plan of how to find both $m$ and $k$.

## Jupyter labs

1. Mass-spring system
	[Exercise4.ipynb](resources/ClassicalMechanicsSpecialRelativity-Ex4.ipynb)
