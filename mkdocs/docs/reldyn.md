#  Relativistic dynamics and collisions

!!! note "Textbook & Collegerama"
    - Chapter 14: section 14.6 & 14.7, 14.10
    - TN1612TU_29 & _30

!!! summary "Learning goals"
    - Apply conservation of 4-momentum and the momentum-energy relation to collisions
    - Understand the importance of choosing the best frame to analyze a problem
    - Be the 4-force with you
    
## 4-force

Starting from the [4-momentum](4impuls.md#4-momentum) defined earlier we can define the 4-force $\vec{K}$

$$
\vec{K}=\frac{d\vec{P}}{d\tau}=\frac{d}{dt}\left ( m\gamma(u)c,m\gamma(u)\vec{u}\right )
$$

with $E=m\gamma(u)c^2$ we can rewrite this to

$$
\vec{K} = \gamma(u) \left ( \frac{1}{c}\frac{dE}{dt}, \frac{d}{dt}m\gamma(u)\vec{u} \right ) = \gamma(u) \left ( \frac{1}{c}\frac{dE}{dt}, \vec{F} \right )
$$

with the 3-force $\vec{F}= \frac{d}{dt}(m\gamma(u)\vec{u})$

## Collisions

The [4-momentum is conserved](4impuls/#conservation-of-4-momentum). For $\vec{P}=(\frac{E}{c},\vec{p})$ we have 

$$
\sum_{i,before} \vec{P}_i = \sum_{j,after} \vec{P}_j
$$

and the [energy-momentum relation](4impuls.md#energy-momentum-relation) from the LT invariance of $\vec{P}\cdot\vec{P}$

$$
E^2 = (mc^2)^2 + (pc)^2
$$

With $E=m\gamma(u)c^2$ and $\vec{p}=m\gamma(u)\vec{u}$.

### Example: head on collision

Two elementary particles collide

![Relativistic Collision](images/relbot.png){ width="300"}

Both particles have mass $m$, after the collision there is only one particle with unknown mass $M$. What is the mass $M$ and the velocity $v$ of the one particle after the collision/fusion?

We consider the conservation of 4-momentum, in 1D:

$$
\begin{array}{rcl}
P_{before}^\mu &=& (m\gamma(u)c,m\gamma(u)u)+(m\gamma(-u)c,-m\gamma(-u)u)\\
&=& (2m\gamma(u)c,0)\\
P_{after}^\mu &=& (M\gamma(v)c,M\gamma(v)v)
\end{array}
$$

with $\gamma(u)=\gamma(-u)$. The 4-momentum is conserved per component, from the space component we see $0=M\gamma(v)v\Rightarrow v=0$. With $\gamma(u)=5/3$ and $\gamma(v)=1$ we find for the time-component $2m\frac{5}{3}=M$. 

This leads to $M=\frac{10}{3}m > 2m$.

### Example: decay of a photon into an electron and positron

We discuss if a photon (of sufficient energy $E>1024$ keV) can decay into an electron $e^-$ and positron $e^+$.

If we place us in the [CM frame](collisions.md#collisions-in-the-cm-frame) of the electron $e^-$ and positron $e^+$ after the decay, then the total spatial momentum is $\vec{p}=0$. The momentum before the decay of the photon is $\vec{p}=\frac{hf}{c}>0$ therefore the decay cannot happen in free space. Momentum must be transferred to an additional different particle. 

$$
\left ( \frac{E_e}{c},\vec{p} \right ) +
\left ( \frac{E_p}{c},-\vec{p} \right )=
\left ( \frac{hf}{c},\frac{hf}{c} \right )
$$
	
### Example: Electron positron annihilation

We consider an electron and positron annihilation, resulting in two photons (after the collision). Remember that the decay cannot happen into one photon as shown above (Remember: equations are invariant under time reversal).

In the CM of the $e^-e^+$ system we have for the 4-momentum before

$$
P_{before}^\mu =(m_e \gamma(u)c, m_e \gamma(u)u,0,0)+ (m_e \gamma(-u)c, -m_e \gamma(-u)u,0,0)
$$

After we have two photons, with different frequencies $f,f'$ and traveling in different directions $\hat{s},\hat{s}'$

$$
P_{after}^\mu = \left ( \frac{hf}{c}, \frac{hf}{c} \hat{s}\right ) + \left ( \frac{hf'}{c}, \frac{hf'}{c}\hat{s}'\right )
$$

From the conservation of 4-momentum we have 

$$
\begin{array}{rcl}
2m_e\gamma(u) c &=& \frac{hf}{c}+\frac{hf'}{c}\\
0 &=& \frac{hf}{c}\hat{s} + \frac{hf'}{c}\hat{s}'
\end{array}
$$

From the second equation we see

$$
\frac{hf}{c}\hat{s} = -\frac{hf'}{c}\hat{s}' 
\Rightarrow \hat{s} = -\hat{s}',\quad f=f'
$$

The two photons are emitted in opposite directions (in the CM system) with the same frequency.

Filling this into the first equation $hf = m_e \gamma(u)c^2 \approx m_e c^2 = 512$ keV. The speed in the CM frame is typically $u\ll c \Rightarrow \gamma(u)=1$.

NB: please observe that analysis in the CM frame is often a good idea.

### Example: Compton scattering

[Compton scattering](https://en.wikipedia.org/wiki/Compton_scattering){ target="_blank"} describes the (elastic) scattering of an incoming photon by a (bound) charged particle, typical an electron. 

![Compton scattering](images/compton.png){ width="500"}

In the rest frame of the electron, we have for the 4 different 4-momenta:

$$
\begin{array}{rcl}
P_{e,b} &=& (m_e c,0,0,0)\\
P_{\gamma, b} &=& (E/c,E/c,0,0)\\
P_{e,a} &=& (\frac{E_e'}{c},m_e\gamma(u)u \cos\phi, -m_e \gamma(u)u \sin \phi,0)\\
P_{\gamma,a} &=& (\frac{E'}{c}, \frac{E'}{c} \cos\theta, \frac{E'}{c}\sin\theta,0)
\end{array}
$$

We have

$$
P_{e,b} + P_{\gamma, b} = P_{e,a} + P_{\gamma,a}
$$

Now we make use of the LT invariance of $P^2$ 

$$
(P_{e,b} + P_{\gamma, b} - P_{\gamma,a})^2 = P^2_{e,a} 
$$

$$
P_{e,b}^2 + P_{\gamma, b}^2 + P_{\gamma,a}^2 + 2P_{e,b}P_{\gamma, b} - 2P_{e,b}P_{\gamma,a} - 2P_{\gamma, b}P_{\gamma, a} = P^2_{e,a}
$$

where we know $P^2_{e,b}=P^2_{e,a} = m_e^2c^2$ (toally elastic collison) and $P_\gamma^2=0$ directly as [shown before](4impuls.md#lt-invariance-of-p). Evaluating the cross terms gives

$$
m_e^2c^2 +0+0+2m_eE'-2m_eE-2 \frac{EE'}{c^2}(1-\cos\theta)=m_e^2c^2
$$

We isolate the energie after the collision $E'$

$$
E' = \frac{Em_ec^2}{m_ec^2 +E(1-\cos\theta)}
$$

With $E=hc/\lambda$ we obtain

$$
\frac{\lambda'}{hc} = \frac{m_ec^2 +\frac{hc}{\lambda}(1-\cos\theta )}{\frac{hc}{\lambda} m_ec^2}
$$

Now we only multiply both sides by $hc$ and on the right we divide out, to obtain

$$
\lambda' = \lambda + \frac{h}{m_ec}(1-\cos\theta)
$$

<!---
This is much more work than the P^2 trick
In the rest frame of the electron

$$
P_{before}^\mu = \left ( \frac{hf}{c},\frac{hf}{c},0,0 \right ) +(m_e c,0,0,0)
$$

After the scattering

$$
\displaylines{P_{after}^\mu = \left ( \frac{hf'}{c},\frac{hf'}{c} \cos\theta, \frac{hf'}{c} \sin\theta,0 \right ) +\\
(m_e\gamma(u)c,\, m_e\gamma(u)u \cos\phi,\, -m_e \gamma(u) u\sin\phi,\,0)}
$$

We have 3 equations, but 4 unknowns $(f', u, \phi, \theta)$. Therefore the outgoing frequency $f'$ is not uniquely determined, but dependent on the scattering angle $\theta$. We can eliminate 2 (here $u,\phi$) of the 4 unknowns, to remain with a relation for the other two.

For the spatial momentum we have 

$$
\begin{array}{rcl}
\frac{hf}{c} &=& \frac{hf'}{c}\cos\theta + m_e \gamma(u)u\cos\phi\\
0 &=& \frac{hf'}{c}\sin\theta - m_e \gamma(u)u\sin\phi
 \end{array}
$$

We rewrite the equations slightly, before squaring them and then adding them to eliminate $\phi$

$$
\begin{array}{rcl}
\frac{hf}{c} - \frac{hf'}{c}\cos\theta&=&  m_e \gamma(u)u\cos\phi\\
\frac{hf'}{c}\sin\theta &=&  m_e \gamma(u)u\sin\phi
 \end{array}
$$

We indeed eliminate $\phi$ to

$$
\frac{h^2f^2}{c^2}-2\frac{hfhf'}{c^2}\cos\theta + \frac{h^2 f'^2}{c^2}=m_e^2 \gamma^2(u)u^2 \quad (*)
$$

The right hand side of the equation is the space component squared of the momentum after: $p^2_{e'} = m_e^2\gamma^2(u)u^2$, but this can be related to the energy via the [momentum-energy relation](4impuls.md#energy-momentum-relation) for the moment after $(p_{e'}c)^2 = E^2_{e'}-(m_ec^2)^2$. We will use this to eliminate the unknown speed $u$.

The energies can be related via the 0-component of the 4-momentum

$$
\begin{array}{rcl}
\frac{hf}{c}+m_e c &=& \frac{hf'}{c} + \frac{E'_e}{c}\\
\Rightarrow E^{'2}_e &=& (hf-hf'+m_ec^2)^2
\end{array}
$$

Substituting the energy $E^{'2}_e$ into the momentum-energy relation and replacing the right hand side of equation $(*)$ after multiplying by $c^2$ to

$$
h^2f^2-2hfhf'\cos\theta + h^2 f'^2=
(hf-hf'+m_ec^2)^2 - (m_ec^2)^2
$$

Indeed we have removed the speed $u$ and angle $\phi$. We cannot do more, but remain with a relation for the frequency $f'$ after scattering as function of angle $\theta$. To this end we evaluate the square in the equation, cancel a few terms and rearrange to

$$
\begin{array}{rcl}
2hfm_ec^2 - 2hf'm_ec^2 &=& 2h^2 ff' (1-\cos\theta)\\
\frac{c}{f'}-\frac{c}{f} &=& \frac{h}{m_ec}(1-\cos\theta)
\end{array}
$$

Finally, by replacing the frequency with the wavelength $f\lambda=f'\lambda'=c$

$$
\lambda'-\lambda = \frac{h}{m_ec}(1-\cos\theta)
$$

-->
As $\cos\theta<1$ we find $\lambda'>\lambda$, which makes sense as the photon can only loose energy to the electron in the initial rest frame of the electron. After the scattering the electron can pick up some speed.

To analyze the outcome we check for 

- $\theta=0$ (no scattering): $\Rightarrow \lambda'=\lambda$ which makes sense
- $\theta=\pi$: backwards scattering, maximal $\Delta \lambda = \frac{2h}{m_ec}$ largest energy transfer
