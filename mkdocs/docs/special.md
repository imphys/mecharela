# Special theory of relativity

!!! note "Textbook & Collegerama"
    - Chapter 11 : section 11.1 & 11.2
    - TN1612TU_23 & _24

!!! summary "Learning goals"
	- Understand that Maxwell equations are not GT invariant
	- Understand the consequence of the Michelson-Morley experiment. The speed of light in vacuum is constant in all inertial frames
	- The laws of physics should be the same in all inertial frames

## The problem with Maxwell's equations

In the early 1860s [Maxwell](https://en.wikipedia.org/wiki/James_Clerk_Maxwell){ target="_blank"} extended Ampères law, in combination with Gauss and Faraday laws and these four coupled differential equations describe the generation of electro-magnetic fields from charges and currents. They are now just known as *the Maxwell equations*. They read in modern (differential) notation as follows for the electric  $\vec{E}(\vec{x},t)$ and magnetic $\vec{B}(\vec{x},t)$ field in free space

$$
\begin{array}{rcl}
\nabla \cdot \vec{E} &=& \frac{\rho}{\epsilon_0}\\
\nabla \cdot \vec{B} &=& 0\\
\nabla \times \vec{E} &=&  -\frac{\partial \vec{B}}{\partial t}\\
\nabla \times \vec{B} &=& \mu_0\vec{J}+ \epsilon_0 \mu_0 \frac{\partial \vec{E}}{\partial t}
\end{array}
$$

With $\rho(\vec{x})$ the charge density distribution and $\vec{J}(\vec{x},t)$ the electric current density, and constants $\epsilon_0$ the vacuum permittivity and $\mu_0$ the vacuum magnetic permeability. 
Or in integral notation you learnt earlier in the course *Introduction to Electricity and Magnetism* 

$$
\begin{array}{rcl}
\oint \vec{E}\cdot d\vec{A} &=& \frac{q}{\epsilon_0}\\
\oint \vec{B}\cdot d\vec{A} &=& 0\\
\oint \vec{E}\cdot d\vec{\ell} &=&  -\frac{d \Phi_B}{d t}\\
\oint \vec{B}\cdot d\vec{\ell} &=& \mu_0 I+ \epsilon_0 \mu_0 \frac{d \Phi_E}{d t}
\end{array}
$$

You will learn all about Maxwell's equations in the first and fourth year classes *Electromagnetism* and *Advanced Electrodynamics*. The mathematics of these equations is quite difficult as each equation is $3D+t$ and the equations are coupled.

In vacuum ($\rho=0$ and $\vec{J}=0$) we can take the curl of the third equation, use the [curl identity](https://en.wikipedia.org/wiki/Vector_calculus_identities#Curl_of_curl){ target="_blank"} $\nabla \times (\nabla \times \vec{E})= \nabla (\nabla \cdot \vec{E})-\nabla^2 \vec{E}$, the first, and the fourth equation to arrive at the *wave equation*

$$
\nabla^2 \vec{E} - \epsilon_0 \mu_0 \frac{\partial^2 \vec{E}}{\partial t^2}=0
$$

This equation describes the propagation of the electric field through vacuum (you can of course derive the same equation for the magnetic field). In the wave equation a second derivative in space is coupled with a second derivative in time. The solution to this differential equation is $\vec{E}(\vec{x},t)\propto \cos(\vec{k}\cdot \vec{x}-\omega t)$, with the *wave vector* $\vec{k}$ pointing the direction of propagation and with magnitude $k=2\pi/\lambda, \omega = 2\pi \nu$ and $\nu \lambda =c$ with the frequency of oscillation $\nu$ and wavelength $\lambda$. You will learn more about this in the courses *Electromagnetism* (first year) and *Introduction to Waves* (second year). Light is identified as an electro-magnetic wave and from this equation the wave velocity is clearly 

$$
\frac{1}{\sqrt{\epsilon_0 \mu_0}}\equiv c=2.998\cdot 10^8\ m/s \approx 300,000\ km/s 
$$

If the Maxwell equations are laws of physics all inertial observers must be able to write down the equation in the same form. Therefore for an observer $S'$ we would write down the wave equation for a field that propagates only along the $x$-direction with amplitude in the $z$-direction (without loss of generality) $\vec{E}=(0,0,E_z(x,t))$ as

$$
\frac{\partial^2 E'_{z'}}{\partial x^{'2}} - \epsilon_0 \mu_0 \frac{\partial^2 E'_{z'}}{\partial t^{'2}}=0\ (*)
$$

This has exactly the same form as for $S$ which is good if it should represent a physical law. However, for $S'$ the speed of the wave is also given by $c=\frac{1}{\sqrt{\epsilon_0 \mu_0}}$. As the speed is covered by universal constants $\epsilon_0, \mu_0$, the speed is the same of $S$ and $S'$ or in other words $c=c'$! This is not what should happen! From the [Galileo Transformation](galilean_transformation.md) we know that we should find the same form, but with $c'=c-V$ the relative velocity of the two observers. 

The Navier-Stokes equation was Galileo invariant as [shown earlier](galilean_transformation.md#navier-stokes-equation-is-galilei-invariant), but the the Maxwell's equation are not!

Now we take the more formal route as we have down [with Navier-Stokes](galilean_transformation.md#navier-stokes-equation-is-galilei-invariant). For a transformation $S\to S':\ x'=x-Vt,t'=t$ we need to transfer the partial derivatives to the new coordinates, repeating what we found earlier, by applying the chain rule $\frac{\partial f(x(x',t'))}{\partial x} =  \frac{\partial f}{\partial x'}\frac{\partial x'}{\partial x} + \frac{\partial f}{\partial t'}\frac{\partial t'}{\partial t}$ and using
 $\frac{\partial x'}{\partial x}=1$,
 $\frac{\partial t'}{\partial x}=0$,
 $\frac{\partial t'}{\partial t}=1$ and $\frac{\partial x'}{\partial t}=-V$, we find

$$
\begin{array}{rcl}
\frac{\partial}{\partial x} &=&\frac{\partial}{\partial x'}\\
\frac{\partial}{\partial t}&=&
\frac{\partial}{\partial t'}-V \frac{\partial}{\partial x'}
\end{array}
$$
  
Application of the same trick for the second derivative gives us

$$
\begin{array}{rcl}
\frac{\partial^2}{\partial x^2} &=& \frac{\partial^2}{\partial x^{'2}} \\
\frac{\partial^2}{\partial t^2} &=& 
\frac{\partial^2}{\partial t^{'2}}-
2V \frac{\partial^2}{\partial x'\partial t'} +
V^2 \frac{\partial^2}{\partial x^{'2}}
\end{array}
$$


If we apply the coordinate transformation from $S\to S'$ the wave equation reads thus as

$$
\frac{\partial^2 E_z}{\partial x^{'2}}-\frac{1}{c^2}\frac{\partial^2 E_z}{\partial t^{'2}}-\frac{V^2}{c^2}\frac{\partial^2 E_z}{\partial x^{'2}}+
\frac{2V}{c^2}\frac{\partial^2 E_z}{\partial x' \partial t'}=0
$$

Now we still need to find a transformation $E_z\to E'_{z'}$ (and $c'\to c)$trying to retrieve the general form of the wave equation, but there is no such transformation. Therefore the wave equation of electromagnetic waves is not Galilei invariant at all!

The solution is that the Galilei transformation does not govern the physical laws, but the Lorentz transformation as we will see in the rest of the course. However, the difference between these two transformation is only relevant for very high speeds. Equation $(*)$ is indeed Lorentz invariant.

NB: The Navier-Stokes equation is not Lorentz invariant, and therefore needs a correction for high velocities. If *any* equation is not Lorentz invariant, this means it does not tell us the full picture of physics, something must be missing! For example Schr&ouml;dinger directly knew that his equation was not the end, as it is not Lorentz invariant - only the extension by Paul Dirac made it so.


### Hypothesis of the aether

As light is a wave, people naturally thought there must be a medium to transport the wave, *something* must be oscillating. Vacuum was considered nothing, not something. A water wave, needs water as medium to transport the wave; the water oscillates. Or take sound waves, they need gas or a solid to oscillate. What could be the medium  that light, electromagnetic waves, use to oscillate? This medium must be all around us, in the space between the sun and earth, just everywhere. To save the Galilei invariance of Maxwell's equations this also needs to be a very special kind of medium that behaves differently than other media. This strange hypothetical medium was termed *aether*. The search for the properties of the aether lead to the Michelson-Morley experiment - which showed that there was no aether at all! [Lorentz](lorentz.md#historical-context) and [Fitzgerald](https://en.wikipedia.org/wiki/George_Francis_FitzGerald){ target="_blank"} found an ad hoc way to save the day by postulating an [adapted version of the Galilei transformation](lorentz.md) and a [contraction of length](timedilation.md##length-contraction) respectively. Later more about that, and how Einstein showed that all of this ad hoc business is not needed, things follow directly from his second axiom.


## The Michelson-Morley experiment

The [Michelson-Morley experiment](https://en.wikipedia.org/wiki/Michelson%E2%80%93Morley_experiment){ target="_blank"} was performed 1880-1890 to investigate properties of the hypothetical aether. The experiment returned a null-result, i.e. there was no sign of the existence of the aether - and to this day there is none.

The idea is to check the speed of light for two observers $S$ and $S'$. One is moving with respect to the other with the highest possible speed, the orbit speed of the earth around the sun $\sim 30$ km/s. Of course, that is still only $10^{-4}$ compared to 300,000 km/s of the speed of light but could be measured spectroscopically by interference.

The experiment essentially consists of a [Michelson interferometer](https://en.wikipedia.org/wiki/Michelson_interferometer){ target="_blank"}. Light is send to a 50/50 beam splitter such that half of the light is reflected up towards arm $L_1$ and half is transmitted to arm $L_2$. The mirrors at the end of each arm reflect the light back. On the way back again half of the light is transmitted and reflected at the beamsplitter, such that half of the light from both arms is now traveling downwards towards the image plane/camera. At the image plane the light from both arms form an interference pattern, depending on the path length difference induced by the difference of $L_1-L_2$.

![MM-setup](images/MM-setup.png){ align="right" width="250" }

The whole setup is mounted for stability on a heavy table that is floating in liquid mercury, to reduce vibrations coupling to the setup. If now one arm is parallel to the earth's orbit with $V=30$km/s, while the other is perpendicular to it, there will be some phase shift between the two arms $\Delta \lambda_1$. If we rotate the setup by 90 degree (easily done in the mercury bath), then the roles of $L_1$ and $L_2$ are exchanged, leading to another phase shift $\Delta \lambda_2$. Therefore after rotation the fringe patter should shift as

$$
\Delta \phi=2\pi\frac{\Delta \lambda_1 - \Delta \lambda_2}{\lambda} = 2\pi\frac{(L_1+L_2)}{\lambda}\frac{V^2}{c^2}
$$

If we fill in the numbers $\lambda =550$ nm, $L_1+L_2=11$ m and $V^2/c^2=10^{-8}$ this results in an expected $\Delta \phi=0.4 \pi$. However, Michelson and Morely found only $\Delta \phi \leq 0.01\pi$. The experiment to find the aether failed.

Physics was is serious trouble until 1905.

NB: Back in the days, white light was used for the actual measurement and monochromatic coherent light of e.g. a sodium lamp for alignment. As white light produces a colored interference pattern which is much easier to observe visually. Otherwise temperature changes or vibrations, resulted in constant fringe drift. Today monochromatic laser light can be used in combination with environmental temperature control better than to 0.1 C and sensitive CCD cameras. Today experiments have confirmed the null-result of Michelson and Morely but to much better precision. The anisotropy in the speed of light is $c_\perp / c_{||}\leq 10^{-17}$.

---

Although the proposed hypothetical medium aether (or ether) does not exist, as proven a long time ago, the terminology did not drop from everyday language. 

## Einstein's axioms

In 1905 Einstein formulated his special theory of relativity with the article *Zur Elektrodynamik bewegter K&ouml;rper*, Annalen der Physik, **17**:891-921, 1905. He choose the Maxwell equations and the Michelson Morely experiment as a starting point for this argument to arrive at

1. The laws of physics are the same in all inertial frames of reference.
2. The velocity of light in vacuum is the same in all inertial frames.

That does not sound like a lot or world changing, but it was certainly. You can directly see that the second axioms violates Galilean addition of velocities, but that is what was found experimentally!

If you think these two axioms stubbornly thorough and take their consequences seriously, things get hairy. We will do this.

--- 

Extra reading with a historic perceptive. In a 200 page book [Wolfgang Pauli](https://en.wikipedia.org/wiki/Wolfgang_Pauli){ target="_blank"} *Theory of Relativity*, Dover (The original German version is available online [*Relativitätstheorie*](https://archive.org/details/EncyklopdieDerMathematischenWissenschaftennfterBandPhysik/page/538/mode/2up){ target='_blank"}) summaries all that was known about special relativity as a request made by this PhD advisor ([Arnold Sommerfeld](https://en.wikipedia.org/wiki/Arnold_Sommerfeld){ target="_blank"}). It is worth a read, although the notation is a bit outdated.

---

Extra reading [Hoe de ether verdween uit de natuurkunde](images/HoeDeEtherVerdween.pdf). This article by Jos Engelen in the *Nederandse Tijdschrijft voor Natuurkunde* explains the Michelsen-Morley experiment, places it into historic perspective and then adds the work of Lorentz, Poincaré and Einstein leading to the Lorentz transformation.