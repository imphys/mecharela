# Website for MechaRela course

* <https://qiweb.tudelft.nl/mecharela>

## Bugs, feature-requests and other suggestions

You are invited to send us any bugs you find in this website via the **Issues** menu on the GitLab repository:

* <https://gitlab.tudelft.nl/imphys/mecharela/-/issues>

*Note: you will need to login, preferably with your netid*

Your issue will be picked up by one of the project owners. You are also very welcome to write any suggestions you have for this website via these **Issues**.

## Continues Deployment

With CI/CD after each push to mkdocs files will be rendered as html and `rsync`-ed to the webserver.
